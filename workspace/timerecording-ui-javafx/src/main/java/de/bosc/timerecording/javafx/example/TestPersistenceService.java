package de.bosc.timerecording.javafx.example;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.WorktimeCalculationService;
import de.bosc.timerecording.basic.controller.domino.DominoHolidayEntitlementController;
import de.bosc.timerecording.basic.entity.HolidayEntitlementEntity;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.service.TimeentryService;
import de.bosc.timerecording.javafx.persistence.TerminalPersistenceService;

public class TestPersistenceService {

	private TerminalPersistenceService terminalPersistenceService;
	private Logger logger;
	
	public static void main(String[] args) {
		TestPersistenceService testPersistenceService = new TestPersistenceService(new TerminalPersistenceService());
		//terminalPersistenceService.getDevices();
		//testPersistenceService.createTimeEntry();
		//testPersistenceService.getTimeEntriesMonth();
		//testPersistenceService.writeAccountingMonth();
		testPersistenceService.saveHolidayEntitlement();
		
	}
	
	public TestPersistenceService(TerminalPersistenceService terminalPersistenceService) {
		this.terminalPersistenceService = terminalPersistenceService;
		this.logger = terminalPersistenceService.getLogger();
	}
	
	public void saveHolidayEntitlement(){
		
		try {
			Person person = this.terminalPersistenceService.getPersonService().getPerson("1");
			DominoHolidayEntitlementController controller = new DominoHolidayEntitlementController();
			List<HolidayEntitlementEntity> holidayEntitlements = controller.getHolidayEntitlementForPerson(this.terminalPersistenceService, "1");
			HolidayEntitlementEntity holidayEntitlementEntity = holidayEntitlements.get(1);
			System.out.println("year: " + holidayEntitlementEntity.getYear());;
			holidayEntitlementEntity.setEffectiveDays(35);
			controller.saveHolidayEntitlement(this.terminalPersistenceService, holidayEntitlementEntity);
		} catch (PersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
	public void writeAccountingMonth(){
		String personalNumber = "1";
	//	personalNumber = "MFRZ-AR3MC7";
		//personalNumber = "120518"; // Elsa Kleiner
		
		try {
			Person person = terminalPersistenceService.getPersonService().getPerson(personalNumber);
			WorktimeCalculationService service = new WorktimeCalculationService();
			service.recalculatePerson(terminalPersistenceService, person);
//			WorktimeMonthService service = new WorktimeMonthService();
//			WorktimeMonth worktimeMonth = service.createWorktimeMonth(terminalPersistenceService,person,new Date());
//			service.calculateMonthForPerson(worktimeMonth);
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(new Date());
//			cal.add(Calendar.MONTH,-1);
			//service.calculateMonthForPerson(cal.getTime());
		} catch (PersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void createTimeEntry() {
		try {
			Timeentry timeentry = new Timeentry();
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.set(2018, 0, 4, 8, 0, 0);
			
			Date startDate = cal.getTime();	
			cal.set(2018, 0, 8, 16, 0, 0);
			Date endDate = cal.getTime();
			
			timeentry.setStartDate(new DateString(startDate));
			timeentry.setEndDate(new DateString(endDate));
			timeentry.setPersonalNumber("MFRZ-AR8EBD");
			timeentry.setStartTime(TimeentryService.parseDate(startDate));
			timeentry.setEndTime(TimeentryService.parseDate(endDate));
			timeentry.setTimeentryKindId("69AAF28FEB245C4FC1258202005F85BC");
			terminalPersistenceService.getTimeentryService().saveTimeEntry(timeentry);

		} catch (PersistenceException e) {
			e.printStackTrace();
			logger.error("", e);
		} 
	}

	public TerminalPersistenceService getTerminalPersistenceService() {
		return terminalPersistenceService;
	}
	public void setTerminalPersistenceService(TerminalPersistenceService terminalPersistenceService) {
		this.terminalPersistenceService = terminalPersistenceService;
	}
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	
	
}
