package de.bosc.timerecording.javafx.hardware;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.TooManyListenersException;

import org.apache.log4j.Logger;

//import com.sun.jna.Library;

import de.bosc.timerecording.javafx.interfaces.RfidReaderInterface;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

public class DeviceReader implements SerialPortEventListener {

    static Logger logger = Logger.getLogger(DeviceReader.class);
    private InputStream inputStream;
    private SerialPort serialPort;
    private DeviceType deviceType;
    private RfidReaderInterface rfidReaderInterface;
	private Robot robot;
    
    public DeviceReader(CommPortIdentifier portId, DeviceType deviceType,RfidReaderInterface rfidReaderInterface) {
    	
        try {
        	this.robot = new Robot();
        	this.rfidReaderInterface = rfidReaderInterface;
        	logger.info("portId:" + (portId != null) + " - " + deviceType.name());
        	this.deviceType = deviceType; 
            serialPort = (SerialPort) portId.open("Voith-ZET-" + deviceType, 2000);
            inputStream = serialPort.getInputStream();
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
            serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
        } catch (PortInUseException e) {
           	logger.error("PortInUseException",e);
        } catch (TooManyListenersException e) {
           	logger.error("TooManyListenersException",e);
		} catch (IOException e) {
           	logger.error("IOException",e);
		} catch (UnsupportedCommOperationException e) {
            logger.error("UnsupportedCommOperationException",e);
        } catch (AWTException e) {
            logger.error("AWTException",e);
		}
	}

	public void serialEvent(SerialPortEvent event) {
		switch(event.getEventType()) {
			case SerialPortEvent.BI:
			case SerialPortEvent.OE:
			case SerialPortEvent.FE:
			case SerialPortEvent.PE:
			case SerialPortEvent.CD:
			case SerialPortEvent.CTS:
			case SerialPortEvent.DSR:
			case SerialPortEvent.RI:
			case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
				break;
			case SerialPortEvent.DATA_AVAILABLE:
        		try {		
        			/* 100 Milisekunden warten damit die Bytes an der Schnittstelle gelesen werden k�nnen*/
        	        try {
        	            Thread.sleep(100);
        	        } catch (InterruptedException e) {logger.error("InterruptedException",e);}
        			logger.info("available bytes: " + inputStream.available() + " --- " + deviceType + " --- " + serialPort.getName());
        			if(inputStream.available() > 0 ) {
                		byte[] readBuffer = new byte[inputStream.available()];
            			inputStream.read(readBuffer);
            			String tagid = new String(readBuffer);
            			tagid = tagid.replaceAll("(\\r|\\n)", "");
            			
        				if(deviceType == DeviceType.RFID_READER ) {
        					rfidReaderInterface.rfidScanned(tagid);
        				}
            			robot.keyPress(KeyEvent.VK_ESCAPE);
            	        robot.keyRelease(KeyEvent.VK_ESCAPE);
        			}
        		} catch (IOException e) {
        			logger.error("IOException",e);
        		} catch( Exception e){        			
        			logger.error("Exception",e);
        		}
        		break;
		}
	}
	
	
	public void closePort(){
		serialPort.removeEventListener();
		serialPort.close();
	}
	
	public enum DeviceType{
		RFID_READER,BARCODE_SCANNER;
	}
	
//	public interface User32 extends Library {
//		 public void SendMessage(int HWND_BROADCAST, int WM_SYSCOMMAND, int SC_MONITORPOWER, int LPARAM);
//		 public void PostMessage(int HWND_BROADCAST, int WM_SYSCOMMAND, int SC_MONITORPOWER, int LPARAM);
//	}

}

