package de.bosc.timerecording.javafx.hardware;

import java.util.Enumeration;

import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.javafx.hardware.DeviceReader.DeviceType;
import de.bosc.timerecording.javafx.interfaces.RfidReaderInterface;
import gnu.io.CommPortIdentifier;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

@SuppressWarnings("restriction")
public class DeviceValidator{
	private Logger logger;

	private Timeline timelinePorts;
	private boolean rfidAvailable = false;
	private boolean hasFirstInit;
	private DeviceReader rfidReader;
	private String rfidreader_comport;
	private RfidReaderInterface rfidReaderInterface;
	
	public DeviceValidator(RfidReaderInterface rfidReaderInterface,String rfidreader_comport,Logger logger) {
		this.logger = logger;
		logger.trace("start DeviceValidator");
		this.rfidreader_comport = rfidreader_comport;
		logger.debug("rfidreader_comport: " + rfidreader_comport);
		
		timelinePorts = new Timeline(new KeyFrame(Duration.millis(2000), new EventHandler<ActionEvent>() {  
		     public void handle(ActionEvent event) {  
		    	 checkComPorts();
		     }  
		}));
		timelinePorts.setCycleCount(Animation.INDEFINITE);  
		timelinePorts.play();
		
	}
	

	private boolean initRFIDReader(){
		boolean rfidReaderOK = false;
		Enumeration portList = CommPortIdentifier.getPortIdentifiers();
		try{
			if(rfidReader != null) rfidReader.closePort();
		}catch(Exception e){}
			
        while (portList.hasMoreElements()) {
        	CommPortIdentifier portId = (CommPortIdentifier) portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (portId.getName().equals(rfidreader_comport)) {
            		rfidReader = new DeviceReader( portId, DeviceType.RFID_READER,rfidReaderInterface);
            		rfidReaderOK = true;
                }
            }
        }
		return rfidReaderOK;
	}
		
	public void checkComPorts(){
		logger.trace("---------- checkComPorts ----------------");
		Enumeration portList = CommPortIdentifier.getPortIdentifiers();
		try {
			/* Kontrolliere ob ComPort des RFID Reader verf�gbar*/
			boolean rfidfound = false;
			boolean barcodescannerfound = false;
			while (portList.hasMoreElements()) {
	        	CommPortIdentifier portId = (CommPortIdentifier) portList.nextElement();
	            if (portId.getName().equals(rfidreader_comport)) {
	            	rfidfound = true;
	            }
            }

			/* Wenn ComPort des RFID-Readers verf�gbar ist zuvor aber nicht
			 * dann informiere GUI �ber Wechsel*/
			if(rfidfound && (!rfidAvailable || !hasFirstInit)){
				logger.trace("--- RFID Reader connection is back");
				if (initRFIDReader()){
					rfidReaderInterface.rfidConnectionBack();
					rfidAvailable = true;					
				}
			}else if(!rfidfound && (rfidAvailable || !hasFirstInit )){
				logger.trace("--- RFID Reader connection lost 1");
				if(rfidReader != null) rfidReader.closePort();
				rfidReaderInterface.rfidConnectionLost();
				rfidAvailable = false;				
			}
			
			hasFirstInit = true;
		} catch (Exception e) {
			logger.error("",e);
		}

	}

	public void closePorts(){
		try{
			if(rfidReader != null) rfidReader.closePort();
		}catch(Exception e){}

	}	
}