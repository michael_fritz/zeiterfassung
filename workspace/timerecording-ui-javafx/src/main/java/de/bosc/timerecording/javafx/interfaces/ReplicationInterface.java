package de.bosc.timerecording.javafx.interfaces;

public interface ReplicationInterface {

	public void replication(boolean canReplicate);
	
}
