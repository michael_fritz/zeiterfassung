package de.bosc.timerecording.javafx.interfaces;

public interface RfidReaderInterface {

	public void rfidScanned(String scannedID);
	public void rfidConnectionLost();
	public void rfidConnectionBack();
}
