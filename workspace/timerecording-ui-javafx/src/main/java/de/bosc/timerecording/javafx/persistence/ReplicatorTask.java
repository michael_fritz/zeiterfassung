package de.bosc.timerecording.javafx.persistence;

import java.util.TimerTask;

import org.apache.log4j.Logger;

import de.bosc.timerecording.javafx.ui.MainPanelControler;
import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.NotesFactory;
import lotus.domino.NotesThread;
import lotus.domino.Session;

public class ReplicatorTask extends TimerTask{
	private Logger logger = Logger.getLogger(this.getClass());

	private Database db;
	private String server;
	private Session session;
	private TerminalPersistenceService terminPersistenceService;
	private MainPanelControler mainPanelControler;
	private boolean isConfig;
	private boolean firstRun = true;

	
	public ReplicatorTask(MainPanelControler mainPanelControler ,Database db,TerminalPersistenceService terminalPersistenceService, boolean isConfig) {
		try {
			this.session = NotesFactory.createSession((String)null, (String)null,terminalPersistenceService.getPassword());
			this.db = session.getDatabase(db.getServer(), db.getFilePath());
			this.terminPersistenceService = terminalPersistenceService;
			this.server = terminalPersistenceService.getServer();
			this.isConfig = isConfig;
			this.mainPanelControler = mainPanelControler;
		} catch (NotesException e) {
			logger.error("",e);
		}
	}
	
	@Override
	public void run() {
		long repl = System.nanoTime();
		try {					
			logger.trace("*********** replicate  ************ ");
			NotesThread.sinitThread();
			logger.trace("--- server: " + server + " --- database: " + db.getTitle());
			try {
				db.replicate(server);
				if(isConfig && !firstRun){
					logger.trace("--- reinit configuration ");
					terminPersistenceService.initConfigServices();					
					terminPersistenceService.getCompanyService().getCompanies();
				}
				mainPanelControler.getFooter().replication(true);
			} catch (NotesException e) {
				logger.error("Terminal offline",e);
				if(isConfig){
					mainPanelControler.getFooter().replication(false);
				}
			}finally{
				firstRun = false;
			}
			NotesThread.stermThread();
		} catch (Exception e) {
			logger.error("NotesException:",e);
		}
		logger.trace("------- duration to replicate: " + (System.nanoTime() -repl)/1000000);
	}	
}
