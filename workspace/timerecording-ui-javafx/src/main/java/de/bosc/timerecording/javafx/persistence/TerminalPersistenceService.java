package de.bosc.timerecording.javafx.persistence;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.controller.domino.DominoDatabaseConfigController;
import de.bosc.timerecording.basic.enumeration.LogLevel;
import de.bosc.timerecording.basic.logging.LoggerDomino;
import de.bosc.timerecording.basic.model.DatabaseConfiguration;
import de.bosc.timerecording.basic.model.YearString;
import lotus.domino.Database;
import lotus.domino.NotesFactory;
import lotus.domino.NotesThread;

public class TerminalPersistenceService extends DominoPersistenceService implements Serializable{
	private static final long serialVersionUID = 1L;

	private LogLevel logLevel = LogLevel.INFO; 
	private String server;
	private String password;
	private String agentLogPath;
	private String databaseConfigPath;
	private String databaseDataPath;
	private long REPLICATION_INTERVALL_DATA;
	private long REPLICATION_INTERVALL_CONFIG;
	private long REPLICATION_INTERVALL_ALOG;
	private String rfidReaderCOMPort;

	
	@Override
	public void replicate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void init() {
		try{
			readTerminalProperties();
			logger.trace("##### startNotesThread NotesSession ######### ");
			NotesThread.sinitThread();
			logger.trace("create NotesSession:" + System.nanoTime());
			session = NotesFactory.createSession((String)null, (String)null,password);
			dbConfig = session.getDatabase("", databaseConfigPath);
			dbData = session.getDatabase("", databaseDataPath);
			dbLog = session.getDatabase("", agentLogPath);
			logger.debug("agentLogPath:" + agentLogPath);
			DominoDatabaseConfigController databaseConfigController = new DominoDatabaseConfigController();
			DatabaseConfiguration databaseConfiguration = databaseConfigController.getDatabaseConfig(this);
			logLevel = databaseConfiguration.getLogLevel();
			logger = new LoggerDomino(getSession(),"",this.getClass().getName(),agentLogPath,logLevel,databaseConfiguration.getLogLevelsClass());
			logger.info("DEFAULT LOG-LEVEL: " + logLevel ,this.getClass());
			logger.info("databaseConfigPath: " + databaseConfigPath + " - dbConfig: " + (dbConfig.isOpen()?dbConfig.getTitle():false),this.getClass());
			logger.info("databaseDataPath: " + databaseDataPath +  " - dbData: " + (dbData.isOpen()?dbData.getTitle():false),this.getClass());
			logger.info("REPLICATION_INTERVALL_DATA: " + REPLICATION_INTERVALL_DATA ,this.getClass());			
			logger.info("REPLICATION_INTERVALL_CONFIG: " + REPLICATION_INTERVALL_CONFIG ,this.getClass());			
			logger.info("REPLICATION_INTERVALL_ALOG: " + REPLICATION_INTERVALL_ALOG ,this.getClass());			
			logger.info("rfidReaderCOMPort: " + rfidReaderCOMPort ,this.getClass());			
			for(Map.Entry<String,LogLevel>entry:databaseConfiguration.getLogLevelsClass().entrySet()){
				logger.info("class: " + entry.getKey() + " = " + entry.getValue(),this.getClass());
			}
			databaseConfiguration = getDatabaseConfig();
		}catch(Exception e){
			logger.error("Eception on init",e);
		}	
	}
	
	private void readTerminalProperties(){
		FileInputStream file=null;
	    try {
	    	Properties terminalProperties = new Properties();
	    	try{
		    	URL root = getClass().getProtectionDomain().getCodeSource().getLocation();
		    	URL propertiesFile = new URL(root, "terminal.properties");
		    	System.out.println("root-terminal.properties: " + propertiesFile.getPath());
			    terminalProperties.load(propertiesFile.openStream());
	    	}catch(Exception e){
			    String path = "./terminal.properties";
			    file = new FileInputStream(path);
			    terminalProperties.load(file);
				logger.error("Cannot read terminal.properties",e);
	    	}
		    this.logLevel = LogLevel.valueOf(terminalProperties.getProperty("notes.loglevel","INFO"));
		    this.password = terminalProperties.getProperty("notes.password","");
		    this.server = terminalProperties.getProperty("notes.server","");
		    this.databaseConfigPath = terminalProperties.getProperty("notes.database.config","");
		    this.databaseDataPath = terminalProperties.getProperty("notes.database.data","");
		    this.agentLogPath = terminalProperties.getProperty("notes.agentlog","");
		    this.REPLICATION_INTERVALL_DATA = Long.parseLong(terminalProperties.getProperty("notes.replication.interval.data","120000"));
		    this.REPLICATION_INTERVALL_CONFIG = Long.parseLong(terminalProperties.getProperty("notes.replication.interval.config","300000"));
		    this.REPLICATION_INTERVALL_ALOG = Long.parseLong(terminalProperties.getProperty("notes.replication.interval.alog","300000"));
		    this.rfidReaderCOMPort = terminalProperties.getProperty("rfidreader.comport","COM5");
		} catch (Exception e) {
			logger.error("",e);
		} finally{
			if(file !=null){
			    try {file.close();} catch (IOException e) {}				
			}
		}
	}
	
	
	@Override
	public Database getDbData(YearString year) {
		return dbData;
	}

	public Database getDbData(){
		return dbData;
	}
	public String getPassword() {
		return password;
	}

	public long getREPLICATION_INTERVALL() {
		return REPLICATION_INTERVALL_CONFIG;
	}

	public long getREPLICATION_INTERVALL_ALOG() {
		return REPLICATION_INTERVALL_ALOG;
	}

	public long getREPLICATION_INTERVALL_DATA() {
		return REPLICATION_INTERVALL_DATA;
	}

	public String getServer() {
		return server;
	}

	public String getRfidReaderCOMPort() {
		return rfidReaderCOMPort;
	}


	public LogLevel getLogLevel() {
		return logLevel;
	}

	public String getAgentLogPath() {
		return agentLogPath;
	}

	@Override
	public void stopService() {
		try{
			logger.trace("##### stopNotesThread NotesSession ######### ");
			NotesThread.stermThread();
		}catch(Exception e){
			logger.error("Eception on stopService",e);
		}		
		
	}


	
}
