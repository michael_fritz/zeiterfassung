package de.bosc.timerecording.javafx.ui;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import de.bosc.timerecording.basic.business.WorktimeDayService;
import de.bosc.timerecording.basic.business.model.WorktimeDay;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.AccountingMonth;
import de.bosc.timerecording.basic.model.AccountingPerson;
import de.bosc.timerecording.basic.model.DayTimeentry;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.AccountingMonthService;
import de.bosc.timerecording.basic.service.AccountingPersonService;
import de.bosc.timerecording.basic.service.TimeentryService;
import de.bosc.timerecording.javafx.ui.component.ButtonNavigation;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

@SuppressWarnings("restriction")
public class Account  extends VBox{
	private MainPanelControler mainPanelControler;
	private Logger logger;
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM | HH:mm");
	public static SimpleDateFormat updateFormatDate = new SimpleDateFormat("dd.MM.yyyy");
	public static SimpleDateFormat updateFormatTime = new SimpleDateFormat("HH:mm");
	private Person person;

	public Account(MainPanelControler mainPanelControler,Person person) {
		this.mainPanelControler = mainPanelControler;
		this.logger = mainPanelControler.getLogger();
		this.person = person;
		this.setAlignment(Pos.CENTER);

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(0,20,0,20));
		vbox.setStyle("-fx-background-color:rgba(0,0,0,0.2)");
		vbox.setSpacing(3);
		VBox.setVgrow(vbox, Priority.SOMETIMES);
		Label fullname = new Label(person.getFullname() + " | " + this.mainPanelControler.getLastScannedId());
		fullname.getStyleClass().add("loginInfoPanelText");
		fullname.setPadding(new Insets(5,0,5,0));
		vbox.getChildren().add(fullname);
		vbox.getChildren().add(getInfoPanel(person));
		vbox.getChildren().add(getTotalAccountInfo(person));
		this.getChildren().add(vbox);
		this.getChildren().add(getBackButton());
		mainPanelControler.getInput().requestFocus();
		
	}
	
	private VBox getInfoPanel(Person person){
		VBox vbox = new VBox();
		vbox.getStyleClass().add("accountInfoPanel");
		vbox.setStyle("-fx-border-width: 1 0 1 0;");
		GridPane gridPane = new GridPane();
		gridPane.setHgap(15);
		gridPane.setVgap(0);
		gridPane.setStyle("-fx-border-color: none");
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(50);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(50);
        gridPane.getColumnConstraints().addAll(col1,col2);
		
		gridPane.add(createLabel("Tages Soll:", HPos.RIGHT,TextAlignment.RIGHT,""),0, 0);		
		gridPane.add(createLabel("Restarbeit:\nHeute", HPos.RIGHT,TextAlignment.RIGHT,"-fx-line-spacing: -5pt;"),0, 1);
		gridPane.add(createLabel("Überstunden:\nHeute", HPos.RIGHT,TextAlignment.RIGHT,"-fx-line-spacing: -5pt;-fx-padding:-5 0 0 0"),0, 2);

		int solltime = 8*60*60;
		String solltimeString = TimeentryService.parseTimeToString(solltime);
		int worktime = mainPanelControler.getTerminalPersistenceService().getTimeentryService().getDayWorktime(person.getPersonalNumber(), TimeentryService.dateFormat.format(new Date()));
		try {
			WorktimeDayService worktimeDayService = new WorktimeDayService(mainPanelControler.getTerminalPersistenceService(), person,new MonthString(new Date()));
			WorktimeDay dayWorktime = worktimeDayService.getDayWorktime(mainPanelControler.getTerminalPersistenceService(), new Date());
			if(dayWorktime != null){
				worktime = dayWorktime.getIstWorktime() + dayWorktime.getAdditionDay() + dayWorktime.getAdditionKinds() - dayWorktime.getPausetimeGeneral() - dayWorktime.getBreakTime();
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		
		int resttime = solltime - worktime;
		String resttimeString = TimeentryService.parseTimeToString(Math.abs(resttime)); 
		
		gridPane.add(createLabel( solltimeString +  " h", HPos.LEFT,TextAlignment.LEFT,""),1, 0);
		gridPane.add(createLabel((resttime>0?resttimeString:0) + " h", HPos.LEFT,TextAlignment.LEFT,"-fx-padding: 3 0 0 0;"),1, 1);
		gridPane.add(createLabel((resttime<0?resttimeString:0) +" h", HPos.LEFT,TextAlignment.LEFT,"-fx-padding: -2 0 0 0;"),1, 2);

		
		vbox.getChildren().add(gridPane);
		return vbox;
	}
	
	public Label createLabel(String caption, HPos gridAlign, TextAlignment textalign, String style){
		Label l = new Label(caption);
		l.setTextAlignment(textalign);
		l.setStyle(style);
		l.getStyleClass().add("loginInfoPanelText");
		GridPane.setHgrow(l,  Priority.SOMETIMES);
		GridPane.setHalignment(l, gridAlign);
		GridPane.setValignment(l,VPos.TOP);
		return l;
	}
	
	private VBox getTotalAccountInfo(Person person){
		VBox vbox = new VBox();
		vbox.setPadding(new Insets(0));
		VBox.setVgrow(vbox, Priority.SOMETIMES);
		vbox.getStyleClass().add("accountInfoPanel");
		
		GridPane gridPane = new GridPane();
		gridPane.setTranslateY(-5);
		gridPane.setPadding(new Insets(0));
		gridPane.setHgap(15);
		gridPane.setVgap(0);
		gridPane.setStyle("-fx-border-color: none");
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(50);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(50);
        gridPane.getColumnConstraints().addAll(col1,col2);

        AccountingPerson accountingPerson = new AccountingPersonService(mainPanelControler.getTerminalPersistenceService()).getAccountingPerson(person);
		
		Label l = createLabel("Stand " +  updateFormatDate.format(accountingPerson.getLastUpdate()) + " um " + updateFormatTime.format(accountingPerson.getLastUpdate()), HPos.LEFT,TextAlignment.LEFT,"-fx-font-size: 18px;");
		GridPane.setColumnSpan(l, 2);
		gridPane.add(l,0, 0);
		gridPane.add(createLabel("Resturlaub:\n2017", HPos.RIGHT,TextAlignment.RIGHT,"-fx-line-spacing: -5pt;"),0, 1);
		gridPane.add(createLabel("Überstunden:\nMonat", HPos.RIGHT,TextAlignment.RIGHT,"-fx-line-spacing: -5pt;-fx-padding:-5 0 0 0"),0, 2);
		gridPane.add(createLabel("Überstunden:\nGesamt", HPos.RIGHT,TextAlignment.RIGHT,"-fx-line-spacing: -5pt;-fx-padding:-5 0 0 0"),0, 3);
		gridPane.add(createLabel("Arbeitsstunden:\nMonat", HPos.RIGHT,TextAlignment.RIGHT,"-fx-line-spacing: -5pt;-fx-padding:-5 0 0 0"),0, 4);

		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		
		Map<MonthString,AccountingMonth> accountingMonths = new AccountingMonthService(mainPanelControler.getTerminalPersistenceService()).getAccountingMonthsYear(person, new YearString(new Date()));
		AccountingMonth accountingMonth = new AccountingMonth();
		if(accountingMonths.containsKey(new MonthString(new Date()))){
			accountingMonth = accountingMonths.get(new MonthString(new Date()));			
		}
		
		gridPane.add(createLabel((accountingPerson.getDaysHoliday()) +  " Tage", HPos.LEFT,TextAlignment.LEFT,"-fx-padding: 3 0 0 0;"),1, 1);
		gridPane.add(createLabel(decimalFormat.format(accountingMonth.getOvertime()/3600.0) + "h", HPos.LEFT,TextAlignment.LEFT,"-fx-padding: 0 0 0 0;"),1, 2);
		gridPane.add(createLabel(decimalFormat.format(accountingPerson.getOvertime()/3600.0) + "h", HPos.LEFT,TextAlignment.LEFT,"-fx-padding: -2 0 0 0;"),1, 3);
		gridPane.add(createLabel(decimalFormat.format(accountingMonth.getIst()/3600.0) + "h", HPos.LEFT,TextAlignment.LEFT,"-fx-padding: -2 0 0 0;"),1, 4);

		vbox.getChildren().add(gridPane);
		return vbox;
	}
	
	private ButtonNavigation getBackButton(){
		ButtonNavigation btn_back = new ButtonNavigation(FontAwesomeIcon.ARROW_CIRCLE_LEFT ,"Zurück",null);
	    btn_back.getButton().setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
            	mainPanelControler.login(person);;
            }
        });
	    return btn_back;		
	}
}
