package de.bosc.timerecording.javafx.ui;

import de.bosc.timerecording.basic.logging.Logger;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class Error  extends VBox{
	private MainPanelControler mainPanelControler;
	private Logger logger;
	
	public Error(MainPanelControler mainPanelControler, String message) {
		this.mainPanelControler = mainPanelControler;
		this.logger = mainPanelControler.getLogger();
		this.setAlignment(Pos.CENTER);
		this.getChildren().add(getInfoPanel(message));
		this.getChildren().add(mainPanelControler.getLogoutButton());
	}

	private VBox getInfoPanel(String message){
		VBox vbox = new VBox();
		this.setAlignment(Pos.CENTER);
		VBox.setVgrow(vbox, Priority.SOMETIMES);
		vbox.setId("errorPanel");

		Label label_message = new Label(message);
		label_message.getStyleClass().add("errorPanelText");
		vbox.getChildren().add(label_message);
		return vbox;
	}

}
