package de.bosc.timerecording.javafx.ui;

import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Device;
import de.bosc.timerecording.basic.model.Person;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

@SuppressWarnings("restriction")
public class Footer extends HBox{

	private Label label_offline = new Label("Terminal offline!");
	private Label label_scanner = new Label("ID-Scanner offline!");
	private Button btn_exit = new Button();
	private Logger logger;
	
	private boolean online = false;
	private boolean scanner = false;
	private Device device;
	private MainPanelControler mainPanelControler;
	
	public Footer(MainPanelControler mainPanelControler, Device device) {
		this.logger = mainPanelControler.getLogger();
		this.device = device;
		this.mainPanelControler = mainPanelControler;
		this.setId("footer");
		this.setWidth(Double.MAX_VALUE);
		Footer.setHgrow(label_offline, Priority.ALWAYS);
		btn_exit = getExitButton();
		refreshPane(null);
	}
	
	public void refreshPane(Person person){
		this.getChildren().remove(0, this.getChildren().size());
		this.setStyle( !online || !scanner?"-fx-background-color: rgba(255,0,0,0.2);":"-fx-background-color: rgba(0,0,0,0.2);");
		
		if(online){
			label_offline.setStyle("-fx-text-fill: white");
			label_offline.setText("Terminal online!");
		}else{
			label_offline.setStyle("-fx-text-fill: white");			
			label_offline.setText("Terminal offline!");			
		}

		VBox vbox = new VBox();
		vbox.getChildren().add(label_offline);
		HBox.setHgrow(vbox, Priority.SOMETIMES);
		this.getChildren().add(vbox);
		if(!scanner){
			vbox.getChildren().add(label_scanner);
		}
		mainPanelControler.getLogger().debug("Person: " + (person == null?null:person.getPrimaryKey()) + " DeviceManager: " + device.getDeviceManager(),this.getClass());
		if(person != null && device.getDeviceManager().contains(person.getPrimaryKey())){
			mainPanelControler.getLogger().debug("Person: is device manager", this.getClass());
			this.getChildren().add(btn_exit);
		}
		
	}
	
	private Button getExitButton(){
		Button btn_exit = new Button();
		GlyphsDude.setIcon(btn_exit,FontAwesomeIcon.POWER_OFF);
		btn_exit.setId("buttonexit");
		btn_exit.setPrefSize(30.0, 30.0);
		btn_exit.setAlignment(Pos.CENTER);
		btn_exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
            	exitApp();
            }
        });
	    return btn_exit;
	}
	
	protected void exitApp() {
		Stage stage = (Stage) btn_exit.getScene().getWindow();
		stage.close();
		this.mainPanelControler.stopReplication();
	}
	
	public void rfidConnection(boolean rfidScannerActive) {
		this.scanner = rfidScannerActive;
	}
	
	public void replication(boolean canReplicate){
		this.online = canReplicate;
	}
	
}
