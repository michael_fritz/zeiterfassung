package de.bosc.timerecording.javafx.ui;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.bosc.timerecording.basic.model.Device;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class Header extends HBox {
	
	public static SimpleDateFormat timeFormat = new SimpleDateFormat("dd.MM.yyyy | HH:mm:ss");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	
	private Label label_clock = new Label();
	private Label label_Company = new Label("");
	private String companyName;
	private Device device;

	public Header(Device device) {
		
		this.device = device;
		this.setSpacing(1.0);
		this.setId("header");
		buildComponents();
	}
	
	private void buildComponents(){
		
		label_clock.setId("textclock");
		refreshClock();
		
		companyName = (device.getCompany() != null?device.getCompany().getName():"");
		
		label_Company.setId("textcompany");
		label_Company.setText(companyName);
		
		VBox p = new VBox();
		p.getChildren().add(label_Company);
		HBox.setHgrow(p, Priority.SOMETIMES);
		this.getChildren().add(p);

		this.getChildren().add(label_clock);
	}

	public void setHeaderLabel(String headline){
		label_Company.setText(companyName + headline);		
	}
	
	public void refreshClock(){
		label_clock.setText(timeFormat.format(new Date()));		
	}

	public Device getDevice() {
		return device;
	}
}
