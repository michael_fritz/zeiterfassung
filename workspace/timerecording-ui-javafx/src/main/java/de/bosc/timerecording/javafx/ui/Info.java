package de.bosc.timerecording.javafx.ui;

import java.text.SimpleDateFormat;
import java.util.Map;

import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Notification;
import de.bosc.timerecording.basic.model.Person;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class Info  extends VBox{
	private MainPanelControler mainPanelControler;
	private Logger logger;
	private Person person;
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM | HH:mm");

	public Info(MainPanelControler mainPanelControler,Person person, Map<String,Notification> notifications) {
		this.mainPanelControler = mainPanelControler;
		this.person = person;
		this.logger = mainPanelControler.getLogger();
		this.logger.debug("create Info Panel");
		this.setAlignment(Pos.CENTER);
		this.getChildren().add(getInfoPanel(notifications));
		this.getChildren().add(getLogoutButton());
	}

	@SuppressWarnings("restriction")
	private VBox getInfoPanel(Map<String,Notification> notifications){
		VBox vbox = new VBox();
		this.setAlignment(Pos.CENTER);
		VBox.setVgrow(vbox, Priority.SOMETIMES);
		vbox.setId("errorPanel");

		for(Notification notification:notifications.values()){
			Label label_creator = new Label(dateFormat.format(notification.getDate()) + " | " + notification.getCreator());
			label_creator.getStyleClass().add("infoPanelText");
			label_creator.setStyle("-fx-font-size:12pt");
			vbox.getChildren().add(label_creator);			
			
			Label label_message = new Label(notification.getMessage());
			label_message.getStyleClass().add("infoPanelTextLine");
			label_creator.setStyle("-fx-font-size:12pt");
			vbox.getChildren().add(label_message);			
		}

		return vbox;
	}


	private Button getLogoutButton(){
		Button btn_logout = new Button();
	    GlyphsDude.setIcon(btn_logout, FontAwesomeIcon.SIGN_OUT );
	    btn_logout.getStyleClass().add("buttonDefault");
	    btn_logout.setAlignment(Pos.CENTER);
	    btn_logout.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
               mainPanelControler.login(person);
            }
        });
	    return btn_logout;		
	}

}
