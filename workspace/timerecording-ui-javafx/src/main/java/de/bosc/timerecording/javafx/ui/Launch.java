package de.bosc.timerecording.javafx.ui;

import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.javafx.persistence.TerminalPersistenceService;
import de.jensd.fx.glyphs.GlyphsStyle;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

@SuppressWarnings("restriction")
public class Launch extends Application {
	private TerminalPersistenceService terminalPersistenceService = new TerminalPersistenceService();
	private Logger logger;
	private Scene scene;
	private MainPanelControler mainPanelControler;
	
	public static void main(String[] args) {
		try{
			launch(args);						
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		logger = terminalPersistenceService.getLogger();
		logger.info("###### Launch start #######",this.getClass());
		terminalPersistenceService.getLocations();

		this.mainPanelControler = new MainPanelControler(terminalPersistenceService);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setFullScreen(true);
		stage.setTitle("Voith Zeiterfasser");
		stage.setFullScreenExitHint("");
		stage.setFullScreenExitKeyCombination(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN));

		
		
		HBox hbox = new HBox();
		hbox.setStyle("-fx-background-color:rgb(230,230,230,0)");
		hbox.setAlignment(Pos.CENTER);

		HBox hboxHeader = new HBox();
		hboxHeader.getChildren().add(mainPanelControler.getHeader());
		hboxHeader.setSpacing(0.0);
		HBox.setHgrow(mainPanelControler.getHeader(),Priority.ALWAYS);

		BorderPane borderPane = new BorderPane();
		borderPane.setMaxWidth(500.0);
		borderPane.setStyle("-fx-border-width:0;-fx-border-color: rgba(200,200,200,0.5)");
		borderPane.setCenter(this.mainPanelControler);
		borderPane.setBottom(mainPanelControler.getFooter());
		borderPane.setTop(hboxHeader);
		BorderPane.setMargin(hboxHeader, new Insets(10,-4,10,10));
		BorderPane.setMargin(this.mainPanelControler, new Insets(0,10,0,10));

		hbox.getChildren().add(borderPane);
		hbox.setId("pane");
		
		
		if(terminalPersistenceService.getRfidReaderCOMPort().equals("KEYBOARD")){
			hboxHeader.getChildren().add(mainPanelControler.getInput());
			mainPanelControler.getFooter().rfidConnection(true);
		}else{
			// TODO initialize rfidScanner
		}
		
		HBox.setHgrow(borderPane, Priority.ALWAYS);
		
		scene = new Scene(hbox, 500, 800);

		scene.getStylesheets().addAll(GlyphsStyle.DEFAULT.getStylePath());
		scene.getStylesheets().add("/css/launchui.css");
		stage.setScene(scene);
		
		stage.show();
	}


}
