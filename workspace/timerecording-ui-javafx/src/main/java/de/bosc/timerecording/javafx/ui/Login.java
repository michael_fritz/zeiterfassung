package de.bosc.timerecording.javafx.ui;

import java.util.Date;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.model.TimeentryKind;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.TimeentryService;
import de.bosc.timerecording.javafx.ui.component.ButtonNavigation;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

@SuppressWarnings("restriction")
public class Login extends VBox{

	private MainPanelControler mainPanelControler;
	private VBox buttonPanel = new VBox();
	private Person person;
	private Timeentry timeentry;
	private Logger logger;
	private WorktimeSetting worktimeSetting;

	
	public Login(MainPanelControler mainPanelControler, Person person, final Timeentry timeEntry) {
		this.mainPanelControler = mainPanelControler;
		this.logger = mainPanelControler.getLogger();
		this.person = person;
		this.timeentry = timeEntry;
		
		if(this.timeentry == null){
			this.timeentry = new Timeentry();
			this.timeentry.setTimeentryKindId("0");
			this.timeentry.setEndTime(-1);
			this.timeentry.setStartTime(-1);
			this.timeentry.setPersonalNumber(person.getPersonalNumber());
		}

		this.setAlignment(Pos.CENTER);
		try {
			this.worktimeSetting = mainPanelControler.getTerminalPersistenceService().getWorktimeSettingService().getWorktimeSettingForDate(person, new Date());
		} catch (PersistenceException e) {
			logger.error("Error getWorktimeSettingForDate from PersistenceService", e);
		}
		
		VBox.setVgrow(buttonPanel, Priority.SOMETIMES);
		buttonPanel.setId("loginButtonPanel");
		buttonPanel.setAlignment(Pos.CENTER);

		buildComponent();
	}
	
	private void buildComponent(){
		repaintButtonPanel(false);
		this.getChildren().add(getInfoPanel());
		this.getChildren().add(buttonPanel);
	}
	
	private VBox getInfoPanel(){
		VBox vbox = new VBox();
		vbox.setPrefHeight(300.0);
		vbox.setId("loginInfoPanel");
		Label fullname = new Label(person.getFullname() + " | " + this.mainPanelControler.getLastScannedId());
		Label department = new Label("Abteilung:");
		fullname.getStyleClass().add("loginInfoPanelText");
		department.getStyleClass().add("loginInfoPanelText");
		VBox vboxline = new VBox();
		VBox.setMargin(vboxline, new Insets(20, 0,20,0));
		vboxline.setId("loginInfoPanelLine"); 
       
		vbox.getChildren().add(fullname);
		vbox.getChildren().add(department);
		vbox.getChildren().add(vboxline);
		vbox.getChildren().add(getWorktimeInfo());
		return vbox;
	}

	private void repaintButtonPanel(boolean displayTimeentryKind){
		buttonPanel.getChildren().clear();
		if(displayTimeentryKind){
			HBox hbox = new HBox();
			for(TimeentryKind timeentryKind:this.worktimeSetting.getTimeentryKinds().values()){
				logger.debug("+++++++ TimeentryKind: " + timeentryKind.getName() + " - " + (timeentryKind.getSelectable().equals("1")) + " - " + timeentryKind.getPrimaryKey() + " - " + timeentry.getTimeentryKindId() ,this.getClass());
				if(timeentryKind.getSelectable().equals("1")){
					ButtonNavigation buttonNavigation =  getTimeentryKindButton(timeentryKind);
					if(timeentryKind.getPrimaryKey().equals(this.timeentry.getTimeentryKindId())){
						logger.debug("++++++++++++ TimeentryKind.setActive: " +  timeentryKind.getPrimaryKey() ,this.getClass() );
						buttonNavigation.setActive();
					}
					hbox.getChildren().add(buttonNavigation);
				    HBox.setHgrow(buttonNavigation, Priority.SOMETIMES);
				}
			}
			buttonPanel.getChildren().add(hbox);
			buttonPanel.getChildren().add(getBackButtonTimeentryKinds());
		}else{
			logger.trace("repaintButtonPanel.timeentryKindId: " + timeentry.getTimeentryKindId(),this.getClass());
			TimeentryKind timeentryKind = null;
			if(this.worktimeSetting != null){
				timeentryKind = this.worktimeSetting.getTimeentryKinds().get(timeentry.getTimeentryKindId());				
			}
			if(timeentryKind == null){
				try {
					timeentryKind = mainPanelControler.getTerminalPersistenceService().getTimeentryKindService().getTimeentryKinds().get("0");
				} catch (Exception e) {
					logger.error("Error getTimeentryKinds from PersistenceService", e);
				}
				logger.trace("repaintButtonPanel.timeentryKindId timeentryKind is null --- set to: " + timeentryKind,this.getClass());
			}
			logger.trace("repaintButtonPanel.timeentryKind: " + (timeentryKind != null),this.getClass());
			if(timeentry.getStartTime() == -1){
				ButtonNavigation buttonNavigation = new ButtonNavigation(FontAwesomeIcon.PLAY, "Start ZE", timeentryKind.getNotation());
				buttonNavigation.getButton().setOnAction(new EventHandler<ActionEvent>() {
		            public void handle(ActionEvent event) {
		            	start();
		            }
		        });
				buttonPanel.getChildren().add(buttonNavigation);				
			}else{
				ButtonNavigation buttonNavigation = new ButtonNavigation(FontAwesomeIcon.PAUSE, "Stop ZE", timeentryKind.getNotation());
				buttonNavigation.getButton().setOnAction(new EventHandler<ActionEvent>() {
		            public void handle(ActionEvent event) {
		            	stop();
		            }
		        });
				buttonPanel.getChildren().add(buttonNavigation);								
			}

			HBox hbox = new HBox();
			hbox.setAlignment(Pos.BASELINE_CENTER);
			ButtonNavigation buttonNavigation = new ButtonNavigation(FontAwesomeIcon.LINE_CHART, "Konto ZE", "");
			buttonNavigation.getButton().setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent event) {
	            	mainPanelControler.showAccount(person);
	            }
	        });
			hbox.getChildren().add(buttonNavigation);
			HBox.setHgrow(buttonNavigation, Priority.SOMETIMES);

			buttonNavigation = getLogoutButton();
			hbox.getChildren().add(buttonNavigation);
			HBox.setHgrow(buttonNavigation, Priority.SOMETIMES);
			
			buttonNavigation = new ButtonNavigation(FontAwesomeIcon.REFRESH, "Arbeitsart", "");
			buttonNavigation.getButton().setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent event) {
	            	repaintButtonPanel(true);
	            }
	        });

			hbox.getChildren().add(buttonNavigation);
			HBox.setHgrow(buttonNavigation, Priority.SOMETIMES);
			buttonPanel.getChildren().add(hbox);			
		}
	}

	
	private ButtonNavigation getTimeentryKindButton(final TimeentryKind timeentryKind){
		ButtonNavigation buttonNavigation = new ButtonNavigation(FontAwesomeIcon.REFRESH,timeentryKind.getNotation(),null);
		buttonNavigation.getButton().setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
            	timeentry.setTimeentryKindId(timeentryKind.getPrimaryKey());
            	repaintButtonPanel(false);
            }
        });
	    return buttonNavigation;		
	}
	
	private GridPane getWorktimeInfo(){
		GridPane gridPane = new GridPane();
		gridPane.setTranslateY(30);
		gridPane.setPadding(new Insets(0));
		gridPane.setHgap(15);
		gridPane.setVgap(0);
		gridPane.setStyle("-fx-border-color: none");
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(50);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(50);
        gridPane.getColumnConstraints().addAll(col1,col2);
		gridPane.add(createLabel("Start", HPos.RIGHT,TextAlignment.RIGHT,"-fx-line-spacing: -5pt;"),0, 0);
		gridPane.add(createLabel("Arbeit seit", HPos.RIGHT,TextAlignment.RIGHT,"-fx-line-spacing: -5pt;"),0, 1);
		
		if(timeentry.getStartTime() != -1){
			String starttime = mainPanelControler.getTerminalPersistenceService().getTimeentryService().parseTimeToString(timeentry.getStartTime()); 		
			gridPane.add(createLabel( starttime, HPos.LEFT,TextAlignment.LEFT,""),1, 0);
			String worktime = mainPanelControler.getTerminalPersistenceService().getTimeentryService().getWorktimeOpenTimeentry(timeentry); 		
			gridPane.add(createLabel( worktime, HPos.LEFT,TextAlignment.LEFT,""),1, 1);			
		}

		//VBox.setVgrow(gridPane, Priority.SOMETIMES);
        return gridPane;
	}
	
	public Label createLabel(String caption, HPos gridAlign, TextAlignment textalign, String style){
		Label l = new Label(caption);
		l.setTextAlignment(textalign);
		l.setStyle(style);
		l.getStyleClass().add("loginInfoPanelText");
		GridPane.setHgrow(l,  Priority.SOMETIMES);
		GridPane.setHalignment(l, gridAlign);
		GridPane.setValignment(l,VPos.TOP);
		return l;
	}
	
	private ButtonNavigation getBackButtonTimeentryKinds(){
		ButtonNavigation btn_back = new ButtonNavigation(FontAwesomeIcon.ARROW_CIRCLE_LEFT ,"Zurück",null);
	    btn_back.getButton().setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
            	repaintButtonPanel(false);
            }
        });
	    return btn_back;		
	}
	

	private ButtonNavigation getLogoutButton(){
		ButtonNavigation btn_logout = new ButtonNavigation(FontAwesomeIcon.SIGN_OUT ,"Beenden",null);
	    btn_logout.getButton().setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
               mainPanelControler.logout();
            }
        });
	    return btn_logout;		
	}
	
	
	public void start(){
		try{
			timeentry.setNewEntry(true);
			timeentry.setStartTime(TimeentryService.parseDate(new Date()));
			timeentry.setEndTime(-1);
			timeentry.setStartDate(new DateString(new Date()));
			timeentry.setEndDate(new DateString(new Date()));
			timeentry.setTimeDuration("3");
			timeentry.setDeviceDocIdStart(mainPanelControler.getDevice().getPrimaryKey());
			timeentry.setPersonalNumber(person.getPersonalNumber());
			mainPanelControler.getTerminalPersistenceService().getTimeentryService().saveTimeEntry(timeentry);
	
			mainPanelControler.logout();
		}catch(Exception e){
			logger.error("Error stop timeentry", e);
			// TODO show error information
		}
	}
	
	public void stop(){
		try{
			DateString currentDate = new DateString(new Date());
			/*
			 * Wenn das Datum der Startzeit nicht mehr mit dem heutigen Datum übereinstimmt
			 * muss der Zeiteintrag mit 24 Uhr geschlossen und ein weiterer mit 
			 * 0 Uhr - Endezeit angelegt werden.
			 */
//			if(!currentDate.equals(timeentry.getStartDate())){
//				timeentry.setEndTime(TimeentryService.MAX_ENDTIME);
//				timeentry.setDeviceDocIdEnd(mainPanelControler.getDevice().getPrimaryKey());
//				mainPanelControler.getTerminalPersistenceService().getTimeentryService().saveTimeEntry(timeentry);
//				timeentry = timeentry.clone();
//				timeentry.setStartTime(0);
//				timeentry.setDeviceDocIdStart(mainPanelControler.getDevice().getPrimaryKey());
//			}
			timeentry.setNewEntry(true);
			timeentry.setDeviceDocIdEnd(mainPanelControler.getDevice().getPrimaryKey());
			timeentry.setEndTime(TimeentryService.parseDate(new Date()));
			timeentry.setEndDate(currentDate);
			mainPanelControler.getTerminalPersistenceService().getTimeentryService().saveTimeEntry(timeentry);
			mainPanelControler.logout();
		}catch(Exception e){
			logger.error("Error stop timeentry", e);
			// TODO show error information
		}

	}
}
