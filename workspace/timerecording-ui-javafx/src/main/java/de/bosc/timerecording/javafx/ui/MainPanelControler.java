package de.bosc.timerecording.javafx.ui;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Map;
import java.util.Timer;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Device;
import de.bosc.timerecording.basic.model.Notification;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.javafx.persistence.ReplicatorTask;
import de.bosc.timerecording.javafx.persistence.TerminalPersistenceService;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

@SuppressWarnings("restriction")
public class MainPanelControler extends StackPane{
	
	private TerminalPersistenceService terminalPersistenceService;
	private Logger logger;
	private Timer timer;
	private Timeline timeline;
	private ReplicatorTask replTaskConfig;
	private ReplicatorTask replTaskData;
	private ReplicatorTask replTaskLog;
	private Footer footer;
	private Header header;
	private Person person;
	private TextField input = buildInputTextField();
	private Startpage startpage;
	private String locationName = "";
	private String lastScannedId = "";
	private Device device = null;
	
	public MainPanelControler(TerminalPersistenceService terminalPersistenceService) {
		this.terminalPersistenceService = terminalPersistenceService;
		this.logger = terminalPersistenceService.getLogger();
		String macAddress = getMacAddress();		
		try {
			logger.info("Device Mac-Adresse: " + macAddress,this.getClass());
			System.out.println("Device Mac-Adresse: " + macAddress);
			device = terminalPersistenceService.getDeviceService().getDevice(macAddress);
		} catch (PersistenceException e) {
			logger.error("Error getDevices from PersistenceService",e);
		}
		if(device == null){
			logger.warn("Device not found in configuration");
			device = new Device();
		}else if(device.getLocation() == null){
			logger.warn("Location for device not found in configuration");
		}else{
			locationName = device.getLocation().getName();
		}
		logger.info("Device:" + device.getDeviceName() + " - Manager: " + device.getDeviceManager(),this.getClass());			
		header = new Header(device);
		footer = new Footer(this,device);
		startpage = new Startpage();
		setId("mainpanelcontroler");
		getChildren().add(startpage);
		initReplication();
		input.requestFocus();
	}
	
	public void showAccount(Person person){
		this.getChildren().remove(0, getChildren().size());
		getChildren().add(new Account(this,person));
	}
	
	
	public void login(Person person){
		this.person = person;
		this.getChildren().remove(0, getChildren().size());

		if(person == null){
			logger.info("*********** Error: Person not found",this.getClass());
			getChildren().add(new Error(this,"Person mit Tag-ID: " + lastScannedId + " nicht gefunden"));
			getChildren().add(new NumberBlock(this));
		}else{
			logger.info("*********** LOGIN Person: " + person.getFullname(),this.getClass());
			Map<String, Notification> mapNotifications = null;
			try {
				mapNotifications = terminalPersistenceService.getNotificationService().getPersonNotifications(person.getPrimaryKey());
				if(mapNotifications != null){
					for(Notification notification:mapNotifications.values()){
						notification.setNotRead(false);
					}					
				}
			} catch (PersistenceException e) {
				logger.error("Error getNotifications from PersistenceService", e);
			}
			if( (1 != 1)     && mapNotifications != null && mapNotifications.size() > 0){
				getChildren().add(new Info(this,person, mapNotifications));
			}else{				
				try {
					Timeentry timeentry = terminalPersistenceService.getTimeentryService().getOpenTimeentry(person);
					logger.debug("timeentry: " + (timeentry!=null?timeentry.getStartDate():"no open timeentry"),this.getClass());
					getChildren().add(new Login(this,person,timeentry));
				} catch (Exception e) {
					logger.error("Fehler bei Ermittlung von bestehenden Zeiteinträgen: ", e);
					getChildren().add(new Error(this,"Fehler bei Ermittlung von bestehenden Zeiteinträgen: " + Logger.getStackTrace(e)));
				}
			}
		}
		this.header.setHeaderLabel(" | " + locationName);
	}

	public void logout(){
		logger.debug("*********** LOGOUT Person: " + (person != null?person.getFullname():""),this.getClass());
		this.person = null;
		this.getChildren().remove(0, getChildren().size());
		startpage = new Startpage();
		getChildren().add(startpage);
		input.requestFocus();
		this.header.setHeaderLabel("");
	}

	public Button getLogoutButton(){
		Button btn_logout = new Button();
	    GlyphsDude.setIcon(btn_logout, FontAwesomeIcon.SIGN_OUT );
	    btn_logout.getStyleClass().add("buttonDefault");
	    btn_logout.setAlignment(Pos.BASELINE_CENTER);
	    btn_logout.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
            	logout();
            }
        });
	    return btn_logout;		
	}

	public TextField buildInputTextField(){
		final TextField input = new TextField("");
		input.setMaxWidth(1.0);
		input.setStyle("-fx-background-color:rgba(0,0,0,0);-fx-border-width:0px");
		input.setOnKeyPressed(new EventHandler<KeyEvent>() {
	        public void handle(KeyEvent event) {
	            if(event.getCode().equals(KeyCode.ENTER)) {
	            	String rfidcode = input.getText();
	            	input.setText("");
	            	rfidScanned(rfidcode);
	            }
	        }
	    });		
		return input;
	}
	
	public void rfidScanned(String scannedId) {
		logger.debug("ID: " + scannedId ,this.getClass());
		Person person = null;
		try {
			person = terminalPersistenceService.getPersonService().getPersonsByTagId().get(scannedId);
		} catch (PersistenceException e) {
			logger.error("Error getPerson from PersistenceService", e);
		}
		this.setLastScannedId(scannedId);
		this.login(person);
	}
	public void startTimeEntry(Person person){
		
	}
	
	public void stopTimeEntry(Timeentry timeEntry){
		
	}
	
	public String getMacAddress(){
		String macAddress = "";
		try{
			InetAddress ip;
			ip = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);

			byte[] mac = network.getHardwareAddress();

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			macAddress = sb.toString();
			logger.info("MAC address : " + macAddress,this.getClass());
		}catch(Exception e){
			e.getStackTrace();
		}
		return macAddress;
	}
	
	public void initReplication(){
		logger.info("*********** initReplication **********" ,this.getClass());
		timer = new Timer();
		replTaskConfig = new ReplicatorTask(this,terminalPersistenceService.getDbConfig(),terminalPersistenceService,true);
		replTaskData = new ReplicatorTask(this,terminalPersistenceService.getDbData(),terminalPersistenceService,false);
		replTaskLog = new ReplicatorTask(this,terminalPersistenceService.getDbLog(),terminalPersistenceService,false);
		timer.schedule(replTaskConfig, 0, terminalPersistenceService.getREPLICATION_INTERVALL());
		timer.schedule(replTaskData, 0, terminalPersistenceService.getREPLICATION_INTERVALL_DATA());
		timer.schedule(replTaskLog, 0, terminalPersistenceService.getREPLICATION_INTERVALL_ALOG());
		timeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {  
		     public void handle(ActionEvent event) {  
		    	 footer.refreshPane(person);
		    	 header.refreshClock();
		     }  
		}));
		timeline.setCycleCount(Animation.INDEFINITE);  
		timeline.play();
	}
	
	public void stopReplication(){
		logger.info("*********** stopReplication ************" ,this.getClass());
		replTaskLog.cancel();
		replTaskConfig.cancel();
		replTaskData.cancel();
		timer.cancel();
	}

	public TerminalPersistenceService getTerminalPersistenceService() {
		return terminalPersistenceService;
	}


	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Footer getFooter() {
		return footer;
	}

	public Header getHeader() {
		return header;
	}

	public String getLastScannedId() {
		return lastScannedId;
	}

	public void setLastScannedId(String lastScannedId) {
		this.lastScannedId = lastScannedId;
	}

	public Startpage getStartpage() {
		return startpage;
	}

	public TextField getInput() {
		return input;
	}

	public Device getDevice() {
		return device;
	}

//	public void rfidScanned(String scannedID) {
//		logger.info("scanned id: " + scannedID);
//	}
//
//	public void rfidConnectionLost() {
//		logger.warn("rfidConnectionLost");
//	}
//
//	public void rfidConnectionBack() {
//		logger.info("rfidConnectionBack");
//	}
	
	
}
