package de.bosc.timerecording.javafx.ui;

import java.util.ArrayList;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Person;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class NumberBlock extends HBox {
	
	final static int BUTTON_SIZE = 70;
	private PasswordField passwordfield;
	private MainPanelControler mainPanelControler;
	private Person person;
	private Label user = new Label();
	private Logger logger;
	
	
	public NumberBlock(MainPanelControler mainPanelControler) {
		this.mainPanelControler = mainPanelControler;
		this.logger = mainPanelControler.getLogger();
		this.setMaxHeight(500);
		this.setId("numberblock");
		this.setPadding(new Insets(10));
		this.setSpacing(10);
		this.setStyle("-fx-translate-y: -50px;	");
		passwordfield = BuildPasswordField();
		GridPane.setHalignment(user, HPos.LEFT);
		user.setStyle("-fx-text-fill: white;-fx-font-size:14pt;");
		user.setPadding(new Insets(0,5,0,5));
		
		this.getChildren().add(buildListViewPerson());
		
		GridPane gridPane = new GridPane();
		gridPane.setHgap(5);
		gridPane.setVgap(5);
		gridPane.setStyle("-fx-border-color: none");
		VBox vboxuser = new VBox();
		vboxuser.getChildren().add(user);
		vboxuser.getStyleClass().add("numberBlockUser");

		VBox vboxpasscode = new VBox();
		vboxpasscode.getChildren().add(passwordfield);
		vboxpasscode.getStyleClass().add("numberBlockUser");
		
		Label passcodeHint = new Label("Geben Sie Ihren PIN-Code ein.");
		passcodeHint.setWrapText(true);
		passcodeHint.setStyle("-fx-text-fill: white;-fx-font-size:14pt;");
		
		VBox vbox = new VBox();
		vbox.getChildren().add(passcodeHint);
		vbox.getChildren().add(vboxuser);
		vbox.getChildren().add(vboxpasscode);
		vbox.setSpacing(10);

		GridPane.setColumnSpan(vbox, 3);
		gridPane.add(vbox,0, 0);
		
		for(int i = 1;i<4;i++){
			for(int j=0;j<3;j++){
				final String number = "" + (((i-1)*3)+j+1);
				gridPane.add(buildNumberButton(number), j, i);
			}
		}
		Button btn_backspace = buildNumberButton("");
		btn_backspace.setPrefSize(BUTTON_SIZE, BUTTON_SIZE);
		GlyphsDude.setIcon(btn_backspace, FontAwesomeIcon.STEP_BACKWARD);
		gridPane.add(btn_backspace, 0, 4);
		btn_backspace.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
            	removePasscodeNumber();
            }
        });
		
		gridPane.add(buildNumberButton("0"), 1, 4);
		gridPane.add(buildNumberButton(""), 2, 4);

		this.getChildren().add(gridPane);
	}
	
	private VBox buildListViewPerson(){
		ArrayList<Person> persons = new ArrayList<Person>();
		try {
			for(Person person:mainPanelControler.getTerminalPersistenceService().getPersonService().getPersonsActiveFullname().values()){
				persons.add(person);
			}
		} catch (PersistenceException e) {
			logger.error("Error getPersonService from PersistenceService", e);
		}
		ObservableList<Person> myObservableList = FXCollections.observableList(persons);

		final ListView<Person> listView = new ListView<Person>();
		listView.setItems(myObservableList);
		listView.getStyleClass().add("personlistView");
		listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Person>() {
			public void changed(ObservableValue<? extends Person> observable,Person oldValue, Person newValue) {
				person = newValue;
				passwordfield.setText("");
				user.setText(person.getFullname());
				
			}
        });
		listView.setOnMouseReleased(new EventHandler<MouseEvent>() {
	        public void handle(MouseEvent event) {
	            logger.debug("list item selected",this.getClass());
	            mainPanelControler.getInput().requestFocus();
	        }
	    });
		VBox.setVgrow(listView,Priority.ALWAYS);
		
		VBox vbox = new VBox();
		vbox.setStyle("-fx-background-color: rgba(0,0,0,0.2);");
		Label headline = new Label("W�hlen Sie Ihren Namen:");
		VBox.setVgrow(headline, Priority.ALWAYS);
		VBox.setMargin(headline, new Insets(0,10,0,10));
		headline.getStyleClass().add("personlistViewHeadline");
		vbox.getChildren().add(headline);
		vbox.getChildren().add(listView);
		HBox.setHgrow(vbox,Priority.ALWAYS);
		return vbox;
	}
	
	private Button buildNumberButton(final String number){
		Button b = new Button(number);
		b.getStyleClass().add("numberblockButton");
		b.setPrefSize(BUTTON_SIZE, BUTTON_SIZE);
		if(!number.equals("")){
			b.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent event) {
	            	addPasscodeNumber(number);
	            }
	        });			
		}
		return b;		
	}
	
	private PasswordField BuildPasswordField(){
		PasswordField passwordfield = new PasswordField();
		passwordfield.getStyleClass().add("passwordfield");
		passwordfield.setAlignment(Pos.CENTER);
		passwordfield.setPadding(new Insets(0));
		passwordfield.setEditable(false);
		return passwordfield;
	}
	
	private void addPasscodeNumber(String number){
		if(person != null && passwordfield.getText().length()<4 ){
			passwordfield.setText(passwordfield.getText() + number);
		}
		passwordfield.setStyle(passwordfield.getText().length()==4?"-fx-text-fill: red;":"-fx-text-fill: white;");
		checkPassword();
	}
	
	public void removePasscodeNumber(){
	   	 int l = passwordfield.getText().length();
	   	 if(!passwordfield.getText().equals("")){
	   		passwordfield.setText(passwordfield.getText().substring(0,--l));
	   		checkPassword();		    		 
	   	 }
		passwordfield.setStyle(passwordfield.getText().length()==4?"-fx-text-fill: red;":"-fx-text-fill: white;");
	}
	
	private void checkPassword(){
    	if(person != null && person.getPasscode().equals(passwordfield.getText())){
    		person.getRfidTagsTime().add(mainPanelControler.getLastScannedId());
    		try {
				mainPanelControler.getTerminalPersistenceService().savePerson(person);
			} catch (PersistenceException e) {
				e.printStackTrace();
			}
    		mainPanelControler.login(person);
    	}		
	}
}
