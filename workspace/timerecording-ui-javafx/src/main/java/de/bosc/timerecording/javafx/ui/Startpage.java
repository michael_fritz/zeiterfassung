package de.bosc.timerecording.javafx.ui;

import javafx.scene.layout.BorderPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.layout.StackPane;
import javafx.geometry.Pos;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;

@SuppressWarnings("restriction")
public class Startpage extends BorderPane {

	private Label text_login = new Label();
	private Label text_creditcard = new Label(); 
	private Label text_loginhint = new Label(); 
	private Label text_Caret_down = new Label();
	
	private Button btn_login = new Button();
	private Button btn_backspace = new Button();
	
	public Startpage() {
		buildComponents();
		StackPane vbox = new StackPane();
		vbox.setAlignment(Pos.CENTER);
		vbox.getChildren().add(text_creditcard);
		vbox.getChildren().add(text_loginhint);
		vbox.getChildren().add(text_Caret_down);
		
		this.setBottom(vbox);
	}
	
	private void buildComponents(){
		text_login.setText("Press button for login");
		text_login.setAlignment(Pos.CENTER);
		text_login.setId("textlogin");

		text_loginhint.setText("Scan ID card");
		text_loginhint.setId("textloginhint");
		text_loginhint.setAlignment(Pos.CENTER);

		GlyphsDude.setIcon(btn_login,FontAwesomeIcon.SIGN_IN);
		GlyphsDude.setIcon(text_Caret_down, FontAwesomeIcon.ANGLE_DOWN);
		GlyphsDude.setIcon(text_creditcard, FontAwesomeIcon.CREDIT_CARD);
		text_creditcard.setId("textcreditcard");
		text_Caret_down.setId("textcaretdown");
	}
	
}
