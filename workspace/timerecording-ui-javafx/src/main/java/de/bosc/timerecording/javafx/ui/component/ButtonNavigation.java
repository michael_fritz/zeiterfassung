package de.bosc.timerecording.javafx.ui.component;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class ButtonNavigation extends VBox{

	private Button button = new Button();
	
	public ButtonNavigation(FontAwesomeIcon fontAwesomeIcon,String firstLabel, String secondLabel) {
		this.setAlignment(Pos.BOTTOM_CENTER);
		
	    GlyphsDude.setIcon(button, fontAwesomeIcon );
	    button.getStyleClass().addAll("buttonNavigation");
	    button.setAlignment(Pos.CENTER);

	    VBox vbox = new VBox();
	    vbox.getStyleClass().add("vboxbuttonNavigation");
	    vbox.setPrefWidth(100.0);
	    if(firstLabel != null){
		    Label first = new Label(firstLabel);
		    first.getStyleClass().add("firstLabelbuttonNavigation");
		    vbox.getChildren().add(first);	    	
	    }
	    if(secondLabel != null){
		    Label second  = new Label(secondLabel);
		    second.getStyleClass().add("secondLabelbuttonNavigation");
		    vbox.getChildren().add(second);
	    }
	    HBox hbox = new HBox();
	    hbox.setPrefWidth(100.0);
	    hbox.setAlignment(Pos.BASELINE_CENTER);
	    hbox.getChildren().add(vbox);
	    this.getChildren().add(button);
	    this.getChildren().add(hbox);
	}

	public Button getButton() {
		return button;
	}
	
	public void setActive(){
		button.setStyle("-icons-color: white;");
	}
	
}
