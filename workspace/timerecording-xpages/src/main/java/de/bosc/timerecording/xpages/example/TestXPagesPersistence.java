package de.bosc.timerecording.xpages.example;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.WorktimeCalculationService;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.xpages.persistence.XPagesPersistenceService;

public class TestXPagesPersistence {

	private static XPagesPersistenceService persistenceService;
	private Logger logger;
	
	public static void main(String[] args) {
		TestXPagesPersistence testPersistenceService = new TestXPagesPersistence(new XPagesPersistenceService());
		//terminalPersistenceService.getDevices();
		//testPersistenceService.createTimeEntry();
		//testPersistenceService.getTimeEntriesMonth();
		testPersistenceService.writeAccountingMonth();
	}
	public TestXPagesPersistence(XPagesPersistenceService persistenceService) {
		this.persistenceService = persistenceService;
		this.logger = persistenceService.getLogger();
	}
	
	public void writeAccountingMonth(){
		String personalNumber = "1";
	//	personalNumber = "MFRZ-AR3MC7";
		//personalNumber = "120518"; // Elsa Kleiner
		
		try {
			Person person = persistenceService.getPersonService().getPerson(personalNumber);
			WorktimeCalculationService service = new WorktimeCalculationService();
			service.recalculatePerson(persistenceService, person);
//			WorktimeMonthService service = new WorktimeMonthService();
//			WorktimeMonth worktimeMonth = service.createWorktimeMonth(terminalPersistenceService,person,new Date());
//			service.calculateMonthForPerson(worktimeMonth);
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(new Date());
//			cal.add(Calendar.MONTH,-1);
			//service.calculateMonthForPerson(cal.getTime());
		} catch (PersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
