package de.bosc.timerecording.xpages.persistence;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import lotus.domino.Database;
import lotus.domino.NotesFactory;
import lotus.domino.NotesThread;

import com.ibm.domino.xsp.module.nsf.NotesContext;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.controller.domino.DominoDatabaseConfigController;
import de.bosc.timerecording.basic.controller.domino.DominoPublicHolidayController;
import de.bosc.timerecording.basic.enumeration.LogLevel;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.logging.LoggerDomino;
import de.bosc.timerecording.basic.model.DatabaseConfiguration;
import de.bosc.timerecording.basic.model.PublicHoliday;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.AccountingMonthService;
import de.bosc.timerecording.basic.service.LocationService;
import de.bosc.timerecording.basic.service.PersonService;
import de.bosc.timerecording.basic.service.TimeentryKindService;
import de.bosc.timerecording.basic.service.TimeentryService;
import de.bosc.timerecording.basic.service.WorktimeSettingService;


public class XPagesPersistenceService extends DominoPersistenceService{

	private static final long serialVersionUID = 1L;
	private LogLevel logLevel = LogLevel.DEBUG; 
	private String server;
	private String agentLogPath;
	private String databaseConfigPath;
	private String databaseDataPath;
	private String error;
	private transient Database dbExports; 
	private transient DominoPublicHolidayController dominoPublicHolidayController;
	
	@Override
	public void init() {
		try{
			readTerminalProperties();
			logger.trace("##### startNotesThread NotesSession ######### ");
			NotesThread.sinitThread();
			logger.trace("create NotesSession:" + System.nanoTime());
			session = NotesFactory.createSession((String)null, (String)null,"und2straif");
			dbConfig = session.getDatabase("", databaseConfigPath);
			dbData = session.getDatabase("", databaseDataPath);
			dbLog = session.getDatabase("", agentLogPath);
			logger.debug("agentLogPath:" + agentLogPath);
			server = "";
		    
			logger.trace("create NotesSession:" + System.nanoTime());
			DominoDatabaseConfigController databaseConfigController = new DominoDatabaseConfigController();
			DatabaseConfiguration databaseConfiguration = databaseConfigController.getDatabaseConfig(this);
			agentLogPath = databaseConfiguration.getPathDbLog();
			databaseConfigPath = dbConfig.getFilePath();
			databaseDataPath = databaseConfiguration.getPathDbData();
			startYear = databaseConfiguration.getStartYear();
			dbData = session.getDatabase(server,databaseDataPath + ".nsf");
			dbExports = session.getDatabase(server,databaseDataPath + "Exports.nsf");
			dbDatas = new HashMap<String,Database>();
			dbLog = session.getDatabase(server, agentLogPath);
			logger = new LoggerDomino(getSession(),server,this.getClass().getName(),agentLogPath,logLevel,databaseConfiguration.getLogLevelsClass());
			
			StringBuffer sb = new StringBuffer("-------------- log levels   --------------------------------------\n");
			for(Map.Entry<String,LogLevel> levels: databaseConfiguration.getLogLevelsClass().entrySet()){
				sb.append("---class:" + levels.getKey() + " = " + levels.getValue()+ "\n");
			}
			logger.trace(sb.toString() ,this.getClass());
			logger.trace("user:" + session.getEffectiveUserName() ,this.getClass());
			logger.trace("server:" + server ,this.getClass());
			logger.trace("agentLogPath:" + databaseConfiguration.getPathDbLog() ,this.getClass());
			logger.trace("databaseDataPath:" + databaseDataPath,this.getClass());
			logger.trace("startYear:" + startYear);
			logger.trace("logLevel:" + logLevel ,this.getClass());
			logger.trace("databasePath:" + databaseConfigPath + " - dbConfig: " + (dbConfig.isOpen()?dbConfig.getTitle():false) + " - dbData: " + (dbData.isOpen()?dbData.getTitle():false),this.getClass());

			Calendar cal = Calendar.getInstance();
			Integer nextYear = cal.get(Calendar.YEAR)+1;
			logger.trace("nextYear:" + nextYear);
			while(startYear <= nextYear){
				Database db = session.getDatabase(dbConfig.getServer(), databaseDataPath + startYear + ".nsf");
				dbDatas.put("" + startYear, db);
				logger.trace("dbData: " +  startYear + ": " + server + " - " + databaseDataPath + startYear + ".nsf " + (db.isOpen()),this.getClass());
				startYear++;
			}
			startYear = databaseConfiguration.getStartYear();

			this.worktimeSettingService = new WorktimeSettingService(this);
			this.personService = new PersonService(this);
			this.accountingMonthService = new AccountingMonthService(this);
			this.locationService = new LocationService(this);
			this.timeentryKindService = new TimeentryKindService(this);
			this.worktimeSettingService = new WorktimeSettingService(this);
			this.timeentryService = new TimeentryService(this);

			this.dominoPublicHolidayController = new DominoPublicHolidayController();
		}catch(Exception e){
			error = Logger.getStackTrace(e);
			if(logger != null){
				logger.error("", e);
			}else{
				e.printStackTrace();				
			}
		}	
		
	}

	private void readTerminalProperties(){
		FileInputStream file=null;
	    try {
	    	Properties terminalProperties = new Properties();
	    	try{
		    	URL root = getClass().getProtectionDomain().getCodeSource().getLocation();
		    	URL propertiesFile = new URL(root, "terminal.properties");
		    	System.out.println("root-terminal.properties: " + propertiesFile.getPath());
			    terminalProperties.load(propertiesFile.openStream());
	    	}catch(Exception e){
			    String path = "./terminal.properties";
			    file = new FileInputStream(path);
			    terminalProperties.load(file);
				logger.error("Cannot read terminal.properties",e);
	    	}
		    this.logLevel = LogLevel.valueOf(terminalProperties.getProperty("notes.loglevel","INFO"));
		    this.server = terminalProperties.getProperty("notes.server","");
		    this.databaseConfigPath = terminalProperties.getProperty("notes.database.config","");
		    this.databaseDataPath = terminalProperties.getProperty("notes.database.data","");
		    this.agentLogPath = terminalProperties.getProperty("notes.agentlog","");
		} catch (Exception e) {
			logger.error("",e);
		} finally{
			if(file !=null){
			    try {file.close();} catch (IOException e) {}				
			}
		}
	}	
	
	public List<PublicHoliday> getPublicHolidays(String year){
		List<PublicHoliday> listPublicHolidays = dominoPublicHolidayController.getPublicHolidays(this, new YearString(year));		
		return listPublicHolidays;
	}
	
	@Override
	public Database getDbData(YearString year) {
		YearString thisYear = new YearString(new Date());
		return thisYear.equals(year)?getDbData(): super.getDbData(year);
	}

	public Database getDbExports() {
		return dbExports;
	}
	public String getAgentLogPath() {
		return agentLogPath;
	}
	public LogLevel getLogLevel() {
		return logLevel;
	}
	public String getServer() {
		return server;
	}
	public String getDatabaseConfigPath() {
		return databaseConfigPath;
	}
	public String getDatabaseDataPath() {
		return databaseDataPath;
	}
	public String getError() {
		return error;
	}
}
