package de.bosc.timerecording.basic.business;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.bosc.timerecording.basic.controller.domino.DominoAccountingMonthController;
import de.bosc.timerecording.basic.controller.domino.DominoAccountingPersonController;
import de.bosc.timerecording.basic.controller.domino.DominoAccountingYearController;
import de.bosc.timerecording.basic.controller.domino.DominoCompanyController;
import de.bosc.timerecording.basic.controller.domino.DominoDatabaseConfigController;
import de.bosc.timerecording.basic.controller.domino.DominoDeviceController;
import de.bosc.timerecording.basic.controller.domino.DominoLocationController;
import de.bosc.timerecording.basic.controller.domino.DominoNotificationController;
import de.bosc.timerecording.basic.controller.domino.DominoPersonController;
import de.bosc.timerecording.basic.controller.domino.DominoPublicHolidayController;
import de.bosc.timerecording.basic.controller.domino.DominoTimeentryController;
import de.bosc.timerecording.basic.controller.domino.DominoTimeentryKindController;
import de.bosc.timerecording.basic.controller.domino.DominoWorktimeSettingController;
import de.bosc.timerecording.basic.model.AccountingMonth;
import de.bosc.timerecording.basic.model.AccountingPerson;
import de.bosc.timerecording.basic.model.AccountingYear;
import de.bosc.timerecording.basic.model.Company;
import de.bosc.timerecording.basic.model.DatabaseConfiguration;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.Department;
import de.bosc.timerecording.basic.model.Device;
import de.bosc.timerecording.basic.model.Location;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Notification;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.PublicHoliday;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.model.TimeentryKind;
import de.bosc.timerecording.basic.model.VersionControl;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.model.YearString;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.Item;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

public abstract class DominoPersistenceService extends PersistenceService implements Serializable{


	private static final long serialVersionUID = 1L;
	public static final SimpleDateFormat updateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public final String HISTORY_ITEM_NAME = "updateHistory_";
	protected transient Session session;
	protected transient Database dbConfig;
	protected transient Map<String,Database> dbDatas;
	protected transient Database dbData;
	protected transient Database dbLog;
	protected transient DatabaseConfiguration databaseConfiguration;

	
	public DatabaseConfiguration getDatabaseConfig() {
		DominoDatabaseConfigController service = new DominoDatabaseConfigController();
		return service.getDatabaseConfig(this);
	}

	@Override
	public List<Location> getLocations() {
		DominoLocationController service = new DominoLocationController();
		return service.getLocations(this);
	}

	@Override
	public List<Company> getCompanies() {
		DominoCompanyController service = new DominoCompanyController();
		return service.getCompanies(this);
	}

	@Override
	public List<Department> getDepartments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Device> getDevices() {
		DominoDeviceController service = new DominoDeviceController();
		return service.getDevices(this);
	}
	
	@Override
	public List<Notification> getNotifications(){
		DominoNotificationController service = new DominoNotificationController();
		return service.getNotifications(this);
	}
	
	@Override
	public Map<MonthString, AccountingMonth> getAccountingMonthsYear(Person person,YearString year) {
		DominoAccountingMonthController controller = new DominoAccountingMonthController();
		return controller.getAccountingMonthsYear(this, person,year);
	}
	@Override
	public List<AccountingMonth> getAccountingMonths(MonthString month) {
		DominoAccountingMonthController controller = new DominoAccountingMonthController();
		return controller.getAccountingMonths(this, month);
	}

	@Override
	public Map<YearString, AccountingYear> getAccountingYears(Person person) {
		DominoAccountingYearController controller = new DominoAccountingYearController();
		return controller.getAccountingYears(this, person);
	}

	@Override
	public boolean saveAccountingYear(AccountingYear accountingYear) throws PersistenceException {
		DominoAccountingYearController controller = new DominoAccountingYearController();
		return controller.saveAccountingYear(this, accountingYear);
	}
	@Override
	public void replicate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stopService() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public List<WorktimeSetting> getWorktimeSettings(String primaryKey) {
		DominoWorktimeSettingController service = new DominoWorktimeSettingController();
		return service.getWorktimeSettings(this, primaryKey);
	}

	@Override
	public List<WorktimeSetting> getWorktimeSettings() {
		DominoWorktimeSettingController service = new DominoWorktimeSettingController();
		return service.getWorktimeSettings(this);
	}

	public void setVersionControlFields(VersionControl versionControl){
		versionControl.setUpdateDate(updateFormat.format(new Date()));
		try {
			versionControl.setUpdatedBy(session.getCommonUserName());
		} catch (NotesException e) {
			versionControl.setUpdatedBy("n.n.");
			logger.error("error while getting current username", e);
		}
	}
	
	public void writeVersionControl(Document doc,String version) throws NotesException{
		try{
			List<String> listHistory = getObjectHistory(doc);
			doc.replaceItemValue(HISTORY_ITEM_NAME + (listHistory.size()+1), version);			
		}catch(Exception e){
			logger.error("error while write VersionControl", e);
		}

	}

	public Document getDocumentByKey(String key){		
		Document doc = null;
		try {
			View view = dbConfig.getView("($docId)");
			doc = view.getDocumentByKey(key);
		} catch (NotesException e) {
			logger.error("Error DominoPersistenceService.getDocumentByKey ", e);
		}
		return doc;
	}
	
	public Document getDocumentByKey(Database db,String key){		
		Document doc = null;
		try {
			View view = db.getView("($docId)");
			doc = view.getDocumentByKey(key);
		} catch (NotesException e) {
			logger.error("Error DominoPersistenceService.getDocumentByKey ", e);
		}
		return doc;
	}
	
	@Override
	public List<PublicHoliday> getPublicHolidaysYear(YearString year) {
		DominoPublicHolidayController dominoHolidayController = new DominoPublicHolidayController();		
		return dominoHolidayController.getPublicHolidays(this, year);
	}

	@Override
	public List<TimeentryKind> getTimeentryKinds() {
		DominoTimeentryKindController service = new DominoTimeentryKindController();
		return service.getTimeentryKinds(this);
	}
	
	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Database getDbConfig() {
		return dbConfig;
	}

	public void setDbConfig(Database dbConfig) {
		this.dbConfig = dbConfig;
	}

	public Database getDbData(YearString year) {
		return dbDatas.get(year.toString());
	}
	public Database getDbData() {
		return dbData;
	}

	public Map<String, Database> getDbDatas() {
		return dbDatas;
	}

	public Database getDbLog() {
		return dbLog;
	}

	public void setDbLog(Database dbLog) {
		this.dbLog = dbLog;
	}
	
	@Override
	public AccountingPerson getAccountingPerson(Person person) {
		DominoAccountingPersonController controller = new DominoAccountingPersonController();		
		return controller.getAccountingPerson(this, person);
	}

	@Override
	public boolean saveAccountingPerson(AccountingPerson accountingPerson) throws PersistenceException {
		DominoAccountingPersonController controller = new DominoAccountingPersonController();		
		return controller.saveAccountingPerson(this, accountingPerson);
	}

	@Override
	public WorktimeSetting getWorktimeSetting(String unid) {
		DominoWorktimeSettingController controller = new DominoWorktimeSettingController();
		return controller.getWorktimeSetting(this, unid);
	}

	@Override
	public List<Person> getPersons() {
		DominoPersonController controller = new DominoPersonController();
		return controller.getPersons(this);
	}	
	
	@Override
	public void savePerson(Person person) throws PersistenceException {
		DominoPersonController service = new DominoPersonController();
		service.savePerson(this, person);
	}

	@Override
	public List<Timeentry> getOpenTimeentries(String personalNumber) throws PersistenceException {
		DominoTimeentryController service = new DominoTimeentryController();
		List<Timeentry> personTimeEntries = new ArrayList<Timeentry>();
		List<Timeentry> timeEntries =  getOpenTimeentries();
		for (Timeentry timeentry : timeEntries) {
			if(timeentry.getPersonalNumber().equals(personalNumber)){
				personTimeEntries.add(timeentry);
			}
		}
		return personTimeEntries;
	}

	@Override
	public List<Timeentry> getOpenTimeentries() throws PersistenceException {
		DominoTimeentryController service = new DominoTimeentryController();
		return service.getOpenTimeentries(this);
	}

	@Override
	public List<Timeentry> getTimeentries(String personalNumber, String date) {
		DominoTimeentryController service = new DominoTimeentryController();
		return service.getTimeentries(this, personalNumber, date);
	}	
	
	@Override
	public Map<DateString,List<Timeentry>> getTimeentriesMonth(String personalNumber, MonthString month) {
		DominoTimeentryController service = new DominoTimeentryController();
		return service.getTimeentriesMonth(this, personalNumber, month);
	}	
	
	@Override
	public Map<DateString, List<Timeentry>> getTimeentriesMonthDeleted(String personalNumber, MonthString month) {
		DominoTimeentryController service = new DominoTimeentryController();
		return service.getTimeentriesMonthDeleted(this, personalNumber, month);
	}

	@Override
	public Timeentry getTimeentry(String unid,YearString year) {
		DominoTimeentryController service = new DominoTimeentryController();
		return service.getTimeentry(this, unid,year);
	}

	@Override
	public void saveTimeentry(Timeentry timeentry) throws PersistenceException {
		DominoTimeentryController service = new DominoTimeentryController();
		service.saveTimeentry(this, timeentry);	
	}

	@Override
	public void saveTimeentryKind(TimeentryKind timeentryKind) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveLocation(Location location) {
		DominoLocationController service = new DominoLocationController();
		service.saveLocation(this, location);
	}

	@Override
	public void saveCompany(Company company) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveDepartment(Department department) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveDevice(Device device) throws PersistenceException {
		DominoDeviceController service = new DominoDeviceController();
		service.saveDevice(this, device);
		
	}

	@Override
	public void saveWorktimeSetting(WorktimeSetting worktimeSetting) {
		DominoWorktimeSettingController service = new DominoWorktimeSettingController();
		service.saveWorktimeSetting(this, worktimeSetting);
	}

	@Override
	public void saveHoliday(PublicHoliday holiday) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveNotification(Notification notification) throws PersistenceException {
		DominoNotificationController service = new DominoNotificationController();
		service.saveNotificatio(this, notification);
	}

	
	@Override
	public boolean saveAccountingMonth(AccountingMonth accountingMonth) throws PersistenceException {
		DominoAccountingMonthController service = new DominoAccountingMonthController();
		return service.saveAccountingMonth(this, accountingMonth);
	}

	/**
	 * liest aus dem Dokumnet eine Liste von Strings mit den serialierten Objekten
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 */
	public List<String> getObjectHistory(Document doc){
		List<String> listHistoryObjects = new ArrayList<String>();
		int i = 1;
		try {
			Item item = doc.getFirstItem(HISTORY_ITEM_NAME + i);
			while(item != null){
				String entry = item.getValueString();
				listHistoryObjects.add(entry);
				i++;
				item = doc.getFirstItem(HISTORY_ITEM_NAME + i);
			}
		} catch (NotesException e) {
			logger.error("Error DominoPersistenceService.getObjectHistory ", e);
		}
		
		return listHistoryObjects;
	}
}
