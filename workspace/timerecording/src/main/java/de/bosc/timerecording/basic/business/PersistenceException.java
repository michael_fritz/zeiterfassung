package de.bosc.timerecording.basic.business;

public class PersistenceException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public PersistenceException(String msg) {
		super(msg);
	}
	
}
