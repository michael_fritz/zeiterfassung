package de.bosc.timerecording.basic.business;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.logging.LoggerDefault;
import de.bosc.timerecording.basic.model.AccountingMonth;
import de.bosc.timerecording.basic.model.AccountingPerson;
import de.bosc.timerecording.basic.model.AccountingYear;
import de.bosc.timerecording.basic.model.Company;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.Department;
import de.bosc.timerecording.basic.model.Device;
import de.bosc.timerecording.basic.model.Location;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Notification;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.PublicHoliday;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.model.TimeentryKind;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.AccountingMonthService;
import de.bosc.timerecording.basic.service.CompanyService;
import de.bosc.timerecording.basic.service.DepartmentService;
import de.bosc.timerecording.basic.service.DeviceService;
import de.bosc.timerecording.basic.service.LocationService;
import de.bosc.timerecording.basic.service.NotificationService;
import de.bosc.timerecording.basic.service.PersonService;
import de.bosc.timerecording.basic.service.TimeentryKindService;
import de.bosc.timerecording.basic.service.TimeentryService;
import de.bosc.timerecording.basic.service.WorktimeSettingService;

public abstract class PersistenceService {
	protected Logger logger;
	
	protected LocationService locationService;
	protected DeviceService deviceService;
	protected CompanyService companyService;
	protected DepartmentService departmentService;
	protected PersonService personService;
	protected TimeentryService timeentryService;
	protected TimeentryKindService timeentryKindService;
	protected WorktimeSettingService worktimeSettingService;
	protected NotificationService notificationService;
	protected AccountingMonthService accountingMonthService;
	protected Integer startYear;

	
	abstract public List<Person> getPersons() throws PersistenceException;
	abstract public List<Location> getLocations() throws PersistenceException;
	abstract public List<Company> getCompanies() throws PersistenceException;	
	abstract public List<Department> getDepartments() throws PersistenceException;
	abstract public List<Device> getDevices() throws PersistenceException;
	abstract public List<TimeentryKind> getTimeentryKinds();
	abstract public List<Timeentry> getTimeentries(String key,String date) throws PersistenceException;
	abstract public Map<DateString,List<Timeentry>> getTimeentriesMonth(String key,MonthString month);
	abstract public Map<DateString,List<Timeentry>> getTimeentriesMonthDeleted(String key,MonthString month);
	abstract public List<Timeentry> getOpenTimeentries(String key) throws PersistenceException;
	abstract public List<Timeentry> getOpenTimeentries() throws PersistenceException;
	abstract public Timeentry getTimeentry(String unid,YearString year);
	abstract public List<WorktimeSetting> getWorktimeSettings(String primaryKey) throws PersistenceException;
	abstract public List<WorktimeSetting> getWorktimeSettings();	
	abstract public WorktimeSetting getWorktimeSetting(String unid);
	abstract public List<PublicHoliday> getPublicHolidaysYear(YearString year);
	abstract public List<Notification> getNotifications() throws PersistenceException;
	abstract public Map<MonthString,AccountingMonth> getAccountingMonthsYear(Person person,YearString year);
	abstract public List<AccountingMonth> getAccountingMonths(MonthString month);
	abstract public boolean saveAccountingMonth(AccountingMonth accountingMonth) throws PersistenceException;
	abstract public Map<YearString,AccountingYear> getAccountingYears(Person person);
	abstract public boolean saveAccountingYear(AccountingYear accountingYear) throws PersistenceException;
	abstract public AccountingPerson getAccountingPerson(Person person);
	abstract public boolean saveAccountingPerson(AccountingPerson accountingPerson) throws PersistenceException;
	abstract public void replicate();
	abstract public void stopService();
	abstract public void savePerson(Person person) throws PersistenceException;
	abstract public void saveTimeentry(Timeentry timeentry) throws PersistenceException;
	abstract public void saveTimeentryKind(TimeentryKind timeentryKind) throws PersistenceException;
	abstract public void saveLocation(Location location) throws PersistenceException;
	abstract public void saveCompany(Company company) throws PersistenceException;
	abstract public void saveDepartment(Department department) throws PersistenceException;
	abstract public void saveDevice(Device device) throws PersistenceException;
	abstract public void saveWorktimeSetting(WorktimeSetting worktimeSetting) throws PersistenceException;
	abstract public void saveHoliday(PublicHoliday holiday) throws PersistenceException;
	abstract public void saveNotification(Notification notification) throws PersistenceException;
	
	abstract protected void init();
	
	public PersistenceService() {
		try{
			logger = new LoggerDefault();
			this.init();
			initConfigServices();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void initConfigServices(){
		this.locationService = new LocationService(this);
		this.personService = new PersonService(this);
		this.departmentService = new DepartmentService(this);
		this.deviceService = new DeviceService(this);
		this.companyService = new CompanyService(this);
		this.timeentryService = new TimeentryService(this);
		this.timeentryKindService = new TimeentryKindService(this);
		this.worktimeSettingService = new WorktimeSettingService(this);
		this.notificationService = new NotificationService(this);
		this.accountingMonthService = new AccountingMonthService(this);
	}

	public Integer getStartYear() {
		return startYear;
	}
	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	synchronized public LocationService getLocationService() {
		return locationService;
	}
	synchronized public DeviceService getDeviceService() {
		return deviceService;
	}
	synchronized public CompanyService getCompanyService() {
		return companyService;
	}
	synchronized public DepartmentService getDepartmentService() {
		return departmentService;
	}
	synchronized public PersonService getPersonService() {
		return personService;
	}
	synchronized public TimeentryService getTimeentryService() {
		return timeentryService;
	}
	synchronized public TimeentryKindService getTimeentryKindService() {
		return timeentryKindService;
	}
	synchronized public WorktimeSettingService getWorktimeSettingService() {
		return worktimeSettingService;
	}
	public NotificationService getNotificationService() {
		return notificationService;
	}
    public AccountingMonthService getAccountingMonthService() {
		return accountingMonthService;
	} 
	/** Read the object from Base64 string. */
   @SuppressWarnings("unchecked")
   public Object deserializeObjectfromString( String content, Class clazz ) throws IOException , ClassNotFoundException {
		ObjectMapper objectMapper = new ObjectMapper();
		Object o = objectMapper.readValue(content, clazz);
        return o;
   }

    /** Write the object to a Base64 string. */
	public String serializeObjectToString( Object o) {
    	String result = "";
    	try{
    		ObjectMapper objectMapper = new ObjectMapper();
    		result = objectMapper.writeValueAsString(o);
    	}catch(Exception e){
    		result = Logger.getStackTrace(e);
    	}
        return result;
    }
}
