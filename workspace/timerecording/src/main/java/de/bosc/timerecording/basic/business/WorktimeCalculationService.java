package de.bosc.timerecording.basic.business;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.bosc.timerecording.basic.business.model.WorktimeMonth;
import de.bosc.timerecording.basic.business.model.WorktimePerson;
import de.bosc.timerecording.basic.business.model.WorktimeYear;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.AccountingMonth;
import de.bosc.timerecording.basic.model.AccountingPerson;
import de.bosc.timerecording.basic.model.AccountingYear;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.PublicHoliday;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.AccountingMonthService;
import de.bosc.timerecording.basic.service.AccountingPersonService;
import de.bosc.timerecording.basic.service.AccountingYearService;
import de.bosc.timerecording.basic.service.PersonService;

/**
 * @author MiFr
 *
 */
public class WorktimeCalculationService{

	
	public void timeentryUpdate(PersistenceService persistenceService,Timeentry timeentry){
		Logger logger = persistenceService.getLogger();
		try{
			PersonService personService = new PersonService(persistenceService); 
			DateString date = timeentry.getStartDate();
			Person person = personService.getPerson(timeentry.getPersonalNumber());
			WorktimeMonthService service = new WorktimeMonthService();
			WorktimeMonth worktimeMonth = service.createWorktimeMonth(persistenceService,person,date.getJavaDate());
			worktimeMonthUpdate(persistenceService,worktimeMonth);
		}catch(Exception e){
			logger.error("" ,e);
		}
	}
	

	
	public void worktimeSettingLocationUpdate(PersistenceService persistenceService,WorktimeSetting worktimeSetting){
		recalculateAllPerson(persistenceService);
	}

	public void publicHolidayUpdate(PersistenceService persistenceService,PublicHoliday publicHoliday){
		recalculateAllPerson(persistenceService);
	}
	
	/**
	 * @param persistenceService
	 * @param persons
	 */
	public void recalculatePersonList(PersistenceService persistenceService, List<Person> persons){
		try {
			for (Person person : persons) {
				recalculatePerson( persistenceService, person);							
			}
		} catch (Exception e) {
			persistenceService.getLogger().error("", e);
		}
	}

	
	/**
	 * @param persistenceService
	 */
	public void recalculateAllPerson(PersistenceService persistenceService){
		try {
			List<Person> persons = persistenceService.getPersons();
			recalculatePersonList(persistenceService, persons);
		} catch (PersistenceException e) {
			persistenceService.getLogger().error("", e);
		}
	}

	/**
	 * @param persistenceService
	 * @param person
	 */
	public void recalculatePerson(PersistenceService persistenceService,Person person){
		WorktimeMonthService service = new WorktimeMonthService();
		Calendar cal = Calendar.getInstance();
		cal.setTime(person.getEntryDate());
		cal.set(Calendar.DAY_OF_MONTH, 1);

		WorktimeMonth worktimeMonth = null;
		while(cal.getTime().before(new Date())){
			worktimeMonth = service.createWorktimeMonth(persistenceService,person,cal.getTime());
			service.calculateMonthForPerson(persistenceService,worktimeMonth);
			if(cal.get(Calendar.MONTH) == 11){
				worktimeYearUpdate(persistenceService, worktimeMonth.getWorktimeYear());
			}
			cal.add(Calendar.MONTH, 1);
		}
		worktimeYearUpdate(persistenceService, worktimeMonth.getWorktimeYear());
	}
	
	
	/**
	 * Beechnet den Abrechnungstand einer Person bis zu einem bestimmten
	 * Zeitpunkt
	 * @param person
	 * @param date
	 * @return
	 */
	public AccountingPerson computeOvertimeTillMonth(PersistenceService persistenceService,Person person, Date date){
		StringBuffer sb = new StringBuffer();
		WorktimeMonthService service = new WorktimeMonthService();
		WorktimeMonth worktimeMonth = service.createWorktimeMonth(persistenceService,person,date);
		AccountingPerson accountingPerson = new AccountingPerson();
		accountingPerson.setPersonalNumber(person.getPersonalNumber());
		Double daysSickness = 0.0;
		Double daysHoliday = 0.0;
		Integer overtime = 0;
		sb.append("person: " + person.getFullname() + "(" + person.getPersonalNumber() + ")" + "\n");
		sb.append("Date: " + date + " = " + new MonthString(date) + "\n");
		
		
		/* Aufsummierung der Jahre bis zum Datum*/
		Map<YearString, AccountingYear> accountingYears = new AccountingYearService(persistenceService).getAccountingYears(person);
		Integer startYear = persistenceService.getStartYear();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		while(startYear < cal.get(Calendar.YEAR)){
			YearString yearString = new YearString("" +startYear);
			AccountingYear accountingYear = accountingYears.get(yearString);
			if(accountingYear != null){
				sb.append("--- Jahr: " + startYear + " = " + accountingYear.getOvertime() + "\n");
				daysSickness += accountingYear.getDaysSickness();
				daysHoliday -= accountingYear.getDaysHoliday() + accountingYear.getDaysGivenHoliday();
				overtime += accountingYear.getOvertime();				
			}
			startYear++;
		}
		sb.append("Gesamt nach Aufsummierung Vorjahre: " + overtime + " = " + (overtime/3600.0) + "\n\n");

		/* Aufsummierung der Monate des Abrechnungsjahres bis zum Datum*/
		Map<MonthString, AccountingMonth> accountingMonthsYear = new AccountingMonthService(persistenceService).getAccountingMonthsYear(person,new YearString(date));
		Integer month = 0;
		DecimalFormat monthFormat = new DecimalFormat("00");
		while (month <= cal.get(Calendar.MONTH)){
			MonthString monthString = new MonthString(cal.get(Calendar.YEAR) + "-" + monthFormat.format(month+1));
			AccountingMonth accountingMonth = accountingMonthsYear.get(monthString);
			sb.append("--- Monat: " + monthString + " = " + (accountingMonth != null?accountingMonth.getOvertime():null) + " Month: " + monthFormat.format(month+1) + "\n");
			if(accountingMonth != null){
				daysSickness += accountingMonth.getDaysSickness();
				daysHoliday -= accountingMonth.getDaysHoliday();
				overtime += accountingMonth.getOvertime() - accountingMonth.getInclusiveTime();								
			}
			month++;
		}
		sb.append("Gesamt nach Aufsummierung Monate: " + overtime + " = " + (overtime/3600.0) + "\n\n");

		/* Startwerte der Person ber�cksichigen*/
		if(person.getStartHoliday() != null){
			daysHoliday += person.getStartHoliday();			
			accountingPerson.setStartHoliday(daysHoliday);
		}
		if(person.getStartOvertime() != null){
			Double startOvertime = person.getStartOvertime()*3600;
			overtime +=startOvertime.intValue();
			sb.append("--- Start: " + startOvertime  + "\n");
			accountingPerson.setStartOvertime(startOvertime);
		}

		sb.append("Gesamt nach Aufsummierung Start: " + overtime + " = " + (overtime/3600.0) + "\n\n");
		
		accountingPerson.setDaysHoliday(daysHoliday);
		accountingPerson.setDaysSickness(daysSickness);
		accountingPerson.setOvertime(overtime);
		persistenceService.getLogger().debug(sb.toString(),this.getClass());
		return accountingPerson;
	}

	
	/**
	 * Berechnet einen Monat f�r eine Person und aktualisiert die
	 * Jahresabrechnung
	 * 
	 * @param persistenceService
	 * @param worktimeMonth
	 */
	
	public void worktimeMonthUpdate(PersistenceService persistenceService,Person person, Date date){
		WorktimeMonthService service = new WorktimeMonthService();
		WorktimeMonth worktimeMonth = service.createWorktimeMonth(persistenceService,person,date);
		worktimeMonthUpdate(persistenceService,worktimeMonth);
	}
	
	/**
	 * Berechnet einen Monat f�r eine Person und aktualisiert die
	 * Jahresabrechnung
	 * 
	 * @param persistenceService
	 * @param worktimeMonth
	 */
	public void worktimeMonthUpdate(PersistenceService persistenceService,WorktimeMonth worktimeMonth){
		WorktimeMonthService service = new WorktimeMonthService();
		service.calculateMonthForPerson(persistenceService,worktimeMonth);
		worktimeYearUpdate(persistenceService, worktimeMonth.getWorktimeYear());

	}
	
	/**
	 * Berechnet die Jahresabrechnung einer Person und aktualisiert
	 * die Gesamtabrechnung der Person
	 * @param persistenceService
	 * @param worktimeYear
	 */
	public void worktimeYearUpdate(PersistenceService persistenceService,WorktimeYear worktimeYear){
		AccountingYearService accountingYearService = new AccountingYearService(persistenceService);
		worktimeYear = new WorktimeYear(persistenceService, worktimeYear, worktimeYear.getWorktimePerson());
		AccountingYear accountingYear = worktimeYear.getWorktimePerson().getAccountingYears().get(worktimeYear.getYear());

		Double daysSickness = 0.0;
		Double daysPublicHoliday = 0.0;
		Double daysHoliday = 0.0;
		Integer overtime = 0;
		
		if(accountingYear == null){
			accountingYear = new AccountingYear();
			accountingYear.setYear(worktimeYear.getYear());
			accountingYear.setPersonalNumber(worktimeYear.getWorktimePerson().getPerson().getPersonalNumber());;
		}
		
		for(AccountingMonth accountingMonth:worktimeYear.getAccountingMonths().values()){
			daysSickness += accountingMonth.getDaysSickness();
			daysPublicHoliday += accountingMonth.getDaysPublicHoliday();
			daysHoliday += accountingMonth.getDaysHoliday();
			overtime += (accountingMonth.getOvertime() - accountingMonth.getInclusiveTime());
		}
		
		//Integer inclusiveOvertime = worktimeYear.getWorktimePerson().getPerson().get
		
		accountingYear.setDaysSickness(daysSickness);
		accountingYear.setDaysHoliday(daysHoliday);
		accountingYear.setDaysPublicHoliday(daysPublicHoliday);
		accountingYear.setOvertime(overtime);
		
		accountingYearService.saveAccountingYear(accountingYear);
		worktimeYear.getWorktimePerson().getAccountingYears().put(accountingYear.getYear(), accountingYear);
		worktimePersonUpdate(worktimeYear.getWorktimePerson());
	}
	
	
	/**
	 * Berechnet die Gesamtabrechnung einer Person
	 * @param worktimePerson
	 */
	public void worktimePersonUpdate(WorktimePerson worktimePerson){
		AccountingPersonService accountingPersonService = new AccountingPersonService(worktimePerson.getPersistenceService());
		AccountingPerson accountingPerson = worktimePerson.getAccountingPerson();
		if(accountingPerson == null){
			accountingPerson = new AccountingPerson();
			
		}
		accountingPerson.setPersonalNumber(worktimePerson.getPerson().getPersonalNumber());
		Double daysSickness = 0.0;
		Double daysHoliday = 0.0;
		Integer overtime = 0;

		if(worktimePerson.getPerson().getStartHoliday() != null){
			daysHoliday += worktimePerson.getPerson().getStartHoliday();			
			accountingPerson.setStartHoliday(daysHoliday);
		}
		if(worktimePerson.getPerson().getStartOvertime() != null){
			Double startOvertime = worktimePerson.getPerson().getStartOvertime()*3600;
			overtime +=startOvertime.intValue();
			accountingPerson.setStartOvertime(startOvertime);
		}
		
		Map<YearString, AccountingYear> accountingYears = worktimePerson.getAccountingYears();
		for (AccountingYear accountingYear : accountingYears.values()) {
			daysSickness += accountingYear.getDaysSickness();
			daysHoliday -= accountingYear.getDaysHoliday();
			overtime += accountingYear.getOvertime();
		}

		accountingPerson.setDaysHoliday(daysHoliday);
		accountingPerson.setDaysSickness(daysSickness);
		accountingPerson.setOvertime(overtime);
		
		
		accountingPersonService.saveAccountingPerson(accountingPerson);
	}
}
