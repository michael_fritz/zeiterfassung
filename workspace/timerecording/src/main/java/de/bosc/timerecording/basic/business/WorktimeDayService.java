package de.bosc.timerecording.basic.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.bosc.timerecording.basic.business.model.WorktimeDay;
import de.bosc.timerecording.basic.business.model.WorktimeGlobal;
import de.bosc.timerecording.basic.business.model.WorktimeMonth;
import de.bosc.timerecording.basic.business.model.WorktimePerson;
import de.bosc.timerecording.basic.business.model.WorktimeYear;
import de.bosc.timerecording.basic.enumeration.LogLevel;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.DayTimeentry;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.PublicHoliday;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.model.TimeentryKind;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.TimeentryKindService;
import de.bosc.timerecording.basic.service.TimeentryService;
import de.bosc.timerecording.basic.service.WorktimeSettingService;
import de.bosc.timerecording.basic.tool.Util;

/**
 * @author MiFr
 *
 */
public class WorktimeDayService {

	private WorktimeMonth worktimeMonth;
	private PersistenceService persistenceService;
	private LogLevel logLevel = LogLevel.INFO;

	public WorktimeDayService(PersistenceService persistenceService,WorktimeMonth worktimeMonth){
		this.worktimeMonth = worktimeMonth;
		this.persistenceService = persistenceService;
		this.logLevel = (persistenceService.getLogger()).getLogLevelForClass(this.getClass());

	}
	
	public WorktimeDayService(PersistenceService persistenceService,Person person,MonthString month) throws Exception {
		this.persistenceService = persistenceService;
		if(person == null){
			throw new Exception("Person is null");
		}
		if(month == null){
			throw new Exception("Month is null");
		}
		WorktimeGlobal worktimeGlobal = new WorktimeGlobal(persistenceService);
		WorktimePerson worktimePerson = new WorktimePerson(persistenceService, person);
		WorktimeYear worktimeYear = new WorktimeYear(persistenceService,worktimeGlobal, worktimePerson ,new YearString(month.toString().substring(0, 7)));;
		this.worktimeMonth = new WorktimeMonth(persistenceService,worktimeYear, month);
		this.logLevel = (persistenceService.getLogger()).getLogLevelForClass(this.getClass());
	}
	
	
	public WorktimeDay getDayWorktime(PersistenceService persistenceService,Date date) throws PersistenceException{
		this.persistenceService = persistenceService;
		WorktimeSetting worktimeSetting = new WorktimeSettingService(persistenceService).getWorktimeSettingForDate(getWorktimeSettings(), date);
		if(worktimeSetting == null){
			worktimeSetting = new WorktimeSettingService(persistenceService).getWorktimeSettingForDate(worktimeMonth.getPerson().getLocationDocId(), date);
		}
		return getDayWorktime(persistenceService,date,worktimeSetting);
	}
	
	public WorktimeDay getDayWorktime(PersistenceService persistenceService,Date date, WorktimeSetting worktimeSetting) throws PersistenceException{
		this.persistenceService = persistenceService;
		StringBuffer sb = new StringBuffer("\n\nTagesabrechnung f�r " + getPerson().getFullname() + " ("  + getPerson().getPersonalNumber() + ") on " + TimeentryService.dateFormat.format(date) + "\n\n");
		DateString dateString = new DateString(date);
		WorktimeDay worktimeDay = new WorktimeDay(sb);
		worktimeDay.setDate(dateString);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		if(worktimeSetting == null){
			throw new PersistenceException("Keine Arbeitseinstung f�r " + getPerson().getFullname() + " ("   + getPerson().getPersonalNumber() + ") am " + TimeentryService.dateFormat.format(date));
		}

		Integer soll = worktimeSetting.getDailyWorktimes().get(cal.get(Calendar.DAY_OF_WEEK));
		List<Timeentry> timeentries = getTimeentries(persistenceService,dateString);
		sb.append("Anzahle Eintr�ge				: " + timeentries.size() + "\n");
		timeentries = getCleanedTimeEntries(timeentries, TimeentryService.dateFormat.format(date));
		sb.append("Anzahle Eintr�ge	nach clean	: " + timeentries.size()+ "\n");
		worktimeDay.setTimeentries(timeentries);
		worktimeDay.setWorktimeSetting(worktimeSetting);
		worktimeDay.setDayTimeFactor(worktimeSetting.getDailyPercents().get(cal.get(Calendar.DAY_OF_WEEK)));
		worktimeDay.setPublicHoliday(getPublicHoliday(date));
		worktimeDay.setSoll(soll);

		sb.append("Wochentag					: " + Util.getWeekdayTextLong(cal.get(Calendar.DAY_OF_WEEK)) + "\n");
		sb.append("Soll aus Arbeitseinstellungen		: " + Util.getHours(soll) + "\n");
		sb.append("Zuschlag aus Arbeitseinstellungen	: " + Util.getPercent(worktimeDay.getDayTimeFactor()) + "\n");
		sb.append("Pause 1 aus  Arbeitseinstellungen	: nach " + Util.getHours(worktimeSetting.getPausetimes()[0].getAfter()) + " " + Util.getMinutes(worktimeSetting.getPausetimes()[0].getPause()) +  " min\n");
		sb.append("Pause 2 aus  Arbeitseinstellungen	: nach " + Util.getHours(worktimeSetting.getPausetimes()[1].getAfter()) + " " + Util.getMinutes(worktimeSetting.getPausetimes()[1].getPause()) +  " min\n");
		sb.append("Pause 3 aus  Arbeitseinstellungen	: nach " + Util.getHours(worktimeSetting.getPausetimes()[2].getAfter()) + " " + Util.getMinutes(worktimeSetting.getPausetimes()[2].getPause()) +  " min\n");
		sb.append("Generelle Pausenabzug			: " + Util.getMinutes(worktimeDay.getPausetimeGeneral()) + "\n");
		
		
		// Erzeuge Grundliste
		createDayTimeentries(persistenceService,worktimeDay);
		// Berechne Krankheit
		setSicknessInformation(worktimeDay);
		// Berechne Urlaub
		setHolidayInformation(worktimeDay);
		// normale Arbeitszeit mit Pausen berechnen
		calculateNormalWorktime(worktimeDay);
		
		// setzten derr Sollzeit bei Feiertag
		if(worktimeDay.isPublicHoliday()){
			worktimeDay.setSoll(worktimeDay.getPublicHoliday().isHalfDay()?soll/2:0);
		}

		//�berpr�fung ob eine complementary Zeiteintrag vorhanden
		//Wenn ja und Ist < Soll dann Ist auf Soll setzen
		if(worktimeDay.getComplementary()>0){
			Integer istWorktime = worktimeDay.getIstWorktime();
			if(worktimeDay.isPublicHoliday()){
				if(worktimeDay.getPublicHoliday().isHalfDay() && worktimeDay.getComplementary() == 0.5){
					worktimeDay.setComplementary(1.0);
				}
			}
			if(istWorktime < worktimeDay.getSoll()*worktimeDay.getComplementary()){
				Integer complementaryTime = (int) (worktimeDay.getSoll()*worktimeDay.getComplementary() - istWorktime);
				worktimeDay.setComplementaryTime(complementaryTime);
				worktimeDay.setIst(worktimeDay.getIst() + complementaryTime);				
				sb.append("\nComplementary Zeiteintrag vorhanden und Ist der Arbeitszeit < Soll mit Factor " + worktimeDay.getComplementary() + " \n");	
			}
		}
		
		//Zuschl�ge ermitteln und addieren
		if(worktimeDay.isPublicHoliday() && worktimeDay.getPublicHoliday().getTimeBonus() > worktimeDay.getDayTimeFactor()){
			sb.append("FEIERTAG Zuschlag ist h�her: " + worktimeDay.getPublicHoliday().getTimeBonus() + " %\n");
			sb.append("---------------------------------------------------------\n");
			worktimeDay.setDayTimeFactor(worktimeDay.getPublicHoliday().getTimeBonus());
		}
		Integer zuschlag =(int) (worktimeDay.getIstWorktime()*( worktimeDay.getDayTimeFactor()/100));
		worktimeDay.setAdditionDay(zuschlag);
		sb.append("TAGES Zuschlag: " + worktimeDay.getDayTimeFactor() + "% = " + TimeentryService.parseTimeToString( zuschlag) +"\n");
		worktimeDay.setIst(worktimeDay.getIst() + zuschlag);
		
		//Zuschl�ge f�r Arbeitszeitarten
		Integer zuschlagKinds = 0;
		for (DayTimeentry dayTimeentry : worktimeDay.getDayTimeentriesAll()) {
			if(!dayTimeentry.isComplementary()){
				TimeentryKind timeentryKind = worktimeDay.getWorktimeSetting().getTimeentryKinds().get(dayTimeentry.getTimeentryKind().getPrimaryKey());
				if(timeentryKind != null){
					sb.append("---------------------------------------------------------\n");
					Double timeentryKindFactor = timeentryKind.getValue();
					if(timeentryKindFactor != null){
						String percent = timeentryKind.getPercent();
						Integer additionKind = (int) ((percent.equals("1")?dayTimeentry.getDurationDay()*(timeentryKindFactor/100):(timeentryKindFactor*3600)));
						dayTimeentry.setAdditionKind(additionKind);
						sb.append(timeentryKind.getName() + " Zuschlag " + (percent.equals("1")?" auf " + TimeentryService.parseTimeToString(dayTimeentry.getDurationDay()) :"")  + ": " +   timeentryKindFactor + (percent.equals("1")?" % = ":" Std = ") + TimeentryService.parseTimeToString(additionKind)  + "\n");
						zuschlagKinds+=additionKind;
					}
				}
			}
		}
		worktimeDay.setAdditionKinds(zuschlagKinds);
		worktimeDay.setIst(worktimeDay.getIst() + zuschlagKinds);
		
		// Ist-Zeiten der Zusatzzeiten addieren
		Integer ist = 0;
		for (DayTimeentry dayTimeentry : worktimeDay.getDayTimeentriesAll()) {
			if(!dayTimeentry.isComplementary() && !dayTimeentry.isCalculateBreaktime() && !dayTimeentry.isHoliday()){
				ist+=dayTimeentry.getDurationDay();
			}
		}
		ist+=worktimeDay.getIstWorktime();
		Double holidayIstHours = worktimeDay.getHoliday() * worktimeDay.getSoll();
		ist+=holidayIstHours.intValue();
		worktimeDay.setIst(worktimeDay.getIst() + ist);
		
		sb.append("=========================================================\n");
		sb.append("SOLL	: " + TimeentryService.parseTimeToString(worktimeDay.getSoll()) + "\n");
		sb.append("IST	: " + TimeentryService.parseTimeToString(worktimeDay.getIst()) + "\n");
		sb.append("PAUSE	: " + TimeentryService.parseTimeToString(worktimeDay.getBreakTime()) + "\n");
		sb.append("ZUSCHLAG TAG: " + worktimeDay.getDayTimeFactor() + " = " + TimeentryService.parseTimeToString(worktimeDay.getAdditionDay()) + "\n");
		sb.append("ZUSCHLAG ART: " + TimeentryService.parseTimeToString(worktimeDay.getAdditionKinds()) + "\n");
		sb.append("FEIERTAG	: " + (worktimeDay.isPublicHoliday()?(worktimeDay.getPublicHoliday().isHalfDay()?0.5:1.0):0.0) + "\n");
		sb.append("URLAUB	: " + worktimeDay.getHoliday() + "\n");
		sb.append("URLAUB(h): " + holidayIstHours + "\n");
		sb.append("KRANK	: " + worktimeDay.getSickness() + "\n");
		sb.append("=========================================================\n");
		sb.append("�BERSTUNDEN: " + TimeentryService.parseTimeToString(worktimeDay.getIst() - worktimeDay.getSoll()) + "\n");

		persistenceService.getLogger().debug(sb.toString(),this.getClass());
		return worktimeDay;
	}
	
	
	private void calculateNormalWorktime(WorktimeDay worktimeDay){
		StringBuffer sb = worktimeDay.getSb();
		List<DayTimeentry> dayTimeentriesForCalculateBreaktime = worktimeDay.getDayTimeentriesForCalculateBreaktime();
		
		if(dayTimeentriesForCalculateBreaktime.size() > 0){
			Collections.sort(dayTimeentriesForCalculateBreaktime);
			
			DayTimeentry dayTimeentry = dayTimeentriesForCalculateBreaktime.get(0);
			

			
			int totalTimesForBreak = dayTimeentry.getDurationDay();
			int breaktimesBetween = 0;
			sb.append("---------- Pasuenzeiten zwischen Arbeitszeiten ----------\n");
			for(int i = 1;i<dayTimeentriesForCalculateBreaktime.size();i++){
				DayTimeentry entry = dayTimeentriesForCalculateBreaktime.get(i);
				totalTimesForBreak+=entry.getDurationDay();
				int breaktime = entry.getStartTime() - dayTimeentry.getEndTime()  ;
				breaktimesBetween += breaktime;
				sb.append("- Pausenzeit zwischen:"  + TimeentryService.parseTimeToString( dayTimeentry.getEndTime()) + " und " + TimeentryService.parseTimeToString( entry.getStartTime() ) + " = " + TimeentryService.parseTimeToString(breaktime ) +"\n");
				dayTimeentry = entry;
			}
			sb.append("Pausenzeit gesamt:"  + TimeentryService.parseTimeToString( breaktimesBetween) +"\n");
			
			Integer breaktimeDay = getBreakTime(worktimeDay.getWorktimeSetting(),totalTimesForBreak);
			sb.append("---------------------------------------------------------\n");
			sb.append("Gesamtarbeitszeit an diesm Tag			:"  + TimeentryService.parseTimeToString( totalTimesForBreak) +"\n");
			sb.append("Pasuenzeit abgezogen in den Zeiteintr�gen		:"  + TimeentryService.parseTimeToString( worktimeDay.getBreakTime()) +"\n");
			sb.append("Pausenzeit zwischen Zeiteintr�gen			:"  + TimeentryService.parseTimeToString( breaktimesBetween) +"\n");
			Integer breaktimeDayDiff = breaktimeDay - (worktimeDay.getBreakTime() + breaktimesBetween);
			sb.append("-> Pause die noch abgezogen werden muss		:"  + TimeentryService.parseTimeToString( breaktimeDayDiff>0?breaktimeDayDiff:0) +"\n");
			if(breaktimeDayDiff > 0){
				totalTimesForBreak -= breaktimeDayDiff;
			}

			// Genereller Pausenabzug
			dayTimeentry = dayTimeentriesForCalculateBreaktime.get(0);
			if(worktimeDay.getWorktimeSetting().getPausetimeGeneral() != null && worktimeDay.getWorktimeSetting().getPausetimeGeneral() != 0){
				if(dayTimeentriesForCalculateBreaktime.size() >1 || dayTimeentry.getDurationDay() > dayTimeentry.getDurationOtherDay()){
					if(totalTimesForBreak >=worktimeDay.getWorktimeSetting().getPausetimeGeneral()){
						totalTimesForBreak-=worktimeDay.getWorktimeSetting().getPausetimeGeneral();
						sb.append("---------------------------------------------------------\n");
						sb.append("Genereller Pasuenabzug		: " + Util.getMinutes(worktimeDay.getPausetimeGeneral()) + "\n");
						worktimeDay.setPausetimeGeneral(worktimeDay.getWorktimeSetting().getPausetimeGeneral());
					}
				}
			}
			
			sb.append("=========================================================\n");
			
			worktimeDay.setIstWorktime(totalTimesForBreak);
		}
	}
	
	
	/**
	 * @param worktimeDay
	 */
	private void setHolidayInformation(WorktimeDay worktimeDay){	
		for(DayTimeentry dayTimeentry : worktimeDay.getDayTimeentriesAll()) {
			if(dayTimeentry.isHoliday()){
				if(dayTimeentry.getTimeDuration().equals("1")){
					worktimeDay.setHoliday(1.0 - worktimeDay.getSickness());
				}else if(dayTimeentry.getTimeDuration().equals("2")){
					worktimeDay.setHoliday(0.5 - worktimeDay.getSickness());
				}
			}
		}
		
		if(worktimeDay.isPublicHoliday()){
			if(worktimeDay.getPublicHoliday().isHalfDay() && worktimeDay.getHoliday() == 1.0){
				worktimeDay.setHoliday(0.5);
			}else{
				worktimeDay.setHoliday(0.0);
			}
		}
		
		if(worktimeDay.getHoliday() < 0 || worktimeDay.getSoll() == 0){
			worktimeDay.setHoliday(0.0);
		}else if(worktimeDay.getHoliday() > 1){
			worktimeDay.setHoliday(1.0);
		}

	}
	
	
	/**
	 * @param worktimeDay
	 */
	private void setSicknessInformation(WorktimeDay worktimeDay){
		
		for (DayTimeentry dayTimeentry : worktimeDay.getDayTimeentriesAll()) {
			if(dayTimeentry.isSickness()){
				if(dayTimeentry.getTimeDuration().equals("1")){
					worktimeDay.setSickness(1.0);
				}else if(dayTimeentry.getTimeDuration().equals("2")){
					worktimeDay.setSickness(0.5);
				}else{
					worktimeDay.setSickness((double) (dayTimeentry.getDurationDay() / worktimeDay.getSoll()));
				}
			}
		}

	}

	
	private List<DayTimeentry> createDayTimeentries(PersistenceService persistenceService,WorktimeDay worktimeDay){
		List<DayTimeentry> dayTimeentries = new ArrayList<DayTimeentry>();
		List<DayTimeentry> dayTimeentriesForBreaktime = new ArrayList<DayTimeentry>();
		try{		
			DateString date = worktimeDay.getDate();
			Calendar cal = Calendar.getInstance();
			cal.setTime(worktimeDay.getDate().getJavaDate());
			cal.add(Calendar.DATE, 1);
			DateString nextDate = new DateString(cal.getTime());
			cal.add(Calendar.DATE, -2);
			DateString prevDate = new DateString(cal.getTime());
			
			for (Timeentry timeentry : worktimeDay.getTimeentries()) {
				DayTimeentry dayTimeentry = createDayTimeentry(timeentry, worktimeDay, date, prevDate, nextDate);
				dayTimeentries.add(dayTimeentry);
				if(dayTimeentry.isCalculateBreaktime()){
					dayTimeentriesForBreaktime.add(dayTimeentry);
				}
			}
			worktimeDay.setDayTimeentriesAll(dayTimeentries);
			worktimeDay.setDayTimeentriesForCalculateBreaktime(dayTimeentriesForBreaktime);
			
			// Eintrag im Log als Tabelle
			if(logLevel == LogLevel.DEBUG || logLevel == LogLevel.TRACE){
				getFormattedInfoEntry(worktimeDay);			
			}
			
		}catch(Exception e){
			persistenceService.getLogger().error("", e);
		}

		return dayTimeentries;
	}
	
	
	/**
	 * @param timeentry
	 * @param worktimeDay
	 * @param date
	 * @param prevDate
	 * @param nextDate
	 * @return
	 */
	private DayTimeentry createDayTimeentry(Timeentry timeentry, WorktimeDay worktimeDay,DateString date, DateString prevDate, DateString nextDate){
		DayTimeentry dayTimeentry = new DayTimeentry();
		Integer soll = worktimeDay.getSoll();
		WorktimeSetting worktimeSetting = worktimeDay.getWorktimeSetting();
		StringBuffer sb = new StringBuffer("------------------------------------------------------\n");
		dayTimeentry.setTimeentryKind(getTimeentryKind(timeentry.getTimeentryKindId()));
		dayTimeentry.setTimeDuration(timeentry.getTimeDuration());
		dayTimeentry.setStartTime(timeentry.getStartTime() != null?timeentry.getStartTime():0);
		dayTimeentry.setEndTime(timeentry.getEndTime() != null?timeentry.getEndTime():0);
		
		/* 
		 * TimeDuration
		 * --- ganzer Tag	|1
		 * --- halber Tag	|2
		 * --- Variabel		|3
		 */
		
		if(timeentry.getTimeDuration().equals("1")){
			dayTimeentry.setDurationDay(soll);	
			dayTimeentry.setDurationShow(dayTimeentry.getDurationDay());
			dayTimeentry.setDurationTotal(dayTimeentry.getDurationDay());
			if(dayTimeentry.isComplementary()){
				worktimeDay.setComplementary(1.0);
			}
		}else if(timeentry.getTimeDuration().equals("2")){
			dayTimeentry.setDurationDay(soll/2);	
			dayTimeentry.setDurationShow(dayTimeentry.getDurationDay());
			dayTimeentry.setDurationTotal(dayTimeentry.getDurationDay());
			if(dayTimeentry.isComplementary()){
				worktimeDay.setComplementary(0.5);
			}
		}else if(timeentry.getTimeDuration().equals("3")){

			if(timeentry.getStartDate().equals(date) && timeentry.getEndDate().equals(date) ){
				
				// Fall: Start und Ende an diesem Tag
				sb.append(" --- Fall: Start und Ende an diesem Tag\n");
				dayTimeentry.setDurationDay(timeentry.getEndTime() - timeentry.getStartTime());	
				dayTimeentry.setDurationTotal(dayTimeentry.getDurationDay());
			}else if(timeentry.getStartDate().equals(date) && timeentry.getEndDate().equals(nextDate)){
				
				// Fall: Start am gleichen Tag und Ende am n�chsten Tag
				sb.append(" --- Fall: Start am gleichen Tag und Ende am n�chsten Tag\n");
				dayTimeentry.setDurationOtherDay(timeentry.getEndTime());
				dayTimeentry.setEndTime(TimeentryService.MAX_ENDTIME);
				dayTimeentry.setDurationDay(dayTimeentry.getEndTime() - dayTimeentry.getStartTime());
				dayTimeentry.setDurationTotal(dayTimeentry.getDurationDay() + dayTimeentry.getDurationOtherDay());
				sb.append(" ------ Dauer Tag     : " + dayTimeentry.getDurationDay()  + " = " + TimeentryService.parseTimeToString(dayTimeentry.getDurationDay() ) + "\n");
				sb.append(" ------ Dauer Folgetag: " + dayTimeentry.getDurationOtherDay()  + " = " + TimeentryService.parseTimeToString(dayTimeentry.getDurationOtherDay() ) + "\n");
			}else if(timeentry.getStartDate().equals(prevDate) && timeentry.getEndDate().equals(date)){
				
				// Fall: Start am Vortag und Ende am n�chsten Tag
				sb.append(" --- Fall: Start am Vortag und Ende am n�chsten Tag\n");
				dayTimeentry.setDurationDay(dayTimeentry.getEndTime() );
				dayTimeentry.setDurationOtherDay(TimeentryService.MAX_ENDTIME- dayTimeentry.getStartTime());				
				dayTimeentry.setStartTime(0);

				dayTimeentry.setDurationTotal(dayTimeentry.getDurationDay() + dayTimeentry.getDurationOtherDay());
				sb.append(" ------ Dauer Vortag: " + dayTimeentry.getDurationOtherDay() + " = " + TimeentryService.parseTimeToString(dayTimeentry.getDurationOtherDay() ) + "\n");
				sb.append(" ------ Dauer Tag: " + dayTimeentry.getDurationDay()  + " = " + TimeentryService.parseTimeToString(dayTimeentry.getDurationDay() ) + "\n");
			}
			dayTimeentry.setDurationShow(dayTimeentry.getDurationDay());
			dayTimeentry.setBreaktime(getBreakTime(worktimeSetting,dayTimeentry.getDurationTotal()));
			if(dayTimeentry.getTimeentryKind().getTimeCalculation().equals("1")){
				dayTimeentry.setCalculateBreaktime(true);
				if(dayTimeentry.getDurationDay() > dayTimeentry.getDurationOtherDay()){
					dayTimeentry.setDurationDay(dayTimeentry.getDurationDay() - dayTimeentry.getBreaktime());
					worktimeDay.setBreakTime(worktimeDay.getBreakTime() + dayTimeentry.getBreaktime());
				}
				
			}
		}
		if(logLevel == LogLevel.TRACE){
			worktimeDay.getSb().append(sb);
		}
		return dayTimeentry;
	}
	
	
	/**
	 * Ermittelt die Pausenzeit f�r eine Dauer auf Basis der Arbeitszeiteinstellungen
	 * @param duration
	 * @return
	 */
	private int getBreakTime(WorktimeSetting worktimeSetting,int duration){
		int breaktime = 0;
		for(int i = 0;i<3;i++){
			Integer after = worktimeSetting.getPausetimes()[i].getAfter();
			Integer pause = worktimeSetting.getPausetimes()[i].getPause();
			if(after != null && pause != null){
				if(duration >= after && breaktime < pause){
					breaktime = pause;
				}				
			}
		}
		return breaktime;
	}
	
	/**
	 * Konsolidiert die Zeiteintr�ge durch Pr�fung auf �berlappungen
	 * 
	 * @param timeentries
	 * @return
	 */
	private List<Timeentry> getCleanedTimeEntries(List<Timeentry> timeentries, String date){
		/* 
		 * TimeCalculation
		 * --- normale Arbeit	|1
		 * --- Zusatzzeit		|2
		 * --- Auff�llzeit		|3
		 */
		Collections.sort(timeentries);
		List<Timeentry> newTimeentries = new ArrayList<Timeentry>();
		try {
			/* in der ersten Schleife wird ermittelt on ein EIntrag mit
			 * Tages�bergang vorhanden ist -> wenn ja als ersten Eintrag hinzuf�gen
			 * weil sonst m�sste noch mit dem Vor- bzw Folgetag gecheckt werden 
			 */
			for (Timeentry timeentry : timeentries) {
				TimeentryKind timeentryKind = getTimeentryKind(timeentry.getTimeentryKindId());
				 if(timeentryKind.getTimeCalculation().equals("1") && timeentry.getTimeDuration().equals("3")){
					 if(!timeentry.getStartDate().equals(timeentry.getEndDate())){
						 if(newTimeentries.size() == 0){
							 newTimeentries.add(timeentry);
						 }else{
							 boolean addTimeentry = true;
							 for(Timeentry t1:newTimeentries){
								 if(t1.getStartDate().equals(timeentry.getStartDate())){
									 addTimeentry = false;
								 }
							 }
							 if(addTimeentry){
								 newTimeentries.add(timeentry);
							 }
						 }
					 }
				 }
			}
			
			for (Timeentry timeentry : timeentries) {
				TimeentryKind timeentryKind = getTimeentryKind(timeentry.getTimeentryKindId());

				//Fall: erster Eintrag -> hinzuf�gen
				if(timeentry.getEndTime() == null || timeentry.getEndTime() == -1){

				}else if(newTimeentries.size() == 0){
					newTimeentries.add(timeentry);
				}else if(!timeentryKind.getTimeCalculation().equals("1")){
					newTimeentries.add(timeentry);
				}else{
					//ist die Art der Dauer ganzer Tag oder halber Tag
					if(timeentry.getTimeDuration().equals("1") || timeentry.getTimeDuration().equals("2")){
						newTimeentries.add(timeentry);
					}else{						
						//mit bereits hinzugef�gten vergleichen
						if(!newTimeentries.contains(timeentry)){
							if(checkTimeEntries(timeentry, newTimeentries, date)){
								newTimeentries.add(timeentry);
								if(timeentry.isCollision()){
									timeentry.setCollision(false);
									this.persistenceService.saveTimeentry(timeentry);
								}
							}else if(!timeentry.isCollision()){
								timeentry.setCollision(true);
								this.persistenceService.saveTimeentry(timeentry);
							}
						}
					}
				}	
			}
		} catch (Exception e) {
			this.persistenceService.getLogger().error("", e);
		}
		Collections.sort(newTimeentries);
		return newTimeentries;
	}
	
	/**
	 * Pr�ft zwei Zeiteintr�ge auf �berschneidungen
	 * 
	 * @return true = �berschneidung; false = keine �berschneidung
	 */
	private boolean checkTimeEntries(Timeentry timeentry,List<Timeentry> newTimeentries,String date ){

		boolean result = true;
		for(Timeentry timeentrytocompare:newTimeentries){
			TimeentryKind timeentryKind = getTimeentryKind(timeentrytocompare.getTimeentryKindId());
			 if(timeentryKind.getTimeCalculation().equals("1") && timeentrytocompare.getTimeDuration().equals("3")){
				//TODO �berschneidungen muss noch Algorithmus gefudnen werden
				int startTime = timeentry.getStartDate().equals(date) || !timeentry.getEndDate().equals(date)? timeentry.getStartTime():0;
				int startTimeCompare =  timeentrytocompare.getStartDate().equals(date)? timeentrytocompare.getStartTime():0;
				int endTime = timeentry.getEndDate().equals(date)? timeentry.getEndTime():TimeentryService.MAX_ENDTIME;
				int endTimeCompare = timeentrytocompare.getEndDate().equals(date)? timeentrytocompare.getEndTime():TimeentryService.MAX_ENDTIME;
				if(startTimeCompare > startTime && startTimeCompare < endTime){
					/*Zeit liegt zwischen der zu vergleichenden Zeitraum*/
					return false;
				}else if(endTimeCompare > startTime && endTimeCompare < endTime){
					/*Zeit liegt zwischen der zu vergleichenden Zeitraum*/
					return false;
				}else if(startTimeCompare <= startTime && endTimeCompare >= endTime){
					/*Zeit umfasst der zu vergleichenden Zeitraum*/
					return false;
				}else if(startTimeCompare >= startTime && endTimeCompare <= endTime){
					/*Zeit liegt inerhalb der zu vergleichenden Zeitraum*/
					return false;
				}
			}
		}

		return result;
	}
	
	/**
	 * Gibt eine Liste der Zeiteintr�ge zur Berechnung der Pasuenzeit zur�ck
	 * @param dayTimeentriesAll
	 * @return
	 */
	private List<DayTimeentry> getListDayTimeentriesForCalculateBreaktime(List<DayTimeentry> dayTimeentriesAll){
		List<DayTimeentry> dayTimeentries = new ArrayList<DayTimeentry>();
		for (DayTimeentry dayTimeentry : dayTimeentriesAll) {
			if(dayTimeentry.isCalculateBreaktime()){
				dayTimeentries.add(dayTimeentry);
			}
		}
		return dayTimeentries;
	}
	
	
	public void getFormattedInfoEntry(WorktimeDay worktimeDay){
		StringBuffer sb = new StringBuffer();
		sb.append("\n====================================================================================================\n");
		try{
			sb.append(String.format("%-20s", "Zeiteintragsart"));
			sb.append(String.format("%-6s", "Typ"));
			sb.append(String.format("%-6s", "Mode"));
			sb.append(String.format("%-11s", "   Start"));
			sb.append(String.format("%-11s", "   Ende"));
			sb.append(String.format("%-13s", "   Dauer Tag"));
			sb.append(String.format("%-20s", "   Dauer and. Tag"));
			sb.append(String.format("%-13s", "    Gesamt"));
			sb.append("\n----------------------------------------------------------------------------------------------------\n");

			for(DayTimeentry dayTimeentry:worktimeDay.getDayTimeentriesAll()){
				sb.append(String.format("%-20s",dayTimeentry.getTimeentryKind().getName()));
				sb.append(String.format("%-6s", dayTimeentry.getTimeentryKind().getTimeCalculation() + "  "));
				sb.append(String.format("%-6s", dayTimeentry.getTimeDuration() + "  "));
				sb.append(String.format("%11s", TimeentryService.parseTimeToString(dayTimeentry.getStartTime()) + "  "));
				sb.append(String.format("%11s", TimeentryService.parseTimeToString(dayTimeentry.getEndTime())+ "  "));
				sb.append(String.format("%13s", TimeentryService.parseTimeToString(dayTimeentry.getDurationDay())));
				sb.append(String.format("%18s", TimeentryService.parseTimeToString(dayTimeentry.getDurationOtherDay()) + "   "));
				sb.append(String.format("%13s", TimeentryService.parseTimeToString(dayTimeentry.getDurationTotal())));
				sb.append("\n");
			}
			sb.append("----------------------------------------------------------------------------------------------------\n\n");
			worktimeDay.getSb().append(sb);
		} catch(Exception e ){
			
		}

	}
	
	
	/**
	 * Gibt die zugeh�rige Zeiteintragsart zur�ck 
	 * 
	 * @param key
	 * @return
	 */
	private TimeentryKind getTimeentryKind(String key){
		return TimeentryKindService.getTimeentryKind(worktimeMonth.getTimeentryKinds(),key);
	}

	public List<Timeentry> getTimeentries(PersistenceService persistenceServerice,DateString date) {
		List<Timeentry> timeentries = new ArrayList<Timeentry>();
		return worktimeMonth.getTimeentriesMonth(persistenceServerice).containsKey(date)?worktimeMonth.getTimeentriesMonth(persistenceServerice).get(date):timeentries;
	}
	public PublicHoliday getPublicHoliday(Date date) {
		return worktimeMonth.getWorktimeYear().getMapHolidays().get(new DateString(date));
	}
	public MonthString getMonth() {
		return worktimeMonth.getMonth();
	}
	public Person getPerson() {
		return worktimeMonth.getPerson();
	}
	public Map<Date, WorktimeSetting> getWorktimeSettings() {
		return worktimeMonth.getWorktimeSettings();
	}

}
