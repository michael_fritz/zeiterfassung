package de.bosc.timerecording.basic.business;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import de.bosc.timerecording.basic.business.model.WorktimeDay;
import de.bosc.timerecording.basic.business.model.WorktimeGlobal;
import de.bosc.timerecording.basic.business.model.WorktimeMonth;
import de.bosc.timerecording.basic.business.model.WorktimePerson;
import de.bosc.timerecording.basic.business.model.WorktimeYear;
import de.bosc.timerecording.basic.enumeration.LogLevel;
import de.bosc.timerecording.basic.model.AccountingMonth;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.AccountingMonthService;
import de.bosc.timerecording.basic.service.TimeentryService;
import de.bosc.timerecording.basic.service.WorktimeSettingService;
import de.bosc.timerecording.basic.tool.Util;

public class WorktimeMonthService {

	private LogLevel logLevel = LogLevel.INFO;
	
	public WorktimeMonth createWorktimeMonth(PersistenceService persistenceServer,WorktimeGlobal worktimeGlobal, WorktimePerson worktimePerson, WorktimeYear worktimeYear, MonthString month) throws ParseException{
		return new WorktimeMonth(persistenceServer,worktimeYear, month);
	}

	
	/**
	 * @param persistenceService
	 * @param person
	 * @param date
	 * @return
	 */
	public WorktimeMonth createWorktimeMonth(PersistenceService persistenceService, Person person, Date date){
		WorktimeGlobal worktimeGlobal = new WorktimeGlobal(persistenceService);
		WorktimePerson worktimePerson = new WorktimePerson(persistenceService, person);
		WorktimeYear worktimeYear = new WorktimeYear(persistenceService,worktimeGlobal, worktimePerson, new YearString(date));
				
		WorktimeMonth worktimeMonth = new WorktimeMonth(persistenceService,worktimeYear, new MonthString(date));
		
		return worktimeMonth;
	}
	
	
	/**
	 * Berechnet f�r eine Person den Monat
	 * 
	 * @param persistenceService
	 * @param worktimeMonth
	 * @return
	 */
	public AccountingMonth calculateMonthForPerson(PersistenceService persistenceService,WorktimeMonth worktimeMonth){
		boolean update = false;
		logLevel = (persistenceService.getLogger()).getLogLevelForClass(this.getClass());
		WorktimeDayService worktimeDayService = new WorktimeDayService(persistenceService, worktimeMonth);
		StringBuffer sb = new StringBuffer();
		AccountingMonth accountingMonth = null;
		try {
			Date date = worktimeMonth.getMonth().getDate();
			WorktimeSetting worktimeSetting = new WorktimeSettingService(persistenceService).getWorktimeSettingForDate(worktimeMonth.getWorktimeSettings(), date);

			Calendar calStartOfMonth = Calendar.getInstance();
			calStartOfMonth.setTime(new Date());
			calStartOfMonth.set(Calendar.DATE, 1);

			/* 
			 * Wenn Eintrittsdatum nach dem zu berechnenden Monat ist dann
			 * nicht berechnen 
			 */
			if(!isAfterEntryDate(worktimeMonth.getPerson(),calStartOfMonth.getTime(),sb) || !isBeforeToday(date,sb) || !isBeforeDayOfSeparation(worktimeMonth.getPerson(), calStartOfMonth.getTime(), sb)){
				return accountingMonth;
			}
			
			accountingMonth = worktimeMonth.getAccountingMonth();
			if(accountingMonth == null){
				accountingMonth = new AccountingMonth(worktimeMonth.getPerson().getPersonalNumber(),worktimeMonth.getMonth());
			}
			
			Integer soll = 0;
			Integer ist = 0;
			Double sickness = 0.0;
			Double daysPublicHoliday = 0.0;
			Double daysHoliday = 0.0;
			
			Map<DateString,WorktimeDay> listWorktimeDay = new TreeMap<DateString, WorktimeDay>();
			
			
			Calendar calToday = Calendar.getInstance();
			calToday.setTime(new Date());

			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.DATE, 1);
			int month = cal.get(Calendar.MONTH);
			sb.append(getFormattedInfoDay(null));
			while(month == cal.get(Calendar.MONTH)){
				boolean beforeDateOfSeparation = isBeforeDayOfSeparation(worktimeMonth.getPerson(), cal.getTime(), sb);
				boolean afterEntryDate = isAfterEntryDate(worktimeMonth.getPerson(), cal.getTime(), sb);
				if(beforeDateOfSeparation && afterEntryDate && cal.getTime().before(calToday.getTime())){
					WorktimeDay worktimeDay = worktimeDayService.getDayWorktime(persistenceService,cal.getTime());
					listWorktimeDay.put(worktimeDay.getDate(), worktimeDay);
					sb.append(getFormattedInfoDay(worktimeDay));
					soll+=worktimeDay.getSoll();
					ist+=worktimeDay.getIst();
					sickness+=(worktimeDay.getSickness());
					daysPublicHoliday+=(worktimeDay.isPublicHoliday()?(worktimeDay.getPublicHoliday().isHalfDay()?0.5:1.0):0.0);
					daysHoliday+=(worktimeDay.getHoliday());					
				}
				cal.add(Calendar.DATE ,1);
			}
			accountingMonth.setSoll(soll);
			accountingMonth.setMonth(new MonthString(date));
			accountingMonth.setDaysHoliday(daysHoliday);
			accountingMonth.setDaysPublicHoliday(daysPublicHoliday);
			accountingMonth.setDaysSickness(sickness);
			accountingMonth.setIst(ist);			
			accountingMonth.setOvertime(ist - soll);
			accountingMonth.setInclusiveTime(0);
			
			/* Nur berechnen wenn der Monat NICHT in der Zukunft liegt*/
			if(worktimeSetting != null && worktimeSetting.getInclusiveTime() > 0 && worktimeMonth.getMonth().getDate().before(new MonthString(new Date()).getDate())){
				if(accountingMonth.getOvertime() > worktimeSetting.getInclusiveTime()){
					accountingMonth.setInclusiveTime(worktimeSetting.getInclusiveTime());
				}else if(accountingMonth.getOvertime() > 0){
					accountingMonth.setInclusiveTime(accountingMonth.getOvertime());
				}
			}
			accountingMonth.setPersonalNumber(worktimeMonth.getPerson().getPersonalNumber());
			sb.append("-----------------------------------------------------------------------------------------------------\n");
			sb.append(getFormattedInfoMonth(accountingMonth));
			sb.append("-----------------------------------------------------------------------------------------------------\n");
			MonthString todayMonth = new MonthString(new Date());
			sb.append("heutiger Monat: " + todayMonth.getDate() + " - " + todayMonth + "\n");
			sb.append("zu ber.  Monat: " + worktimeMonth.getMonth().getDate() + " - " + worktimeMonth.getMonth() + "\n");
			sb.append("zu berechnender Monat ist vor heutigem Monat: " + (worktimeMonth.getMonth().getDate().before(new MonthString(new Date()).getDate())));
			
			
			update = new AccountingMonthService(persistenceService).saveAccountingMonth(accountingMonth);
			worktimeMonth.getWorktimeYear().getAccountingMonths().put(accountingMonth.getMonth(),accountingMonth);
			persistenceService.getLogger().debug(sb.toString(), this.getClass());
			
		} catch (Exception e) {
			persistenceService.getLogger().error("", e);
		}
		return accountingMonth;
	}
	
	/**
	 * �berpr�ft ob sich der zu berechnende Monat nach dem Eintrittsdatum der Person liegt 
	 * @param entryDate
	 * @param date
	 * @return
	 */
	public boolean isAfterEntryDate(Person person, Date date, StringBuffer sb){
		boolean result = false;
		
		DateString dateEntry = new DateString(person.getEntryDate());
		DateString dateString = new DateString(date);
		if(dateEntry.equals(dateString) || date.after(person.getEntryDate())){
			result = true;
		}
		sb.append(" zu berechnender Tag " + new DateString(date) + " ist nach Eintrittsdatum:" + result + "\n");
		return result;
	}
	
	/**
	 * �berpr�ft ob sich der zu berechnende Monat var dem heutigen Tag liegt 
	 * @param entryDate
	 * @param date
	 * @return
	 */
	public boolean isBeforeToday(Date date,StringBuffer sb){
		boolean result = false;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DATE, 1);
		cal.add(Calendar.MONTH ,1);
		if(cal.getTime().after(date)){
			result = true;
		}
		sb.append(" zu berechnender Tag " + new DateString(date) + " vor Heute::" + result + "\n");
		return result;
	}
	
	/**
	 * �berpr�ft ob sich der zu berechnende Monat vor dem Austrittsdatum ist 
	 * @param entryDate
	 * @param date
	 * @return
	 */
	public boolean isBeforeDayOfSeparation(Person person, Date date,StringBuffer sb){
		boolean result = false;
		if (person.getDateOfSeparation() == null){
			result = true;
		}else {
			DateString dateSeparation = new DateString(person.getDateOfSeparation());
			DateString dateString = new DateString(date);
			if(dateSeparation.equals(dateString) || person.getDateOfSeparation().after(date)){
				result = true;				
			}
		}
		sb.append(" zu berechnender Tag " + new DateString(date) + " vor Austrittsdatum:" + result + "\n");
		return result;
	}
	
	/**
	 * @param worktimeDay
	 * @return
	 */
	public String getFormattedInfoDay(WorktimeDay worktimeDay){
		StringBuffer sb = new StringBuffer();

		try{
			if(logLevel == LogLevel.DEBUG || logLevel == LogLevel.TRACE){
				if(worktimeDay == null){
					sb.append(String.format("%-15s", "   Datum"));
					sb.append(String.format("%-13s", "      Soll"));
					sb.append(String.format("%-13s", "      Ist"));
					sb.append(String.format("%-13s", "      Pause"));
					sb.append(String.format("%-13s", "      �berstd"));
					sb.append(String.format("%-10s", "   Urlaub"));
					sb.append(String.format("%-10s", "   Krank"));
					sb.append(String.format("%-10s", " Feiertag"));
					
				}else{
					Calendar cal = Calendar.getInstance();
					cal.setTime(TimeentryService.parseTimeEntryDate(worktimeDay.getDate().toString(), "00:00:00"));
					sb.append(String.format("%-15s",Util.getWeekdayText(cal.get(Calendar.DAY_OF_WEEK)) + " " + worktimeDay.getDate()));
					sb.append(String.format("%13s", Util.getHours(worktimeDay.getSoll())));
					sb.append(String.format("%13s", Util.getHours(worktimeDay.getIst())));
					sb.append(String.format("%13s", Util.getMinutes(worktimeDay.getBreakTime())));
					Integer overtime = worktimeDay.getIst() - worktimeDay.getSoll();
					sb.append(String.format("%13s", Util.getHours(overtime)));
					sb.append(String.format("%-10s","   " + worktimeDay.getHoliday()));
					sb.append(String.format("%-10s",worktimeDay.getSickness()));
					sb.append(String.format("%-10s",worktimeDay.isPublicHoliday()?"     X":""));					
				}
			}
		} catch(Exception e ){
			
		}

		return sb.toString()+"\n";
	}
	
	public String getFormattedInfoMonth(AccountingMonth accountingMonth){
		StringBuffer sb = new StringBuffer();
		if(logLevel == LogLevel.DEBUG  || logLevel == LogLevel.TRACE){
			sb.append(String.format("%15s",accountingMonth.getMonth()));
			sb.append(String.format("%13s", Util.getHours(accountingMonth.getSoll())));
			sb.append(String.format("%13s", Util.getHours(accountingMonth.getIst())));
			sb.append(String.format("%13s", ""));
			sb.append(String.format("%13s", Util.getHours(accountingMonth.getOvertime())));
			sb.append(String.format("%-10s","     " + accountingMonth.getDaysHoliday()));
			sb.append(String.format("%-10s","     " + accountingMonth.getDaysSickness()));
			sb.append(String.format("%-10s","     " + accountingMonth.getDaysPublicHoliday()));
		}
		sb.append("\n\n---inclusive �berstubnden:" + accountingMonth.getInclusiveTime() + "\n");

		return sb.toString();
	}
}
