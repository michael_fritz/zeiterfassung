package de.bosc.timerecording.basic.business.model;

import java.io.Serializable;
import java.util.List;

import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.DayTimeentry;
import de.bosc.timerecording.basic.model.PublicHoliday;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.model.WorktimeSetting;

public class WorktimeDay implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer soll = 0;
	private Integer ist = 0;
	private Integer istWorktime = 0;
	private Integer breakTime = 0;
	private Integer pausetimeGeneral=0;
	private Integer additionDay=0;
	private Integer additionKinds=0;
	private Integer complementaryTime = 0;
	private PublicHoliday publicHoliday;
	private Double dayTimeFactor = 0.0;
	private Double holiday = 0.0;
	private Double complementary = 0.0;
	private Double sickness = 0.0;
	private Integer sicknessHours = 0;
	private DateString date;
	private List<Timeentry> timeentries;
	private WorktimeSetting worktimeSetting;
	private StringBuffer sb;
	private List<DayTimeentry> dayTimeentriesAll;
	private List<DayTimeentry> dayTimeentriesForCalculateBreaktime;

	
	public Integer getComplementaryTime() {
		return complementaryTime;
	}

	public void setComplementaryTime(Integer complementaryTime) {
		this.complementaryTime = complementaryTime;
	}

	public Integer getAdditionDay() {
		return additionDay;
	}

	public void setAdditionDay(Integer additionDay) {
		this.additionDay = additionDay;
	}

	public Integer getAdditionKinds() {
		return additionKinds;
	}

	public void setAdditionKinds(Integer additionKinds) {
		this.additionKinds = additionKinds;
	}

	public WorktimeDay(StringBuffer sb) {
		this.sb = sb;
	}
	
	public Integer getPausetimeGeneral() {
		return pausetimeGeneral;
	}

	public void setPausetimeGeneral(Integer pausetimeGeneral) {
		this.pausetimeGeneral = pausetimeGeneral;
	}

	public Double getDayTimeFactor() {
		return dayTimeFactor;
	}

	public void setDayTimeFactor(Double dayTimeFactor) {
		this.dayTimeFactor = dayTimeFactor;
	}

	public Integer getIstWorktime() {
		return istWorktime;
	}

	public void setIstWorktime(Integer istWorktime) {
		this.istWorktime = istWorktime;
	}

	public List<DayTimeentry> getDayTimeentriesAll() {
		return dayTimeentriesAll;
	}

	public void setDayTimeentriesAll(List<DayTimeentry> dayTimeentriesAll) {
		this.dayTimeentriesAll = dayTimeentriesAll;
	}

	public List<DayTimeentry> getDayTimeentriesForCalculateBreaktime() {
		return dayTimeentriesForCalculateBreaktime;
	}

	public void setDayTimeentriesForCalculateBreaktime(List<DayTimeentry> dayTimeentriesForCalculateBreaktime) {
		this.dayTimeentriesForCalculateBreaktime = dayTimeentriesForCalculateBreaktime;
	}

	public Double getComplementary() {
		return complementary;
	}

	public void setComplementary(Double complementary) {
		this.complementary = complementary;
	}

	public StringBuffer getSb() {
		return sb;
	}
	public List<Timeentry> getTimeentries() {
		return timeentries;
	}
	public void setTimeentries(List<Timeentry> timeentries) {
		this.timeentries = timeentries;
	}
	public WorktimeSetting getWorktimeSetting() {
		return worktimeSetting;
	}
	public void setWorktimeSetting(WorktimeSetting worktimeSetting) {
		this.worktimeSetting = worktimeSetting;
	}
	public Integer getSoll() {
		return soll;
	}
	public void setSoll(Integer soll) {
		this.soll = soll;
	}
	public Integer getIst() {
		return ist;
	}
	public void setIst(Integer ist) {
		this.ist = ist;
	}
	public Integer getBreakTime() {
		return breakTime;
	}
	public void setBreakTime(Integer breakTime) {
		this.breakTime = breakTime;
	}

	public PublicHoliday getPublicHoliday() {
		return publicHoliday;
	}

	public void setPublicHoliday(PublicHoliday publicHoliday) {
		this.publicHoliday = publicHoliday;
	}
	public Double getSickness() {
		return sickness;
	}

	public void setSickness(Double sickness) {
		this.sickness = sickness;
	}

	public Integer getSicknessHours() {
		return sicknessHours;
	}

	public void setSicknessHours(Integer sicknessHours) {
		this.sicknessHours = sicknessHours;
	}

	public Double getHoliday() {
		return holiday;
	}

	public void setHoliday(Double holiday) {
		this.holiday = holiday;
	}

	public DateString getDate() {
		return date;
	}
	public void setDate(DateString date) {
		this.date = date;
	}
	public boolean isPublicHoliday(){
		return publicHoliday != null;
	}
	
}
