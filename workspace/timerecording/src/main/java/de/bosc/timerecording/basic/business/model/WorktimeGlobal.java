package de.bosc.timerecording.basic.business.model;

import java.io.Serializable;
import java.util.List;

import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.TimeentryKind;

public class WorktimeGlobal implements Serializable{
	private static final long serialVersionUID = 1L;

	private List<TimeentryKind> timeentryKinds;
	
	public WorktimeGlobal(PersistenceService persistenceService) {
		this.timeentryKinds = persistenceService.getTimeentryKinds();
	}

	public List<TimeentryKind> getTimeentryKinds() {
		return timeentryKinds;
	}

	
}
