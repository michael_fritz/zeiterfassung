package de.bosc.timerecording.basic.business.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.model.AccountingMonth;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.model.TimeentryKind;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.service.TimeentryService;

public class WorktimeMonth implements Serializable{

	private static final long serialVersionUID = 1L;
	private WorktimeYear worktimeYear;
	private Map<DateString, List<Timeentry>> timeentriesMonth;
	private MonthString month;
		
	public WorktimeMonth(PersistenceService persistenceServerice,WorktimeYear worktimeYear,MonthString month) {
		this.worktimeYear = worktimeYear;
		this.month = month;
	}
	
	public MonthString getMonth() {
		return month;
	}

	public Map<DateString, List<Timeentry>> getTimeentriesMonth(PersistenceService persistenceServerice) {
		if(timeentriesMonth == null){
			timeentriesMonth = new TimeentryService(persistenceServerice).getTimeentriesMonth(getPerson().getPersonalNumber(), month);
		}
		return timeentriesMonth;
	}

	public WorktimeYear getWorktimeYear() {
		return worktimeYear;
	}

	public void setWorktimeYear(WorktimeYear worktimeYear) {
		this.worktimeYear = worktimeYear;
	}

	public Person getPerson() {
		return worktimeYear.getPerson();
	}

	public Map<Date, WorktimeSetting> getWorktimeSettings() {
		return worktimeYear.getWorktimeSettings();
	}

	public AccountingMonth getAccountingMonth() {
		return worktimeYear.getAccountingMonths().get(month);
	}
	public List<TimeentryKind> getTimeentryKinds() {
		return worktimeYear.getTimeentryKinds();
	}
}
