package de.bosc.timerecording.basic.business.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.model.AccountingPerson;
import de.bosc.timerecording.basic.model.AccountingYear;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.AccountingPersonService;
import de.bosc.timerecording.basic.service.AccountingYearService;
import de.bosc.timerecording.basic.service.WorktimeSettingService;

public class WorktimePerson implements Serializable{

	private static final long serialVersionUID = 1L;

	private Map<Date, WorktimeSetting> worktimeSettings;
	private AccountingPerson accountingPerson;
	private Map<YearString, AccountingYear> accountingYears;
	private PersistenceService persistenceService;
	private Person person;
	
	public WorktimePerson(PersistenceService persistenceService, Person person) {
		this.persistenceService = persistenceService;
		this.person = person;
		try {
			worktimeSettings = new WorktimeSettingService(persistenceService).getWorktimeSettings(person);
			accountingYears = new AccountingYearService(persistenceService).getAccountingYears(person);
			accountingPerson = new AccountingPersonService(persistenceService).getAccountingPerson(person);
		} catch (PersistenceException e) {
			persistenceService.getLogger().error("", e);
		}
	}

	public AccountingPerson getAccountingPerson() {
		return accountingPerson;
	}

	public Map<YearString, AccountingYear> getAccountingYears() {
		return accountingYears;
	}

	public Map<Date, WorktimeSetting> getWorktimeSettings() {
		return worktimeSettings;
	}

	public PersistenceService getPersistenceService() {
		return persistenceService;
	}
	public Person getPerson() {
		return person;
	}
	
}
