package de.bosc.timerecording.basic.business.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.model.AccountingMonth;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.PublicHoliday;
import de.bosc.timerecording.basic.model.TimeentryKind;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.AccountingMonthService;
import de.bosc.timerecording.basic.service.PublicHolidayService;

public class WorktimeYear implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Map<MonthString, AccountingMonth> accountingMonths;
	private Map<DateString,PublicHoliday> mapHolidays;
	private WorktimeGlobal worktimeGlobal;
	private WorktimePerson worktimePerson;
	private YearString year;
	
	public WorktimeYear(PersistenceService persistenceServerice,WorktimeYear worktimeYear, WorktimePerson worktimePerson) {
		this(persistenceServerice,worktimeYear.getWorktimeGlobal(),worktimePerson,worktimeYear.getYear());
	}

	public WorktimeYear(PersistenceService persistenceServerice,WorktimeGlobal worktimeGlobal, WorktimePerson worktimePerson, YearString year) {
		this.worktimeGlobal = worktimeGlobal;
		this.year = year;
		this.worktimePerson = worktimePerson;
		accountingMonths = new AccountingMonthService(persistenceServerice).getAccountingMonthsYear(getPerson(),year);
		this.mapHolidays = new PublicHolidayService(persistenceServerice).getPublicHolidaysForYear(year);
	}

	public YearString getYear() {
		return year;
	}


	public Map<DateString, PublicHoliday> getMapHolidays() {
		return mapHolidays;
	}

	public Map<MonthString, AccountingMonth> getAccountingMonths() {
		return accountingMonths;
	}

	public Person getPerson() {
		return worktimePerson.getPerson();
	}

	public WorktimeGlobal getWorktimeGlobal() {
		return worktimeGlobal;
	}
	
	public Map<Date, WorktimeSetting> getWorktimeSettings() {
		return worktimePerson.getWorktimeSettings();
	}
	
	public List<TimeentryKind> getTimeentryKinds() {
		return worktimeGlobal.getTimeentryKinds();
	}

	public WorktimePerson getWorktimePerson() {
		return worktimePerson;
	}


}
