package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.AccountingMonth;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.YearString;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoAccountingMonthController {

	/**
	 *  Liefert eine Liste aller Monatsabrechnungen f[r eine Person
	 * 
	 * @param persistenceService
	 * @return
	 */
	public Map<MonthString,AccountingMonth> getAccountingMonthsYear(DominoPersistenceService persistenceService, Person person,YearString year){
		Map<MonthString,AccountingMonth> accountingMonths = new TreeMap<MonthString, AccountingMonth>();
		Database dbData = persistenceService.getDbData(year);
		Logger logger = persistenceService.getLogger();

		StringBuffer sb = new StringBuffer();
		sb.append("#### getAccountingMonthsYear: " + person.getPersonalNumber() + "\n");
		sb.append("--- Personalnummer: " + person.getPersonalNumber() + "\n");
		sb.append("--- Jahr: " + year.toString() + "\n");
		try {
			if(person.getPersonalNumber() == null){
				return accountingMonths;
			}
			View vw = dbData.getView("($accountingMonthYear)");
			Vector<String> v = new Vector<String>();
			v.add(year.toString());
			v.add(person.getPersonalNumber());
			DocumentCollection coll = vw.getAllDocumentsByKey(v,true);
			Document doc = coll.getFirstDocument();
			sb.append("--- found: " + coll.getCount() + "\n");	
			while(doc != null){
				Document docNext = coll.getNextDocument(doc);
				sb.append("--- found: " + doc.getItemValueString("personalNumber") + " - month: " + doc.getItemValueString("month") + "\n");	
				AccountingMonth accountingMonth = getAccountingMonthFromDoc(persistenceService, doc);
				accountingMonths.put(accountingMonth.getMonth(),accountingMonth);
				sb.append("--- accountingMonth: " + accountingMonth.getMonth() + "\n");					
				doc.recycle();
				doc = docNext;
			}
		} catch (Exception e) {
			logger.error("Error in DominoPersistenceService.getAccountingMonthsYear", e);
		} finally{
			logger.trace(sb.toString(),this.getClass());
		}
		return accountingMonths;
	}

	/**
	 *  Liefert eine Liste aller Monatsabrechnungen f�r einen Monat
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<AccountingMonth> getAccountingMonths(DominoPersistenceService persistenceService, MonthString month){
		List<AccountingMonth>  accountingMonths = new ArrayList<AccountingMonth>();
		Logger logger = persistenceService.getLogger();

		StringBuffer sb = new StringBuffer();
		sb.append("#### getAccountingMonths:\n");
		sb.append("--- Monat: " + month.toString() + "\n");
		try {
			Database dbData = persistenceService.getDbData(new YearString(month.getDate()));
			View vw = dbData.getView("($accountingMonth)");
			DocumentCollection coll = vw.getAllDocumentsByKey(month.toString(),true);
			Document doc = coll.getFirstDocument();
			while(doc != null){
				Document docNext = coll.getNextDocument(doc);
				AccountingMonth accountingMonth = getAccountingMonthFromDoc(persistenceService, doc);
				accountingMonths.add(accountingMonth);
				sb.append("--- accountingMonth: " + accountingMonth.getMonth() + " inclusice time:" + accountingMonth.getInclusiveTime() + "\n");					
				doc.recycle();
				doc = docNext;
			}
		} catch (Exception e) {
			logger.error("Error in getAccountingMonths", e);
		} finally{
			logger.trace(sb.toString(),this.getClass());
		}
		return accountingMonths;
	}	
	
	/**
	 * Speichert ein AccountingMonth Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public boolean saveAccountingMonth(DominoPersistenceService persistenceService, AccountingMonth accountingMonth){
		boolean update = false;
		Logger logger = persistenceService.getLogger();
		try {
			YearString year = new YearString(accountingMonth.getMonth().getDate());
			Document doc = null;
			Database dbData = persistenceService.getDbData(year);
			logger.info("saveAccountingMonth: " + accountingMonth.getPersonalNumber() + " unid: " + accountingMonth.getUnid()  + " db: " + dbData.getFilePath(),this.getClass());
			if(accountingMonth.getUnid() == null || accountingMonth.getUnid().equals("")){
				logger.debug("saveAccountingMonth: create new accountingMonth",this.getClass());
				doc = dbData.createDocument();
				accountingMonth.setUnid(doc.getUniversalID());
			}else{
				doc = dbData.getDocumentByUNID(accountingMonth.getUnid());
				logger.debug("saveAccountingMonth: get existing accountingMonth from database ",this.getClass());
			}
			AccountingMonth origAccountingMonth = null;
			if(doc == null){
				throw new PersistenceException("");
			}else if(!doc.isNewNote()){
				origAccountingMonth = getAccountingMonth(persistenceService, doc.getUniversalID(),year);
			}
			
			if(origAccountingMonth == null || !accountingMonth.equals(origAccountingMonth)){
				doc.replaceItemValue("form", "AccountingMonth");
				doc.replaceItemValue("personalNumber", accountingMonth.getPersonalNumber());
				Person person = persistenceService.getPersonService().getPerson(accountingMonth.getPersonalNumber());
				if(person != null){
					doc.replaceItemValue("fullname", person.getFullname());				
				}
				doc.replaceItemValue("month", accountingMonth.getMonth().toString());
				doc.replaceItemValue("daysHoliday", accountingMonth.getDaysHoliday());
				doc.replaceItemValue("daysPublicHoliday", accountingMonth.getDaysPublicHoliday());
				doc.replaceItemValue("daysSickness", accountingMonth.getDaysSickness());
				doc.replaceItemValue("ist", accountingMonth.getIst());
				doc.replaceItemValue("overtime", accountingMonth.getOvertime());
				doc.replaceItemValue("inclusiveTime", accountingMonth.getInclusiveTime());
				doc.replaceItemValue("soll", accountingMonth.getSoll());
				doc.save();
				doc.recycle();
				update = true;
				logger.info("saveAccountingMonth: AccountingMonth saved ",this.getClass());				
			}
		} catch (Exception e) {
			logger.error("Error AccountingMonthy ", e);
		} 
		return update;
	}
	
	/**
	 * Erzeugt ein AccountingMonth Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 */
	private AccountingMonth getAccountingMonthFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException, PersistenceException{
		AccountingMonth accountingMonth = new AccountingMonth();
		accountingMonth.setUnid(doc.getUniversalID());
		accountingMonth.setPersonalNumber(doc.getItemValueString("personalNumber"));
		accountingMonth.setMonth(new MonthString(doc.getItemValueString("month")));
		accountingMonth.setDaysHoliday(doc.getItemValueDouble("daysHoliday"));
		accountingMonth.setDaysPublicHoliday(doc.getItemValueDouble("daysPublicHoliday"));
		accountingMonth.setDaysSickness(doc.getItemValueDouble("daysSickness"));
		accountingMonth.setIst(doc.getItemValueInteger("ist"));
		accountingMonth.setInclusiveTime(doc.getItemValueInteger("inclusiveTime"));
		persistenceService.getLogger().debug("inclusive time:" + doc.getItemValueInteger("inclusiveTime"),this.getClass());
		accountingMonth.setOvertime(doc.getItemValueInteger("overtime"));
		accountingMonth.setSoll(doc.getItemValueInteger("soll"));
		return accountingMonth;
	}

	private AccountingMonth getAccountingMonth(DominoPersistenceService persistenceService, String unid, YearString year){
		AccountingMonth accountingMonth = null;
		Database dbData = persistenceService.getDbData(year);
		Document doc;
		try {
			doc = dbData.getDocumentByUNID(unid);
			accountingMonth = getAccountingMonthFromDoc(persistenceService, doc);
		} catch (Exception e) {
			persistenceService.getLogger().error("", e);
		} 
		return accountingMonth;
	}
	
}
