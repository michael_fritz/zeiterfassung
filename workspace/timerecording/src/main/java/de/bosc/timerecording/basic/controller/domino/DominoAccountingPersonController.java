package de.bosc.timerecording.basic.controller.domino;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.AccountingPerson;
import de.bosc.timerecording.basic.model.Person;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoAccountingPersonController {

	/**
	 *  Liefert die Gesamtabrechnungen f�r eine Person
	 * 
	 * @param persistenceService
	 * @return
	 */
	public AccountingPerson getAccountingPerson(DominoPersistenceService persistenceService, Person person){
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();
		AccountingPerson accountingPerson = new AccountingPerson();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getAccountingPerson:\n");
		sb.append("--- Personalnummer: " + person.getPersonalNumber() + "\n");
		try {
			if(person.getPersonalNumber() == null){
				return accountingPerson;
			}

			View vw = dbConfig.getView("($accountingPerson)");
			Document doc = vw.getDocumentByKey(person.getPersonalNumber(),true);

			if(doc != null){
				accountingPerson = getAccountingPersonFromDoc(persistenceService, doc);
				doc.recycle();
			}
		} catch (Exception e) {
			logger.error("Error in getAccountingPerson", e);
		} finally{
			logger.trace(sb.toString(),this.getClass());
		}
		return accountingPerson;
	}
	
	
	/**
	 * Speichert ein AccountingMonth Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public boolean saveAccountingPerson(DominoPersistenceService persistenceService, AccountingPerson accountingPerson){
		boolean update = false;
		Logger logger = persistenceService.getLogger();
		try {
			Document doc = null;
			Database dbConfig = persistenceService.getDbConfig();
			logger.info("saveAccountingPerson: " + accountingPerson.getPersonalNumber() + " unid: " + accountingPerson.getUnid()  + " db: " + dbConfig.getFilePath(),this.getClass());
			if(accountingPerson.getUnid() == null || accountingPerson.getUnid().equals("")){
				logger.debug("saveAccountingPerson: create new accountingPerson",this.getClass());
				doc = dbConfig.createDocument();
				accountingPerson.setUnid(doc.getUniversalID());
			}else{
				doc = dbConfig.getDocumentByUNID(accountingPerson.getUnid());
				logger.debug("saveAccountingPerson: get existing accountingPerson from database ",this.getClass());
			}
			AccountingPerson origAccountingPerson = null;
			if(doc == null){
				throw new PersistenceException("");
			}else if(!doc.isNewNote()){
				origAccountingPerson = getAccountingPerson(persistenceService, doc.getUniversalID());
			}
			
			if(origAccountingPerson == null || !accountingPerson.equals(origAccountingPerson)){
				doc.replaceItemValue("form", "AccountingPerson");
				doc.replaceItemValue("personalNumber", accountingPerson.getPersonalNumber());
				Person person = persistenceService.getPersonService().getPerson(accountingPerson.getPersonalNumber());
				if(person != null){
					doc.replaceItemValue("fullname", person.getFullname());				
				}
				doc.replaceItemValue("daysHoliday", accountingPerson.getDaysHoliday());
				doc.replaceItemValue("daysSickness", accountingPerson.getDaysSickness());
				doc.replaceItemValue("startHoliday", accountingPerson.getStartHoliday());
				doc.replaceItemValue("startOvertime", accountingPerson.getStartOvertime());
				doc.replaceItemValue("overtime", accountingPerson.getOvertime());
				doc.save();
				doc.recycle();
				update = true;
				logger.info("saveAccountingPerson: AccountingPerson saved ",this.getClass());				
			}
		} catch (Exception e) {
			logger.error("Error AccountingPerson ", e);
		} 
		return update;
	}
	
	/**
	 * Erzeugt ein AccountingPerson Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 */
	private AccountingPerson getAccountingPersonFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException, PersistenceException{
		AccountingPerson accountingPerson = new AccountingPerson();
		accountingPerson.setUnid(doc.getUniversalID());
		accountingPerson.setLastUpdate(doc.getLastModified().toJavaDate());
		accountingPerson.setPersonalNumber(doc.getItemValueString("personalNumber"));
		accountingPerson.setDaysHoliday(doc.getItemValueDouble("daysHoliday"));
		accountingPerson.setDaysSickness(doc.getItemValueDouble("daysSickness"));
		accountingPerson.setStartHoliday(doc.getItemValueDouble("startHoliday"));
		accountingPerson.setStartOvertime(doc.getItemValueDouble("startOvertime"));
		accountingPerson.setOvertime(doc.getItemValueInteger("overtime"));
		return accountingPerson;
	}

	private AccountingPerson getAccountingPerson(DominoPersistenceService persistenceService, String unid){
		AccountingPerson accountingPerson = null;
		Database dbConfig = persistenceService.getDbConfig();
		Document doc;
		try {
			doc = dbConfig.getDocumentByUNID(unid);
			accountingPerson = getAccountingPersonFromDoc(persistenceService, doc);
		} catch (Exception e) {
			persistenceService.getLogger().error("", e);
		} 
		return accountingPerson;
	}
}

