package de.bosc.timerecording.basic.controller.domino;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.entity.HolidayEntitlementEntity;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.AccountingYear;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.YearString;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoAccountingYearController {

	/**
	 *  Liefert eine Liste aller Monatsabrechnungen f[r eine Person
	 * 
	 * @param persistenceService
	 * @return
	 */
	public Map<YearString,AccountingYear> getAccountingYears(DominoPersistenceService persistenceService, Person person){
		Map<YearString,AccountingYear> accountingYears = new TreeMap<YearString, AccountingYear>();
		List<HolidayEntitlementEntity> holidayEntitlements = new DominoHolidayEntitlementController().getHolidayEntitlementForPerson(persistenceService, person.getPersonalNumber());
		Map<YearString,HolidayEntitlementEntity> mapHolidayEntitlements = new HashMap<YearString, HolidayEntitlementEntity>();
		for(HolidayEntitlementEntity h:holidayEntitlements){
			mapHolidayEntitlements.put(h.getYear(), h);
		}
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();

		StringBuffer sb = new StringBuffer();
		sb.append("#### getAccountingYears:\n");
		sb.append("--- Personalnummer: " + person.getPersonalNumber() + "\n");
		try {
			if(person.getPersonalNumber() == null){
				return accountingYears;
			}
			View vw = dbConfig.getView("($accountingYear)");
			Vector<String> v = new Vector<String>();
			v.add(person.getPersonalNumber());
			DocumentCollection coll = vw.getAllDocumentsByKey(v,true);
			Document doc = coll.getFirstDocument();
			while(doc != null){
				Document docNext = coll.getNextDocument(doc);
				AccountingYear accountingYear = getAccountingYearFromDoc(persistenceService, doc);
				if(mapHolidayEntitlements.containsKey(accountingYear.getYear())){
					accountingYear.setDaysGivenHoliday(mapHolidayEntitlements.get(accountingYear.getYear()).getEffectiveDays());
				}
				accountingYears.put(accountingYear.getYear(),accountingYear);
				sb.append("--- accountingYear: " + accountingYear.getYear() + "\n");					
				doc.recycle();
				doc = docNext;
			}
		} catch (Exception e) {
			logger.error("Error in getAccountingYears", e);
		} finally{
			logger.trace(sb.toString(),this.getClass());
		}
		return accountingYears;
	}
	
	/**
	 * Speichert ein AccountingMonth Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public boolean saveAccountingYear(DominoPersistenceService persistenceService, AccountingYear accountingYear){
		boolean update = false;
		Logger logger = persistenceService.getLogger();
		try {
			Document doc = null;
			Database dbConfig = persistenceService.getDbConfig();
			logger.info("saveAccountingYear: " + accountingYear.getPersonalNumber() + " unid: " + accountingYear.getUnid()  + " db: " + dbConfig.getFilePath(),this.getClass());
			if(accountingYear.getUnid() == null || accountingYear.getUnid().equals("")){
				logger.debug("saveAccountingYear: create new accountingYear",this.getClass());
				doc = dbConfig.createDocument();
				accountingYear.setUnid(doc.getUniversalID());
			}else{
				doc = dbConfig.getDocumentByUNID(accountingYear.getUnid());
				logger.debug("saveAccountingYear: get existing accountingYear from database ",this.getClass());
			}
			AccountingYear origAccountingYear = null;
			if(doc == null){
				throw new PersistenceException("");
			}else if(!doc.isNewNote()){
				origAccountingYear = getAccountingYear(persistenceService, doc.getUniversalID(),accountingYear.getYear());
			}
			
			if(origAccountingYear == null || !accountingYear.equals(origAccountingYear)){
				doc.replaceItemValue("form", "AccountingYear");
				doc.replaceItemValue("personalNumber", accountingYear.getPersonalNumber());
				Person person = persistenceService.getPersonService().getPerson(accountingYear.getPersonalNumber());
				if(person != null){
					doc.replaceItemValue("fullname", person.getFullname());				
				}
				doc.replaceItemValue("year", accountingYear.getYear().toString());
				doc.replaceItemValue("daysHoliday", accountingYear.getDaysHoliday());
				doc.replaceItemValue("daysPublicHoliday", accountingYear.getDaysPublicHoliday());
				doc.replaceItemValue("daysSickness", accountingYear.getDaysSickness());
				doc.replaceItemValue("overtime", accountingYear.getOvertime());
				doc.save();
				doc.recycle();
				update = true;
				logger.info("saveAccountingYear: AccountingYear saved ",this.getClass());				
			}
		} catch (Exception e) {
			logger.error("Error AccountingMonthy ", e);
		} 
		return update;
	}
	
	/**
	 * Erzeugt ein AccountingMonth Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 */
	private AccountingYear getAccountingYearFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException, PersistenceException{
		AccountingYear accountingYear = new AccountingYear();
		accountingYear.setUnid(doc.getUniversalID());
		accountingYear.setPersonalNumber(doc.getItemValueString("personalNumber"));
		accountingYear.setYear(new YearString(doc.getItemValueString("year")));
		accountingYear.setDaysHoliday(doc.getItemValueDouble("daysHoliday"));
		accountingYear.setDaysPublicHoliday(doc.getItemValueDouble("daysPublicHoliday"));
		accountingYear.setDaysSickness(doc.getItemValueDouble("daysSickness"));
		accountingYear.setOvertime(doc.getItemValueInteger("overtime"));
		return accountingYear;
	}

	private AccountingYear getAccountingYear(DominoPersistenceService persistenceService, String unid,YearString year){
		AccountingYear accountingYear = null;
		Database dbConfig = persistenceService.getDbConfig();
		Document doc;
		try {
			doc = dbConfig.getDocumentByUNID(unid);
			accountingYear = getAccountingYearFromDoc(persistenceService, doc);
		} catch (Exception e) {
			persistenceService.getLogger().error("", e);
		} 
		return accountingYear;
	}
}
