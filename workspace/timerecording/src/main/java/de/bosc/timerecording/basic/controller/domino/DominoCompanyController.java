package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.List;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Company;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoCompanyController {

	/**
	 *  Liefert eine Liste aller aktiven Firmen
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Company> getActiveCompanies(DominoPersistenceService persistenceService) {
		List<Company> companies = new ArrayList<Company>();
		for (Company company : getCompanies(persistenceService)) {
			if(company.isActive()){
				companies.add(company);
			}
		}
		return companies;
	}
	
	
	/**
	 * Liefert eine Liste aller Firmen
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Company> getCompanies(DominoPersistenceService persistenceService) {
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();

		StringBuffer sb = new StringBuffer();
		sb.append("#### getCompanies: ");
		List<Company> companies = new ArrayList<Company>();
		try {
			View vw = dbConfig.getView("($companies)");
			Document doc = vw.getFirstDocument();
			while(doc != null){
				Document docNext = vw.getNextDocument(doc);
				Company company = getCompanyFromDoc(persistenceService, doc);
				companies.add(company);
				sb.append("--- company: " + company.getName());
				doc.recycle();
				doc = docNext;
			}
		} catch (NotesException e) {
			logger.error("Error in DominoPersistenceService.getCompanies", e);
		} catch (PersistenceException e) {
			logger.error("Error in DominoPersistenceService.getCompanies", e);
		} finally{
			logger.trace(sb.toString(),this.getClass());
		}
		return companies;
	}
	
	
	/**
	 * Speichert ein Company Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public void saveCompany(DominoPersistenceService persistenceService, Company company){
		Document doc;
		Logger logger = persistenceService.getLogger();
		try {
			Database dbConfig = persistenceService.getDbConfig();
			logger.info("saveCompany: " + company.getName() + " unid: " + company.getUnid()  + " db: " + dbConfig.getFilePath(),this.getClass());
			persistenceService.setVersionControlFields(company);
			if(company.getUnid() == null || company.getUnid().equals("")){
				logger.debug("saveCompany: create new company",this.getClass());
				doc = dbConfig.createDocument();
				company.setUnid(doc.getUniversalID());
				company.setPrimaryKey(doc.getUniversalID());
			}else{
				doc = dbConfig.getDocumentByUNID(company.getUnid());
				logger.debug("saveCompany: get existing company from database ",this.getClass());
			}
			if(doc == null){
				throw new PersistenceException("");
			}
			
			Company origCompany = getCompanyFromDoc(persistenceService, doc);
			logger.debug("saveCompany: company values changed: " + (!origCompany.equals(company)),this.getClass());
			if(!origCompany.equals(company)){
				doc.replaceItemValue("docid", company.getPrimaryKey());
				doc.replaceItemValue("form", "Device");
				doc.replaceItemValue("companyName", company.getName());
				doc.replaceItemValue("Locationdocid", company.getLocationDocid());
				doc.replaceItemValue("active", company.isActive()?"1":"0");	
				String versionControl = persistenceService.serializeObjectToString(company);				
				persistenceService.writeVersionControl(doc,versionControl);
				doc.save();
				logger.info("saveCompany: company saved ",this.getClass());
			}
		} catch (Exception e) {
			logger.error("Error DominoCompanyPersistenceService.saveCompany ", e);
		} 
		
	}
	
	
	
	/**
	 * Erzeugt ein Company Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 */
	private Company getCompanyFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException, PersistenceException{
		Company company = new Company();
		company.setUnid(doc.getUniversalID());
		company.setActive(doc.getItemValueString("active").equals("1"));
		company.setPrimaryKey(doc.getItemValueString("docid"));
		company.setName(doc.getItemValueString("companyName"));
		company.setLocationDocid(doc.getItemValueString("locationdocid"));
		company.setLocation(persistenceService.getLocationService().getLocation(company.getLocationDocid()));
		company.setListVersions(getHistory(persistenceService,doc));
		return company;
	}

	
	/**
	 * generiert eine Liste von Versionen aus der Historie des Dokuments
	 * Die liste wird aus Feldern des Dokuments gelesen die dort serialisiert 
	 * gespeichert sind und f�gt Sie der Liste in umgekehrter Reihenfolge zu
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 */
	public List<Company> getHistory(DominoPersistenceService persistenceService,Document doc){
		List<Company> listHistory = new ArrayList<Company>();
		List<String> entries = persistenceService.getObjectHistory(doc);
		Logger logger = persistenceService.getLogger();
		for(int i = entries.size()-1;i>=0;i--){
			try {
				Company company = (Company) persistenceService.deserializeObjectfromString(entries.get(i),Company.class);
				listHistory.add(company);
			} catch (Exception e) {
				logger.error("Error DominoPersistenceService.getHistory ", e);
			} 
		}
		return listHistory;
	}
}
