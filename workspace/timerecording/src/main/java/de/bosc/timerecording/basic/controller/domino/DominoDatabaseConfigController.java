package de.bosc.timerecording.basic.controller.domino;

import java.util.HashMap;
import java.util.Map;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.enumeration.LogLevel;
import de.bosc.timerecording.basic.model.DatabaseConfiguration;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoDatabaseConfigController {
	
	
	public DatabaseConfiguration getDatabaseConfig(DominoPersistenceService persistenceService){
		
		Database dbConfig = persistenceService.getDbConfig();
		DatabaseConfiguration config = new DatabaseConfiguration();
		try {
			View vw = dbConfig.getView("($databaseConfig)");
			Document doc = vw.getFirstDocument();
			if(doc != null){
				config.setCutOffTime(doc.getItemValueInteger("cutOffTime"));
				config.setPathDbData(doc.getItemValueString("pathDbData"));
				config.setPathDbLog(doc.getItemValueString("pathDbLog"));
				config.setStartYear(doc.getItemValueInteger("startYear"));
				if(!doc.getItemValueString("LogLevel").equals("")){
					config.setLogLevel(LogLevel.getLogLevel(doc.getItemValueString("LogLevel")));					
				}else{
					config.setLogLevel(LogLevel.INFO);
				}
				doc.recycle();
			}
			vw = dbConfig.getView("($LogConfigs)");
			doc = vw.getFirstDocument();
			Map<String,LogLevel> logLevelsClass = new HashMap<String,LogLevel>();
			while(doc != null){
				Document docNext = vw.getNextDocument(doc);
				logLevelsClass.put(doc.getItemValueString("logClass"),LogLevel.getLogLevel(doc.getItemValueString("LogLevel")));;
				doc.recycle();
				doc = docNext;
			}
			config.setLogLevelsClass(logLevelsClass);
		} catch (NotesException e) {
			e.printStackTrace();
		} 
		return config;		
	}
	
}
