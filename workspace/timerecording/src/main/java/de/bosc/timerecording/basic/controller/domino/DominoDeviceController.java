package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.List;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Device;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoDeviceController {

		
	/**
	 * Liefert eine Liste aller Devices
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Device> getDevices(DominoPersistenceService persistenceService) {
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();

		StringBuffer sb = new StringBuffer();
		sb.append("#### getDevices: ");
		List<Device> devices = new ArrayList<Device>();
		try {
			View vw = dbConfig.getView("($devices)");
			Document doc = vw.getFirstDocument();
			while(doc != null){
				Document docNext = vw.getNextDocument(doc);
				Device device = getDeviceFromDoc(persistenceService,doc);
				devices.add(device);
				sb.append("--- device: " + device.getDeviceName() + " (" + device.getDeviceKey() + ")  locationDocID: " + device.getLocationdocid() + "\n");
				doc.recycle();
				doc = docNext;
			}
		} catch (NotesException e) {
			logger.error("Error in DominoPersistenceService.getDevices", e);
		} catch (PersistenceException e) {
			logger.error("Error in DominoPersistenceService.getDevices", e);
		} finally{
			logger.debug(sb.toString(),this.getClass());
		}
		return devices;
	}
	
	
	/**
	 * Speichert ein Device Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public void saveDevice(DominoPersistenceService persistenceService, Device device) throws PersistenceException {
		Document doc;
		Logger logger = persistenceService.getLogger();
		try {
			Database dbConfig = persistenceService.getDbConfig();
			logger.trace("saveDevice: " + device.getDeviceName() + " unid: " + device.getUnid()  + " db: " + dbConfig.getFilePath(),this.getClass());
			persistenceService.setVersionControlFields(device);
			if(device.getUnid() == null || device.getUnid().equals("")){
				logger.debug("saveDevice: create new device",this.getClass());
				doc = dbConfig.createDocument();
				device.setUnid(doc.getUniversalID());
				device.setPrimaryKey(doc.getUniversalID());
			}else{
				doc = dbConfig.getDocumentByUNID(device.getUnid());
				logger.debug("saveDevice: get existing device from database ",this.getClass());
			}
			if(doc == null){
				throw new PersistenceException("");
			}
			
			Device origDevice = getDeviceFromDoc(persistenceService, doc);
			logger.debug("saveDevice: Device values changed: " + (!origDevice.equals(device)),this.getClass());
			if(!origDevice.equals(device)){
				doc.replaceItemValue("docid", device.getPrimaryKey());
				doc.replaceItemValue("form", "Device");
				doc.replaceItemValue("Companydocid", device.getCompanydocid());
				doc.replaceItemValue("Locationdocid", device.getLocationdocid());
				doc.replaceItemValue("DeviceManager", device.getDeviceManager());
				doc.replaceItemValue("devicename", device.getDeviceName());
				doc.replaceItemValue("devicekey", device.getDeviceKey());
				doc.replaceItemValue("active", device.isActive()?"1":"0");	
				String versionControl = persistenceService.serializeObjectToString(device);
				persistenceService.writeVersionControl(doc,versionControl);
				doc.save();
				View vw = dbConfig.getView("($devices)");
				vw.refresh();
				logger.debug("saveDevice: Device saved ",this.getClass());
			}
		} catch (Exception e) {
			logger.error("Error DominoPersistenceService.saveDevice ", e);
		} 
		
	}
	
	/**
	 * Erzeugt ein Device Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 * @throws PersistenceException
	 */
	public Device getDeviceFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException, PersistenceException{
		Device device = new Device();
		device.setUnid(doc.getUniversalID());
		device.setPrimaryKey(doc.getItemValueString("docid"));
		device.setActive(doc.getItemValueString("active").equals("1"));
		device.setDeviceKey(doc.getItemValueString("devicekey"));
		device.setDeviceName(doc.getItemValueString("deviceName"));	
		device.setDeviceManager(doc.getItemValue("deviceManager"));
		device.setCompanydocid(doc.getItemValueString("companydocid"));
		device.setLocationdocid(doc.getItemValueString("locationdocid"));
		device.setLocation(persistenceService.getLocationService().getLocation(device.getLocationdocid()));
		device.setCompany(persistenceService.getCompanyService().getCompany(device.getCompanydocid()));
		device.setListVersions(getHistory(persistenceService,doc));
		return device;
	}
	
	/**
	 * generiert eine Liste von Versionen aus der Historie des Dokuments
	 * Die liste wird aus Feldern des Dokuments gelesen die dort serialisiert 
	 * gespeichert sind und f�gt Sie der Liste in umgekehrter Reihenfolge zu
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 */
	public List<Device> getHistory(DominoPersistenceService persistenceService,Document doc){
		List<Device> listHistory = new ArrayList<Device>();
		List<String> entries = persistenceService.getObjectHistory(doc);
		Logger logger = persistenceService.getLogger();
		for(int i = entries.size()-1;i>=0;i--){
			try {
				Device device = (Device) persistenceService.deserializeObjectfromString(entries.get(i),Device.class);
				listHistory.add(device);
			} catch (Exception e) {
				logger.error("Error DominoPersistenceService.getHistory ", e);
			} 
		}
		return listHistory;
	}
}
