package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.List;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.entity.HolidayEntitlementEntity;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.YearString;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoHolidayEntitlementController {

	/**
	 * @param persistenceService
	 * @param personalNumber
	 * @return
	 */
	public List<HolidayEntitlementEntity> getHolidayEntitlementForPerson(DominoPersistenceService persistenceService, String personalNumber){
		
		List<HolidayEntitlementEntity> listHolidayEntitlement = new ArrayList<HolidayEntitlementEntity>();
		
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();

		StringBuffer sb = new StringBuffer();
		sb.append("#### getHolidayEntitlementForPerson --- personalNumber: " + personalNumber + "\n");
		try {
			View vw = dbConfig.getView("($HolidayEntitlement)");
			DocumentCollection coll = vw.getAllDocumentsByKey(personalNumber,true);
			Document doc = coll.getFirstDocument();
			while(doc != null){
				Document docNext = coll.getNextDocument(doc);
				HolidayEntitlementEntity holidayEntitlement = getHolidayEntitlementFromDoc(persistenceService,doc);
				listHolidayEntitlement.add(holidayEntitlement);
				sb.append("--- Holiday Entitlement: " + holidayEntitlement.getYear() + " " + holidayEntitlement.getAmountDays() + " unid: " + holidayEntitlement.getUnid() + "\n");
				doc.recycle();
				doc = docNext;
			}
		} catch (Exception e) {
			logger.error("Error in getHolidayEntitlementForPerson", e);
		} finally{
			logger.debug(sb.toString(),this.getClass());
		}
		
		return listHolidayEntitlement;
	}
	
	
	/**
	 * @param persistenceService
	 * @param holidayEntitlement
	 * @return
	 */
	public HolidayEntitlementEntity saveHolidayEntitlement(DominoPersistenceService persistenceService, HolidayEntitlementEntity holidayEntitlement){
		Document doc;
		Logger logger = persistenceService.getLogger();
		try {
			Database dbConfig = persistenceService.getDbConfig();
			logger.trace("saveHolidayEntitlement: " + holidayEntitlement.getFullname() + " unid: " + holidayEntitlement.getUnid()  + " db: " + dbConfig.getFilePath(),this.getClass());
			persistenceService.setVersionControlFields(holidayEntitlement);
			if(holidayEntitlement.getUnid() == null || holidayEntitlement.getUnid().equals("")){
				logger.debug("saveHolidayEntitlement: create new holidayEntitlement",this.getClass());
				doc = dbConfig.createDocument();
				holidayEntitlement.setUnid(doc.getUniversalID());
				holidayEntitlement.setPrimaryKey(doc.getUniversalID());
			}else{
				doc = dbConfig.getDocumentByUNID(holidayEntitlement.getUnid());
				logger.debug("saveHolidayEntitlement: get existing holidayEntitlement from database ",this.getClass());
			}
			if(doc == null){
				throw new PersistenceException("");
			}
			
			HolidayEntitlementEntity origHolidayEntitlement = getHolidayEntitlementFromDoc(persistenceService, doc);
			logger.debug("saveHolidayEntitlement: HolidayEntitlement values changed: " + (!origHolidayEntitlement.equals(holidayEntitlement)),this.getClass());
			if(!origHolidayEntitlement.equals(holidayEntitlement)){
				doc.replaceItemValue("docid", holidayEntitlement.getPrimaryKey());
				doc.replaceItemValue("form", "HolidayEntitlement");
				doc.replaceItemValue("docid", holidayEntitlement.getPrimaryKey());
				doc.replaceItemValue("year", holidayEntitlement.getYear().getYear());
				doc.replaceItemValue("AmountDays", holidayEntitlement.getAmountDays());
				doc.replaceItemValue("EffectiveDays", holidayEntitlement.getEffectiveDays());
				doc.replaceItemValue("PersonalNumber", holidayEntitlement.getPersonalNumber());
				doc.replaceItemValue("comment", holidayEntitlement.getComment());
				doc.replaceItemValue("fullname", holidayEntitlement.getFullname());
				logger.debug("before versionControl");
				String versionControl = persistenceService.serializeObjectToString(holidayEntitlement);
				persistenceService.writeVersionControl(doc,versionControl);
				logger.debug("user: " + persistenceService.getSession().getEffectiveUserName());
				doc.save();
				logger.debug("after save");
				View vw = dbConfig.getView("($HolidayEntitlement)");
				vw.refresh();
				logger.debug("saveHolidayEntitlement: HolidayEntitlement saved ",this.getClass());
			}
		} catch (Exception e) {
			logger.error("Error DominoPersistenceService.saveHolidayEntitlement ", e);
		} 
		
		return holidayEntitlement;
	}
	
	/**
	 * Erzeugt ein HolidayEntitlement Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 * @throws PersistenceException
	 */
	public HolidayEntitlementEntity getHolidayEntitlementFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException, PersistenceException{
		HolidayEntitlementEntity holidayEntitlement = new HolidayEntitlementEntity();
		holidayEntitlement.setUnid(doc.getUniversalID());
		holidayEntitlement.setPrimaryKey(doc.getItemValueString("docid"));	
		holidayEntitlement.setYear(new YearString(doc.getItemValueString("year")));	
		holidayEntitlement.setAmountDays(doc.getItemValueInteger("amountDays"));
		holidayEntitlement.setEffectiveDays(doc.getItemValueInteger("effectiveDays"));
		holidayEntitlement.setPersonalNumber(doc.getItemValueString("personalNumber"));
		holidayEntitlement.setFullname(doc.getItemValueString("fullname"));
		holidayEntitlement.setComment(doc.getItemValueString("comment"));
		return holidayEntitlement;
	}
}
