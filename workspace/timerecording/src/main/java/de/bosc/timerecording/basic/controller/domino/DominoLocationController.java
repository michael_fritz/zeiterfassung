package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.List;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Location;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoLocationController {
	
	/**
	 *  Liefert eine Liste aller aktiven Standorte
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Location> getActiveLocations(DominoPersistenceService persistenceService) {
		List<Location> locations = new ArrayList<Location>();
		for (Location location : getLocations(persistenceService)) {
			if(location.isActive()){
				locations.add(location);
			}
		}
		persistenceService.getLogger().debug("Locations: " + locations.size(),this.getClass());
		return locations;
	}
	
	
	/**
	 * Liefert eine Liste aller Standorte
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Location> getLocations(DominoPersistenceService persistenceService) {
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();

		StringBuffer sb = new StringBuffer();
		sb.append("#### getLocations: \n");
		List<Location> locations = new ArrayList<Location>();
		try {
			View vw = dbConfig.getView("($locations)");
			Document doc = vw.getFirstDocument();
			while(doc != null){
				Document docNext = vw.getNextDocument(doc);
				Location location = getLocationFromDoc(persistenceService, doc);
				locations.add(location);
				sb.append("--- location: " + location.getName() + " (" + location.getPrimaryKey() + ")\n");
				doc.recycle();
				doc = docNext;
			}
		} catch (Exception e) {
			logger.error("Error in DominoPersistenceService.getLocations", e);
		} finally{
			logger.trace(sb.toString(),this.getClass());
		}

		return locations;
	}
	
	/**
	 * Speichert ein Location Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public void saveLocation(DominoPersistenceService persistenceService, Location location){
		Document doc;
		Logger logger = persistenceService.getLogger();
		try {
			Database dbConfig = persistenceService.getDbConfig();
			logger.info("saveLocation: " + location.getName() + " unid: " + location.getUnid()  + " db: " + dbConfig.getFilePath(),this.getClass());
			persistenceService.setVersionControlFields(location);
			if(location.getUnid() == null || location.getUnid().equals("")){
				logger.debug("saveLocation: create new location",this.getClass());
				doc = dbConfig.createDocument();
				location.setUnid(doc.getUniversalID());
				location.setPrimaryKey(doc.getUniversalID());
			}else{
				doc = dbConfig.getDocumentByUNID(location.getUnid());
				logger.debug("saveLocation: get existing location from database ",this.getClass());
			}
			if(doc == null){
				throw new PersistenceException("");
			}
			
			Location origLocation = getLocationFromDoc(persistenceService, doc);
			logger.debug("saveLocation: location values changed: " + (!origLocation.equals(location)),this.getClass());
			if(!origLocation.equals(location)){
				doc.replaceItemValue("docid", location.getPrimaryKey());
				doc.replaceItemValue("form", "Location");
				doc.replaceItemValue("Locationname", location.getName());
				doc.replaceItemValue("displayname", location.getDisplayname());
				doc.replaceItemValue("active", location.isActive()?"1":"0");	
				String versionControl = persistenceService.serializeObjectToString(location);				
				persistenceService.writeVersionControl(doc,versionControl);
				doc.save();
				doc.recycle();
				logger.debug("saveLocation: Location saved ",this.getClass());
			}
		} catch (Exception e) {
			logger.error("Error DominoLocationPersistenceService.saveLocation ", e);
		} 
		
	}
	
	/**
	 * Erzeugt ein Location Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 * @throws PersistenceException
	 */
	private Location getLocationFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException, PersistenceException{
		Location location = new Location();
		location.setUnid(doc.getUniversalID());
		location.setPrimaryKey(doc.getItemValueString("docid"));
		location.setActive(doc.getItemValueString("active").equals("1"));
		location.setName(doc.getItemValueString("locationname"));
		location.setDisplayname(doc.getItemValueString("displayname"));
		
		location.setListVersions(getHistory(persistenceService,doc));
		return location;
	}
	
	/**
	 * generiert eine Liste von Versionen aus der Historie des Dokuments
	 * Die liste wird aus Feldern des Dokuments gelesen die dort serialisiert 
	 * gespeichert sind und f�gt Sie der Liste in umgekehrter Reihenfolge zu
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 */
	public List<Location> getHistory(DominoPersistenceService persistenceService,Document doc){
		List<Location> listHistory = new ArrayList<Location>();
		List<String> entries = persistenceService.getObjectHistory(doc);
		Logger logger = persistenceService.getLogger();
		for(int i = entries.size()-1;i>=0;i--){
			try {
				Location location = (Location) persistenceService.deserializeObjectfromString(entries.get(i),Location.class);
				listHistory.add(location);
			} catch (Exception e) {
				logger.error("Error DominoPersistenceService.getHistory ", e);
			} 
		}
		return listHistory;
	}
	
}
