package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.List;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Notification;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.Name;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoNotificationController {

	public List<Notification> getNotifications(DominoPersistenceService persistenceService){
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getNotifications: ");
		List<Notification> notifications = new ArrayList<Notification>();
		try {
			View vw = dbConfig.getView("($notifications)");
			Document doc = vw.getFirstDocument();
			while(doc != null){
				Document docNext = vw.getNextDocument(doc);
				Notification notification = new Notification();
				notification.setPrimaryKey(doc.getItemValueString("docid"));
				Name creator = dbConfig.getParent().createName(doc.getItemValueString("creator"));
				notification.setCreator(creator.getCommon());
				DateTime datetime = (DateTime) doc.getItemValueDateTimeArray("date").elementAt(0);
				notification.setDate(datetime.toJavaDate());
				notification.setMessage(doc.getItemValueString("message"));
				notification.setNotRead(doc.getItemValueString("notRead").equals("1"));
				notification.setPersonalDocId(doc.getItemValueString("personDocID"));
				notifications.add(notification);
				sb.append("--- notification: " + notification.getPrimaryKey());
				doc.recycle();
				doc = docNext;
			}
		} catch (NotesException e) {
			logger.error("Error in DominoPersistenceService.getNotifications", e);
		} finally{
			logger.debug(sb.toString(),this.getClass());
		}
		return notifications;
	}

	
	public void saveNotificatio(DominoPersistenceService persistenceService, Notification notification) throws PersistenceException {
		Document doc;
		Logger logger = persistenceService.getLogger();
		try {
			Database dbConfig = persistenceService.getDbConfig();
			logger.info("DominoNotificationPersistenceService.saveNotificatio: " + notification.getCreator() + " unid: " + notification.getUnid()  + " db: " + dbConfig.getFilePath());
			persistenceService.setVersionControlFields(notification);
			if(notification.getUnid() == null || notification.getUnid().equals("")){
				logger.debug("DominoNotificationPersistenceService.saveNotificatio: create new notification");
				doc = dbConfig.createDocument();
				notification.setUnid(doc.getUniversalID());
				notification.setPrimaryKey(doc.getUniversalID());				
			}else{
				doc = dbConfig.getDocumentByUNID(notification.getUnid());
				logger.debug("DominoNotificationPersistenceService.saveNotificatio: get existing notification from database ");
			}
			if(doc == null){
				throw new PersistenceException("");
			}
			doc.replaceItemValue("docid", notification.getPrimaryKey());
			doc.replaceItemValue("form", "Device");
			doc.replaceItemValue("personDocID", notification.getPersonalDocId());
			doc.replaceItemValue("notRead", notification.isNotRead()?"1":"0");
			doc.replaceItemValue("message", notification.getMessage());
			doc.save();
			logger.debug("saveNotificatio: notification saved ",this.getClass());
		} catch (Exception e) {
			logger.error("Error DominoNotificationPersistenceService.saveNotificatio ", e);
		} finally{
			logger.debug("saveNotificatio notification saved  FINALLY",this.getClass());			
		}
		
	}
}
