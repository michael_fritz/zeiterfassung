package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Person;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoPersonController {

	/**
	 *  Liefert eine Liste aller aktiven Personen
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Person> getActivePersons(DominoPersistenceService persistenceService) {
		List<Person> persons = new ArrayList<Person>();
		for (Person person : getPersons(persistenceService)) {
			if(person.isActive()){
				persons.add(person);
			}
		}
		return persons;
	}
	
	
	/**
	 * Liefert eine Liste aller Personen
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Person> getPersons(DominoPersistenceService persistenceService) {
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();

		StringBuffer sb = new StringBuffer();
		sb.append("#### persons: ");
		List<Person> persons = new ArrayList<Person>();
		try {
			View vw = dbConfig.getView("($personByShortname)");
			Document doc = vw.getFirstDocument();
			while(doc != null){
				Document docNext = vw.getNextDocument(doc);
				Person person = getPersonFromDoc(persistenceService, doc);
				persons.add(person);
				sb.append("--- person: " + person.getLastname() + ", " + person.getFirstname());
				doc.recycle();
				doc = docNext;
			}
		} catch (NotesException e) {
			logger.error("Error in DominoPersistenceService.getPersons", e);
		} finally{
			logger.trace(sb.toString(),this.getClass());
		}

		return persons;
	}
	
	/**
	 * Speichert ein Person Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public void savePerson(DominoPersistenceService persistenceService, Person person) throws PersistenceException {
		Document doc;
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();
		logger.debug("start save Person: " + person.getFullname(),this.getClass());
		try {
			Person origPerson = null;
			if(person.getPrimaryKey() == null || person.getPrimaryKey().equals("")){
				doc = dbConfig.createDocument();
				person.setUnid(doc.getUniversalID());
				person.setPrimaryKey(doc.getUniversalID());
			}else{
				doc = persistenceService.getDocumentByKey(person.getPrimaryKey());
				origPerson = getPersonFromDoc(persistenceService, doc);
			}
			
			logger.debug("orig Person equals person: " + (origPerson != null?origPerson.equals(person):true),this.getClass());

			if(origPerson == null || !origPerson.equals(person)){
				doc.replaceItemValue("docid", person.getPrimaryKey());
				doc.replaceItemValue("form", "Person");
				doc.replaceItemValue("lastname", person.getLastname());
				doc.replaceItemValue("firstname", person.getFirstname());
				doc.replaceItemValue("personalNumber", person.getPersonalNumber());
				doc.replaceItemValue("passcode", person.getPasscode());
				DateTime dateTime = persistenceService.getSession().createDateTime(person.getEntryDate() == null?new Date():person.getEntryDate());					
				doc.replaceItemValue("entryDate", dateTime);
				if(person.getDateOfSeparation() != null){
					dateTime = persistenceService.getSession().createDateTime(person.getDateOfSeparation());					
					doc.replaceItemValue("DateOfSeparation", dateTime);					
				}else{
					doc.removeItem("DateOfSeparation");
				}
				doc.replaceItemValue("active", person.isActive()?"1":"0");
				doc.replaceItemValue("rfidTagsTime", person.getRfidTagsTime());
				doc.replaceItemValue("startOvertime", person.getStartOvertime());
				doc.replaceItemValue("holidayEntitlement", person.getHolidayEntitlement());
				doc.replaceItemValue("startHoliday", person.getStartHoliday());
				doc.replaceItemValue("superiorId", person.getSuperiorId());
				doc.replaceItemValue("locationDocId", person.getLocationDocId());
				doc.replaceItemValue("phoneBusiness", person.getPhoneBusiness());
				doc.replaceItemValue("EmailBusiness", person.getEmailBusiness());
				doc.save();
				doc.recycle();
				logger.debug("save Person: " + person.getFullname(),this.getClass());
			}
		} catch (NotesException e) {
			logger.error("", e);
			throw new PersistenceException("Error while saving peson: " + person.getFullname());
		}
		
	}
	
	/**
	 * Erzeugt ein Person Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 * @throws PersistenceException
	 */
	private Person getPersonFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException{
		Person person = new Person();
		person.setUnid(doc.getUniversalID());
		person.setPrimaryKey(doc.getItemValueString("docid"));
		person.setLastname(doc.getItemValueString("lastname"));
		person.setFirstname(doc.getItemValueString("firstname"));
		person.setLocationDocId(doc.getItemValueString("locationDocId"));
		person.setCompanyDocId(doc.getItemValueString("companyDocId"));
		person.setPersonalNumber(doc.getItemValueString("personalNumber"));
		person.setPasscode(doc.getItemValueString("passcode"));
		person.setStartOvertime(doc.getItemValueDouble("startOvertime"));
		person.setStartHoliday(doc.getItemValueDouble("startHoliday"));
		person.setHolidayEntitlement(doc.getItemValueInteger("holidayEntitlement"));
		person.setPhoneBusiness(doc.getItemValueString("phoneBusiness"));
		person.setEmailBusiness(doc.getItemValueString("EmailBusiness"));
		person.setSuperiorId(doc.getItemValueString("superiorId"));
		
		person.setActive(doc.getItemValueString("active").equals("1")?true:false);
		
		// Eintrittsdatum
		DateTime dateTime = (DateTime) doc.getItemValueDateTimeArray("entryDate").get(0);
		person.setEntryDate(dateTime.toJavaDate());
		
		// Austrittsdatum
		if(doc.hasItem("DateOfSeparation")){
			dateTime = (DateTime) doc.getItemValueDateTimeArray("DateOfSeparation").get(0);
			if(dateTime != null){
				person.setDateOfSeparation(dateTime.toJavaDate());
			}			
		}
		
		
		person.setRfidTagsTime(doc.getItemValue("rfidTagsTime"));
		if(person.getRfidTagsTime() == null){
			person.setRfidTagsTime(new Vector<String>());
		}
		return person;
	}
	
	/**
	 * generiert eine Liste von Versionen aus der Historie des Dokuments
	 * Die liste wird aus Feldern des Dokuments gelesen die dort serialisiert 
	 * gespeichert sind und f�gt Sie der Liste in umgekehrter Reihenfolge zu
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 */
	public List<Person> getHistory(DominoPersistenceService persistenceService,Document doc){
		List<Person> listHistory = new ArrayList<Person>();
		List<String> entries = persistenceService.getObjectHistory(doc);
		Logger logger = persistenceService.getLogger();
		for(int i = entries.size()-1;i>=0;i--){
			try {
				Person person = (Person) persistenceService.deserializeObjectfromString(entries.get(i),Person.class);
				listHistory.add(person);
			} catch (Exception e) {
				logger.error("Error DominoPersistenceService.getHistory ", e);
			} 
		}
		return listHistory;
	}
}
