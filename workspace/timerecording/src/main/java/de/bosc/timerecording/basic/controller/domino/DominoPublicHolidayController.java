package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.PublicHoliday;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.TimeentryService;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoPublicHolidayController {

	/**
	 *  Liefert eine Liste aller Feiertage in einem Jahr
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<PublicHoliday> getPublicHolidays(DominoPersistenceService persistenceService, YearString year) {
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();

		StringBuffer sb = new StringBuffer();
		sb.append("#### getPublicHolidays --- year: " + year + "\n");
		List<PublicHoliday> publicHolidays = new ArrayList<PublicHoliday>();
		try {
			View vw = dbConfig.getView("($publicholidays)");
			Vector<String> keys = new Vector<String>();
			keys.add(year.toString());
			DocumentCollection coll = vw.getAllDocumentsByKey(keys,true);
			Document doc = coll.getFirstDocument();
			while(doc != null){
				Document docNext = coll.getNextDocument(doc);
				PublicHoliday publicHoliday = getHolidayFromDoc(persistenceService,doc);
				publicHolidays.add(publicHoliday);
				sb.append("--- Public Holiday: " + publicHoliday.getDate() + " " + publicHoliday.getName() + " unid: " + publicHoliday.getUnid() + "\n");
				doc.recycle();
				doc = docNext;
			}
		} catch (NotesException e) {
			logger.error("Error in getPublicHolidays", e);
		} catch (PersistenceException e) {
			logger.error("Error in getPublicHolidays", e);
		} finally{
			logger.debug(sb.toString(),this.getClass());
		}
		return publicHolidays;
	}

	
	
	/**
	 * Speichert ein Holiday Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public void savePublicHoliday(DominoPersistenceService persistenceService, PublicHoliday publicHoliday) throws PersistenceException {
		Document doc;
		Logger logger = persistenceService.getLogger();
		try {
			Database dbConfig = persistenceService.getDbConfig();
			logger.trace("savePublicHoliday: " + publicHoliday.getName() + " unid: " + publicHoliday.getUnid()  + " db: " + dbConfig.getFilePath(),this.getClass());
			persistenceService.setVersionControlFields(publicHoliday);
			if(publicHoliday.getUnid() == null || publicHoliday.getUnid().equals("")){
				logger.debug("savePublicHoliday: create new Public Holiday",this.getClass());
				doc = dbConfig.createDocument();
				publicHoliday.setUnid(doc.getUniversalID());

			}else{
				doc = dbConfig.getDocumentByUNID(publicHoliday.getUnid());
				logger.debug("saveHoliday: get existing holiday from database ",this.getClass());
			}

			PublicHoliday origPublicHoliday = getHolidayFromDoc(persistenceService, doc);
			logger.debug("savePublicHoliday: Holiday values changed: " + (!origPublicHoliday.equals(publicHoliday)),this.getClass());
			if(!origPublicHoliday.equals(publicHoliday)){
				doc.replaceItemValue("form", "Holiday");
				doc.replaceItemValue("docid", doc.getUniversalID());
				doc.replaceItemValue("holidayName", publicHoliday.getName());
				doc.replaceItemValue("date", publicHoliday.getDate());
				doc.replaceItemValue("locationDocIDs", publicHoliday.getLocations());
				doc.replaceItemValue("companyDocIDs", publicHoliday.getCompanies());
				doc.replaceItemValue("halfDay", publicHoliday.isHalfDay()?"1":"0");
				doc.replaceItemValue("timeBonus", publicHoliday.getTimeBonus());

				String versionControl = persistenceService.serializeObjectToString(publicHoliday);
				persistenceService.writeVersionControl(doc,versionControl);
				doc.save();
				doc.recycle();
				View vw = dbConfig.getView("($publicholidays)");
				vw.refresh();
				logger.debug("savePublicHoliday: Holiday saved ",this.getClass());
			}
		} catch (Exception e) {
			logger.error("Error savePublicHoliday ", e);
		} 
		
	}
	
	/**
	 * Erzeugt ein Holiday Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 * @throws PersistenceException
	 */
	public PublicHoliday getHolidayFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException, PersistenceException{
		PublicHoliday publicHoliday = new PublicHoliday();
		publicHoliday.setUnid(doc.getUniversalID());
		publicHoliday.setName(doc.getItemValueString("holidayName"));	
		DateTime dateTime = (DateTime) doc.getItemValueDateTimeArray("date").get(0);
		publicHoliday.setDate(TimeentryService.dateFormat.format(dateTime.toJavaDate()));
		publicHoliday.setLocations(doc.getItemValue("locationDocIDs"));
		publicHoliday.setCompanies(doc.getItemValue("companyDocIDs"));
		publicHoliday.setTimeBonus(doc.getItemValueDouble("timeBonus"));
		publicHoliday.setHalfDay(doc.getItemValueString("halfDay").equals("1")?true:false);
		publicHoliday.setListVersions(getHistory(persistenceService,doc));
		return publicHoliday;
	}
	
	/**
	 * generiert eine Liste von Versionen aus der Historie des Dokuments
	 * Die liste wird aus Feldern des Dokuments gelesen die dort serialisiert 
	 * gespeichert sind und f�gt Sie der Liste in umgekehrter Reihenfolge zu
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 */
	public List<PublicHoliday> getHistory(DominoPersistenceService persistenceService,Document doc){
		List<PublicHoliday> listHistory = new ArrayList<PublicHoliday>();
		List<String> entries = persistenceService.getObjectHistory(doc);
		Logger logger = persistenceService.getLogger();
		for(int i = entries.size()-1;i>=0;i--){
			try {
				PublicHoliday holiday = (PublicHoliday) persistenceService.deserializeObjectfromString(entries.get(i),PublicHoliday.class);
				listHistory.add(holiday);
			} catch (Exception e) {
				logger.error("Error DominoPersistenceService.getHistory ", e);
			} 
		}
		return listHistory;
	}
}
