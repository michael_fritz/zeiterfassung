package de.bosc.timerecording.basic.controller.domino;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.model.TimeentryKind;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.model.YearString;
import de.bosc.timerecording.basic.service.TimeentryService;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoTimeentryController {


	/**
	 * @param persistenceService
	 * @param unid
	 * @param year
	 * @return
	 */
	public Timeentry getTimeentry(DominoPersistenceService persistenceService, String unid,YearString year){
		Timeentry timeentry = null;
		persistenceService.getLogger().debug("getTimeentry with unid: " + unid + " in year: " + year,this.getClass());
		try {
			Database dbData = persistenceService.getDbData(year);
			Document doc = dbData.getDocumentByUNID(unid);
			timeentry = getTimeentryFromDoc(doc,year);
			doc.recycle();
		} catch (Exception e) {
			persistenceService.getLogger().error("", e);
		}
		return timeentry;
	}
	
	/**
	 * Liefert eine Liste aller Zeiteintr�ge einer Person f�r einen Monat
	 * 
	 * @param persistenceService
	 * @return
	 */
	public Map<DateString,List<Timeentry>> getTimeentriesMonth(DominoPersistenceService persistenceService, String personalNumber, MonthString month) {
		Map<DateString,List<Timeentry>> timeEntriesMonth = new TreeMap<DateString, List<Timeentry>>();

		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getTimeentries: " + personalNumber + " - " + month + "\n");
		try {
			if (personalNumber != null) {
				YearString year = new YearString(month.getDate());
				Database dbData = persistenceService.getDbData(year);
				View vw = dbData.getView("($timeentriesMonth)");
				vw.refresh();
				Vector<String> keys = new Vector<String>();
				keys.add(month.toString());
				keys.add(personalNumber);
				sb.append("--- keys: " + keys + "\n");
				DocumentCollection coll = vw.getAllDocumentsByKey(keys, true);
				Document doc = coll.getFirstDocument();
				sb.append("--- Coll: " + coll.getCount() + "\n");
				while (doc != null) {
					Document docNext = coll.getNextDocument(doc);
					Timeentry timeentry = getTimeentryFromDoc(doc, year);
					for (String value : (Vector<String>) doc.getItemValue("dates")) {
						if (!value.equals("")) {
							List<Timeentry> timeentries = new ArrayList<Timeentry>();
							if (timeEntriesMonth.containsKey(new DateString(value))) {
								timeentries = timeEntriesMonth.get(new DateString(value));
							} else {
								timeEntriesMonth.put(new DateString(value), timeentries);
							}

							timeentries.add(timeentry);
							sb.append("--- timeentry: " + timeentry.getPrimaryKey() + "\n");
						}
					}
					doc.recycle();
					doc = docNext;
				}
				for (Map.Entry<DateString, List<Timeentry>> entry : timeEntriesMonth.entrySet()) {
					sb.append("Month: " + entry.getKey() + " = " + entry.getValue().size());
				} 
			}
		} catch (Exception e) {
			logger.error("Error in DominoPersistenceService.getTimeentries\n", e);
		} finally{
			logger.debug(sb.toString(),this.getClass());
		}	
		for(Map.Entry<DateString,List<Timeentry>> entry:timeEntriesMonth.entrySet()){
			Collections.sort(entry.getValue());
		}
		return timeEntriesMonth;
	}

	
	/**
	 * Liefert eine Liste aller Zeiteintr�ge einer Person an einem Tag
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Timeentry> getTimeentries(DominoPersistenceService persistenceService, String personalNumber, String date) {
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getTimeentries: " + personalNumber + " - " + date);
		List<Timeentry> timeentries = new ArrayList<Timeentry>();
		try {
			YearString year = new YearString(date);
			Database dbData = persistenceService.getDbData(year);
			View vw = dbData.getView("($timeentries)");
			Vector<String> keys = new Vector<String>();
			keys.add(date);			
			keys.add(personalNumber);
			DocumentCollection coll = vw.getAllDocumentsByKey(keys,true);
			Document doc = coll.getFirstDocument();
			while(doc != null){
				Document docNext = coll.getNextDocument(doc);
				Timeentry timeentry = getTimeentryFromDoc(doc,year);
				timeentries.add(timeentry);
				sb.append("--- timeentry: " + timeentry.getPrimaryKey());
				doc.recycle();
				doc = docNext;
			}				
		} catch (Exception e) {
			logger.error("Error in DominoPersistenceService.getTimeentries", e);
		} finally{
			logger.trace(sb.toString(),this.getClass());
		}
		return timeentries;
	}
	
	/**
	 * Liefert eine Liste aller Zeiteintr�ge einer Person f�r einen Monat
	 * 
	 * @param persistenceService
	 * @return
	 */
	public Map<DateString,List<Timeentry>> getTimeentriesMonthDeleted(DominoPersistenceService persistenceService, String personalNumber, MonthString month) {
		Map<DateString,List<Timeentry>> timeEntriesMonth = new TreeMap<DateString, List<Timeentry>>();
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getTimeentries deleted: " + personalNumber + " - " + month + "\n");
		try {
			YearString year = new YearString(month.getDate());
			Database dbData = persistenceService.getDbData(year);
			View vw = dbData.getView("($timeentriesMonthDeleted)");
			vw.refresh();
			Vector<String> keys = new Vector<String>();
			keys.add(month.toString());			
			keys.add(personalNumber);
			sb.append("--- keys: " + keys + "\n");
			DocumentCollection coll = vw.getAllDocumentsByKey(keys,true);
			Document doc = coll.getFirstDocument();
			sb.append("--- Coll: " + coll.getCount()+ "\n");
			while(doc != null){
				Document docNext = coll.getNextDocument(doc);
				Timeentry timeentry = getTimeentryFromDoc(doc,year);
				for(String value:(Vector<String>)doc.getItemValue("dates")){
					if(!value.equals("")){
						List<Timeentry> timeentries = new ArrayList<Timeentry>();
						if(timeEntriesMonth.containsKey(new DateString(value))){
							timeentries = timeEntriesMonth.get(new DateString(value));
						}else{
							timeEntriesMonth.put(new DateString(value),timeentries);
						}
						
						timeentries.add(timeentry);
						sb.append("--- timeentry: " + timeentry.getPrimaryKey() + "\n");						
					}
				}					
				doc.recycle();
				doc = docNext;
			}				
		} catch (Exception e) {
			logger.error("Error in DominoPersistenceService.getTimeentries deleted\n", e);
		} finally{
			logger.debug(sb.toString(),this.getClass());
		}	
		for(Map.Entry<DateString,List<Timeentry>> entry:timeEntriesMonth.entrySet()){
			Collections.sort(entry.getValue());
		}
		return timeEntriesMonth;
	}	
	
	/**
	 * Liefert eine Liste aller offenen Zeiteintr�ge einer Person
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Timeentry> getOpenTimeentries(DominoPersistenceService persistenceService) throws PersistenceException {
		Logger logger = persistenceService.getLogger();
		
		StringBuffer sb = new StringBuffer();
		sb.append("#### getOpenTimeentries: ");
		List<Timeentry> timeentries = new ArrayList<Timeentry>();
		try {
			Database dbData = persistenceService.getDbData();
			View vw = dbData.getView("($timeentriesOpen)");
			Document doc = vw.getFirstDocument();
			while(doc != null){
				Document docNext = vw.getNextDocument(doc);
				Timeentry timeentry = getTimeentryFromDoc(doc,new YearString(new Date()));
				timeentries.add(timeentry);
				sb.append("--- timeentry: " + timeentry.getPrimaryKey());
				doc.recycle();
				doc = docNext;
			}
		} catch (Exception e) {
			logger.error("Error in DominoPersistenceService.getTimeentries", e);
		} finally{
			logger.debug(sb.toString(),this.getClass());
		}
		return timeentries;
	}

	/**
	 * Speichert ein Person Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public void saveTimeentry(DominoPersistenceService persistenceService, Timeentry timeentry) throws PersistenceException {
		Logger logger = persistenceService.getLogger();
		logger.info("####### saveTimeentry: " + timeentry.getStartDate() + " - personal number:" + timeentry.getPersonalNumber(),this.getClass());
		Document doc;
		Document doc2 = null;
		try {
			YearString databaseYear;
			// In welcher Datenbank muss der Zeiteintrag gesoeichert sein !!
			if(timeentry.getYear() != null){
				databaseYear = timeentry.getYear();
			}else{
				databaseYear = new YearString(timeentry.getStartDate().getJavaDate());
			}
			
			logger.debug("------ database to search timeetry: " + databaseYear + " timeentryYear=" + timeentry.getYear(),this.getClass());
			if(timeentry.getUnid() == null || timeentry.getUnid().equals("")){
				doc = persistenceService.getDbData(databaseYear).createDocument();
				logger.debug("------ create new Timeentry in database: " + doc.getParentDatabase().getFilePath(),this.getClass());
				timeentry.setUnid(doc.getUniversalID());
				timeentry.setPrimaryKey(doc.getUniversalID());
				timeentry.setYear(databaseYear);
			}else{
				logger.debug("------ search Timeentry: " + timeentry.getUnid() + " in database: " + persistenceService.getDbData(databaseYear).getFilePath(),this.getClass());
				doc = persistenceService.getDbData(databaseYear).getDocumentByUNID(timeentry.getUnid());
				if(doc == null){
					doc = persistenceService.getDocumentByKey(persistenceService.getDbData(databaseYear),timeentry.getPrimaryKey());
				}
			}
			if(doc == null){
				throw new PersistenceException("");
			}
			Timeentry origTimeEntry = getTimeentryFromDoc(doc,timeentry.getYear());

			if(!origTimeEntry.equals(timeentry)){
				if(origTimeEntry.getEndTime() != -1 && !doc.isNewNote()){
					logger.debug("------ orig end time: " + origTimeEntry.getEndTime(),this.getClass());
					timeentry.setEdited("1");					
				}
				
				/* Sollte ein weitere EIntrag in einer anderen Datenbank vorhanden sein muss dieser gel�scht werden*/
				checkOtherDocument(persistenceService,timeentry);
				
				/* Sollte der Zeiteintrag in einer anderen Datenbank sein muss dieser gel�scht werden*/
				doc = checkTimeentryDatabase(persistenceService, timeentry, doc, databaseYear);
				
				/* Eintrag scheiben*/
				timeentry.setYear(new YearString(timeentry.getStartDate().getJavaDate()));
				writeTimeentryDoc(persistenceService,timeentry,doc);
				
				/* Rp�fung ob Zeitsplit vorgenommen werden muss */
				checkYearSplit(persistenceService, timeentry, doc);
				doc.recycle();
			}
		} catch (NotesException e) {
			logger.error("Error DominoPersistenceService.saveTimeentry ", e);
		} catch (Exception e) {
			logger.error("Error DominoPersistenceService.saveTimeentry ", e);
		}
	}

	
	/**
	 * Pr�ft ob ein weitere Eintrag in einer abweichenden Datenbank angelegt werdn muss
	 * Dabei muss ber�cksichtigt werden ob es sich um das Startdatun oder das Ende-Datum 
	 * handelt.
	 * 
	 * @param persistenceService
	 * @param timeentry
	 * @throws NotesException 
	 */
	private void checkYearSplit(DominoPersistenceService persistenceService, Timeentry timeentry,Document doc) throws Exception{
		/* Wenn sich Start- oder Endedatum vom Datenbank-Jahr unterscheidet muss gesplittet werden*/
		YearString endYear = new YearString(timeentry.getEndDate().getJavaDate());
		if(!endYear.equals(timeentry.getYear())){
			/* Neues Dokument anlegen */
			persistenceService.getLogger().debug("checkYearSplit: split timeentry: " + endYear);
			Document docOther = persistenceService.getDbData(endYear).createDocument();
			writeTimeentryDoc(persistenceService,timeentry,docOther);
			
			/* Jeweils gegenseitige docid und Jahr eintragen*/
			docOther.replaceItemValue("primaryKeyOther", doc.getUniversalID());
			docOther.replaceItemValue("yearOther", timeentry.getYear().getYear());
			docOther.save();

			doc.replaceItemValue("primaryKeyOther", docOther.getUniversalID());
			doc.replaceItemValue("yearOther", endYear.getYear());
			doc.save();
			docOther.recycle();
		}
	}
	
	/**
	 * L�scht ein evtl weiteres Document immer 
	 * Der Eintrag wird neu angelegt wenn sich der Datensatz weiterhin in einer 
	 * abweichenden Datenabnk befinden muss
	 * 
	 * @param persistenceService
	 * @param timeentry
	 */
	private void checkOtherDocument(DominoPersistenceService persistenceService, Timeentry timeentry){
		try{
			if(timeentry.getYearOther() != null && timeentry.getPrimaryKeyOther() != null){			
				Document doc2 = persistenceService.getDocumentByKey(persistenceService.getDbData(timeentry.getYearOther()),timeentry.getPrimaryKeyOther());
				if(doc2 != null){
					doc2.replaceItemValue("form", "");
					doc2.replaceItemValue("personalNumber", "");
					doc2.save();
					persistenceService.getLogger().debug("checkOtherDocument: delete other document: ");
				}
			}			
		}catch(Exception e){
			persistenceService.getLogger().error("", e);
		}
	}
	
	
	/**
	 * �berpr�ft ob das Dokument in der richtigen Daten-Datenbank ist
	 * Wenn nicht wird das urspr�ngliche Dokument 'gel�scht'
	 * 
	 * @param persistenceService
	 * @param doc
	 * @param databaseYear
	 * @param currentYear
	 * @return
	 * @throws NotesException
	 * @throws ParseException 
	 */
	private Document checkTimeentryDatabase(DominoPersistenceService persistenceService, Timeentry timeentry, Document doc, YearString databaseYear) throws Exception{
		YearString currentYear = new YearString(timeentry.getStartDate().getJavaDate());
		if(databaseYear != null && !databaseYear.equals(currentYear)){
			/* Document aus alter Datenbank 'l�schen'*/
			persistenceService.getLogger().debug("------ Timeentry aus alter Datenbank l�schen: " + databaseYear + " unid:"+ doc.getUniversalID(),this.getClass());
			doc.replaceItemValue("form", "");
			doc.replaceItemValue("personalNumber", "");
			doc.save();

			/* neues Doucment erzeugen*/
			doc = persistenceService.getDbData(currentYear).createDocument();
			timeentry.setUnid(doc.getUniversalID());
			timeentry.setPrimaryKey(doc.getUniversalID());
			persistenceService.getLogger().debug("------ neues Timeentry: " + timeentry.getUnid() + " in yaer: " + currentYear,this.getClass());
		}
		return doc;
	}
	
	/**
	 * 
	 * 
	 * @param persistenceService
	 * @param timeentry
	 * @param doc
	 * @throws Exception
	 */
	private void writeTimeentryDoc(DominoPersistenceService persistenceService, Timeentry timeentry, Document doc) throws Exception{
		doc.replaceItemValue("form", "Timeentry");
		doc.replaceItemValue("docid", doc.getUniversalID());
		doc.replaceItemValue("comment", timeentry.getComment());
		doc.replaceItemValue("dates",getDateTimeStringArray(timeentry.getStartDate(), timeentry.getEndDate()));
		doc.replaceItemValue("collision", timeentry.isCollision()?"1":"0");
		doc.replaceItemValue("deleted", timeentry.isDeleted()?"1":"0");
		doc.replaceItemValue("newEntry", timeentry.isNewEntry()?"1":"0");
		doc.replaceItemValue("deviceDocIdEnd", timeentry.getDeviceDocIdEnd());
		doc.replaceItemValue("deviceDocIdStart", timeentry.getDeviceDocIdStart());
		doc.replaceItemValue("edited", timeentry.getEdited());
		doc.replaceItemValue("editHistory", timeentry.getEditHistory());
		doc.replaceItemValue("endDate", timeentry.getEndDate()!=null?timeentry.getEndDate().toString():null);
		doc.replaceItemValue("endTime", timeentry.getEndTime());
		if(timeentry.getEndTime() != -1){					
			doc.replaceItemValue("endtimeText", TimeentryService.parseTimeToString(timeentry.getEndTime()));
		}
		doc.replaceItemValue("startDate", timeentry.getStartDate().toString());
		doc.replaceItemValue("startTime", timeentry.getStartTime());
		doc.replaceItemValue("starttimeText", TimeentryService.parseTimeToString(timeentry.getStartTime()));

		doc.replaceItemValue("personalNumber", timeentry.getPersonalNumber());
		doc.replaceItemValue("timeentryKindId", timeentry.getTimeentryKindId());
		doc.replaceItemValue("SystemClosed", timeentry.getSystemClosed());
		doc.replaceItemValue("TimeDuration", timeentry.getTimeDuration());
		TimeentryKind timeentryKind = persistenceService.getTimeentryKindService().getTimeentryKind(timeentry.getTimeentryKindId());
		if(timeentryKind != null){
			doc.replaceItemValue("timeentryKindName", timeentryKind.getName());
		}
		doc.replaceItemValue("updated", "1");

		Person person = persistenceService.getPersonService().getPersons().get(timeentry.getPersonalNumber());
		if(person != null){
			doc.replaceItemValue("fullname", person.getFullname());
		}
		WorktimeSetting worktimeSetting = persistenceService.getWorktimeSettingService().getWorktimeSettingForDate(person,TimeentryService.parseTimeEntryDate(timeentry.getStartDate().toString(), "00:00:00"));
		if(worktimeSetting != null){					
			doc.replaceItemValue("CompanyDocId", worktimeSetting.getCompanyDocId());
			doc.replaceItemValue("LocationDocId", worktimeSetting.getLocationDocId()==null?worktimeSetting.getPrimaryKey():worktimeSetting.getLocationDocId());
			doc.replaceItemValue("DepartmentDocId", worktimeSetting.getDepartmentDocId());
		}
		persistenceService.writeVersionControl(doc,persistenceService.serializeObjectToString(timeentry));
		persistenceService.getLogger().debug("save timeentry in database: " + doc.getParentDatabase().getFilePath(),this.getClass());
		doc.save();	
	}
	
	
	private Vector<String> getDateTimeStringArray(DateString start, DateString end) throws ParseException{
		Vector<String> v = new Vector<String>();
		v.add(start.toString());
		
		if(end != null && !end.equals("")){
			Date startDate = TimeentryService.dateFormat.parse(start.toString());
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDate);
			String tempDate = start.toString();
			while(!tempDate.equals(end.toString())){
				cal.add(Calendar.DATE, 1);
				tempDate = TimeentryService.dateFormat.format(cal.getTime());
				v.add(tempDate);
			}
		}
		
		
		return v;
	}
	
	
	/**
	 * Erzeugt ein Timeentry Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 */
	public Timeentry getTimeentryFromDoc(Document doc, YearString year) throws Exception{
		Timeentry timeentry = new Timeentry();
		timeentry.setYear(year);
		timeentry.setYearOther(doc.getItemValueString("yearOther").equals("")?null:new YearString(doc.getItemValueString("yearOther")));
		timeentry.setPrimaryKeyOther(doc.getItemValueString("primaryKeyOther").equals("")?null:doc.getItemValueString("primaryKeyOther"));
		timeentry.setComment(doc.getItemValueString("comment"));
		timeentry.setCompanyDocId(doc.getItemValueString("CompanyDocId"));
		timeentry.setDates(doc.getItemValue("dates"));
		timeentry.setDeleted(doc.getItemValueString("deleted").equals("1")?true:false);
		timeentry.setCollision(doc.getItemValueString("collision").equals("1")?true:false);
		timeentry.setDepartmentDocId(doc.getItemValueString("departmentDocId"));
		timeentry.setDeviceDocIdEnd(doc.getItemValueString("deviceDocIdEnd"));
		timeentry.setDeviceDocIdStart(doc.getItemValueString("deviceDocIdStart"));
		timeentry.setEdited(doc.getItemValueString("edited"));
		timeentry.setEditHistory(doc.getItemValueString("editHistory"));
		timeentry.setEndDate(new DateString(doc.getItemValueString("EndDate")));
		timeentry.setEndTime(doc.getItemValueInteger("endTime"));
		timeentry.setFullname(doc.getItemValueString("fullname"));
		timeentry.setLocationDocId(doc.getItemValueString("locationDocId"));
		timeentry.setNewEntry(doc.getItemValueString("newEntry").equals("1")?true:false);
		timeentry.setPersonalNumber(doc.getItemValueString("personalNumber"));
		timeentry.setPrimaryKey(doc.getItemValueString("docid"));
		timeentry.setStartDate(new DateString(doc.getItemValueString("StartDate")));
		timeentry.setStartTime(doc.getItemValueInteger("startTime"));
		timeentry.setSystemClosed(doc.getItemValueString("systemClosed"));
		timeentry.setTimeDuration(doc.getItemValueString("timeDuration"));
		timeentry.setTimeentryKindId(doc.getItemValueString("timeentryKindId"));
		timeentry.setUnid(doc.getUniversalID());
		return timeentry;
	}
	
	
	/**
	 * Liefert eine Liste aller von System geschlossener Zeiteintr�ge
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Timeentry> getTimeentriesSystemClosed(DominoPersistenceService persistenceService) {
		List<Timeentry> timeEntriesSystemClosed = new ArrayList<Timeentry>();
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getTimeentries system closed:\n");
		try {
			
			for(Map.Entry<String,Database> dbData:persistenceService.getDbDatas().entrySet()){
				sb.append("--- database: " + dbData.getValue().getFilePath()+ ":\n");
				View vw = dbData.getValue().getView("($timeentriesSystemClosed)");
				vw.refresh();
				YearString year = new YearString(dbData.getKey());
				Document doc = vw.getFirstDocument();
				sb.append("--- vw: " + vw.getEntryCount()+ "\n");
				while(doc != null){
					Document docNext = vw.getNextDocument(doc);
					Timeentry timeentry = getTimeentryFromDoc(doc,year);
					timeEntriesSystemClosed.add(timeentry);
					doc.recycle();
					doc = docNext;
				}				
				
			}
			Collections.sort(timeEntriesSystemClosed,Collections.reverseOrder());
		} catch (Exception e) {
			logger.error("Error in DominoPersistenceService.getTimeentriesSystemClosed\n", e);
		} finally{
			logger.debug(sb.toString(),this.getClass());
		}	
		return timeEntriesSystemClosed;
	}	
	
	/**
	 * Liefert eine Liste aller Zeiteintr�ge die kollidieren
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<Timeentry> getTimeentriesCollisions(DominoPersistenceService persistenceService) {
		List<Timeentry> timeEntriesCollision = new ArrayList<Timeentry>();
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getTimeentries collision:\n");
		try {
			
			for(Map.Entry<String,Database> dbData:persistenceService.getDbDatas().entrySet()){
				View vw = dbData.getValue().getView("($timeentriesCollision)");
				vw.refresh();
				YearString year = new YearString(dbData.getKey());
				Document doc = vw.getFirstDocument();
				sb.append("--- vw: " + vw.getEntryCount()+ "\n");
				while(doc != null){
					Document docNext = vw.getNextDocument(doc);
					Timeentry timeentry = getTimeentryFromDoc(doc,year);
					timeEntriesCollision.add(timeentry);
					doc.recycle();
					doc = docNext;
				}				
				
			}
			Collections.sort(timeEntriesCollision,Collections.reverseOrder());
		} catch (Exception e) {
			logger.error("Error in DominoPersistenceService.getTimeentriesCollision\n", e);
		} finally{
			logger.debug(sb.toString(),this.getClass());
		}	
		return timeEntriesCollision;
	}	
	
	
	/**
	 * generiert eine Liste von Versionen aus der Historie des Dokuments
	 * Die liste wird aus Feldern des Dokuments gelesen die dort serialisiert 
	 * gespeichert sind und f�gt Sie der Liste in umgekehrter Reihenfolge zu
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 */
	public List<Timeentry> getHistory(DominoPersistenceService persistenceService,Document doc){
		List<Timeentry> listHistory = new ArrayList<Timeentry>();
		List<String> entries = persistenceService.getObjectHistory(doc);
		Logger logger = persistenceService.getLogger();
		for(int i = entries.size()-1;i>=0;i--){
			try {
				Timeentry timeentry = (Timeentry) persistenceService.deserializeObjectfromString(entries.get(i),Timeentry.class);
				listHistory.add(timeentry);
			} catch (Exception e) {
				logger.error("Error DominoPersistenceService.getHistory ", e);
			} 
		}
		return listHistory;
	}
}
