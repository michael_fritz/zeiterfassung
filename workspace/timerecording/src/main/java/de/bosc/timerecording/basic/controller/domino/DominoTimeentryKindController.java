package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.List;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.TimeentryKind;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoTimeentryKindController {

	/**
	 * Liefert eine Liste aller Zeiteintragsarten
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<TimeentryKind> getTimeentryKinds(DominoPersistenceService persistenceService) {
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getTimeentryKinds: ");
		List<TimeentryKind> timeentryKinds = new ArrayList<TimeentryKind>();
		try {
			View vw = dbConfig.getView("($timeEntryKinds)");
			Document doc = vw.getFirstDocument();
			while(doc != null){
				Document docNext = vw.getNextDocument(doc);
				TimeentryKind timeentryKind = new TimeentryKind();
				timeentryKind.setPrimaryKey(doc.getItemValueString("docid"));
				timeentryKind.setSort(doc.getItemValueInteger("sort"));
				timeentryKind.setTimeEntryOptions(doc.getItemValue("timeentryoptions"));
				timeentryKind.setTimeEntryOptionsDefault(doc.getItemValueString("timeentryoptionsDefault"));
				timeentryKind.setTimeCalculation(doc.getItemValueString("timeCalculation"));
				timeentryKind.setName(doc.getItemValueString("TimeEntryKindname"));
				timeentryKind.setNotation(doc.getItemValueString("Notation"));
				timeentryKind.setSelectable(doc.getItemValueString("selectable"));
				timeentryKind.setDescription(doc.getItemValueString("Description"));
				timeentryKinds.add(timeentryKind);
				sb.append("--- timeentryKind: " + timeentryKind.getPrimaryKey());
				doc.recycle();
				doc = docNext;
			}
		} catch (NotesException e) {
			logger.error("Error in DominoPersistenceService.getTimeentryKinds", e);
		} finally{
			logger.trace(sb.toString(),this.getClass());
		}
		return timeentryKinds;
	}
	
	/**
	 * generiert eine Liste von Versionen aus der Historie des Dokuments
	 * Die liste wird aus Feldern des Dokuments gelesen die dort serialisiert 
	 * gespeichert sind und f�gt Sie der Liste in umgekehrter Reihenfolge zu
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 */
	public List<TimeentryKind> getHistory(DominoPersistenceService persistenceService,Document doc){
		List<TimeentryKind> listHistory = new ArrayList<TimeentryKind>();
		List<String> entries = persistenceService.getObjectHistory(doc);
		Logger logger = persistenceService.getLogger();
		for(int i = entries.size()-1;i>=0;i--){
			try {
				TimeentryKind timeentryKind = (TimeentryKind) persistenceService.deserializeObjectfromString(entries.get(i),TimeentryKind.class);
				listHistory.add(timeentryKind);
			} catch (Exception e) {
				logger.error("Error DominoPersistenceService.getHistory ", e);
			} 
		}
		return listHistory;
	}
}
