package de.bosc.timerecording.basic.controller.domino;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import de.bosc.timerecording.basic.business.DominoPersistenceService;
import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Location;
import de.bosc.timerecording.basic.model.Pausetime;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.TimeentryKind;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.service.LocationService;
import de.bosc.timerecording.basic.service.PersonService;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.Name;
import lotus.domino.NotesException;
import lotus.domino.View;

public class DominoWorktimeSettingController {

	/**
	 * Liefert eine Arbeitszeit Konfigurationen mit der unid
	 * 
	 * @param persistenceService
	 * @return
	 */	
	public WorktimeSetting getWorktimeSetting(DominoPersistenceService persistenceService,String unid){
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();
		WorktimeSetting worktimeSetting = null;
		StringBuffer sb = new StringBuffer();
		sb.append("#### getWorktimeSettings: " + unid);
		try {
			Document doc = dbConfig.getDocumentByUNID(unid);
			if(doc != null){
				worktimeSetting = getWorktimeSettingFromDoc(persistenceService, doc);		
				sb.append("\n--- worktime settings: " + worktimeSetting.getPrimaryKey());
				doc.recycle();
			}
		} catch (Exception e) {
			logger.error("Error in getWorktimeSettings: " + sb.toString() + "\n", e);
		}finally{
			logger.debug(sb.toString(),this.getClass());
		}
		return worktimeSetting;		
		
	}
	
	
	/**
	 * Liefert eine Liste aller Arbeitszeit Konfigurationen zu einer Person oder Firma
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<WorktimeSetting> getWorktimeSettings( DominoPersistenceService persistenceService, String primaryKey) {
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getWorktimeSettings: " + primaryKey);
		List<WorktimeSetting> worktimeSettings = new ArrayList<WorktimeSetting>();
		try {
			View vw = dbConfig.getView("($worktimeSettings)");
			DocumentCollection coll = vw.getAllDocumentsByKey(primaryKey,true);
			sb.append("\n ---- found: " + coll.getCount());
			Document doc = coll.getFirstDocument();
			while(doc != null){
				Document docNext = coll.getNextDocument(doc);
				WorktimeSetting worktimeSetting = getWorktimeSettingFromDoc(persistenceService, doc);		
				worktimeSettings.add(worktimeSetting);
				sb.append("\n--- worktime settings: " + worktimeSetting.getPrimaryKey());
				doc.recycle();
				doc = docNext;
			}
		} catch (Exception e) {
			logger.error("Error in getWorktimeSettings: " + sb.toString() + "\n", e);
		}finally{
			logger.debug(sb.toString(),this.getClass());
		}
		return worktimeSettings;
	}

	/**
	 * Liefert eine Liste aller Arbeitszeit Konfigurationen zu einer Person oder Firma
	 * 
	 * @param persistenceService
	 * @return
	 */
	public List<WorktimeSetting> getWorktimeSettings( DominoPersistenceService persistenceService) {
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		sb.append("#### getWorktimeSettings: ");
		List<WorktimeSetting> worktimeSettings = new ArrayList<WorktimeSetting>();
		try {
			View vw = dbConfig.getView("($worktimeSettings)");
			Document doc = vw.getFirstDocument();
			while(doc != null){
				Document docNext = vw.getNextDocument(doc);
				WorktimeSetting worktimeSetting = getWorktimeSettingFromDoc(persistenceService, doc);		
				worktimeSettings.add(worktimeSetting);
				sb.append("\n--- worktime settings: " + worktimeSetting.getPrimaryKey());
				doc.recycle();
				doc = docNext;
			}
		} catch (Exception e) {
			logger.error("Error in getWorktimeSettings: " + sb.toString() + "\n", e);
		}finally{
			logger.debug(sb.toString(),this.getClass());
		}
		return worktimeSettings;
	}
	
	/**
	 * Speichert ein WorktimeSetting Object im zugeh�rigen Notes Document wenn sich ein Feld ge�ndert hat.
	 * 
	 * @param persistenceService
	 * @param company
	 */
	public void saveWorktimeSetting(DominoPersistenceService persistenceService, WorktimeSetting worktimeSetting) {
		Database dbConfig = persistenceService.getDbConfig();
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer("saveWorktimeSetting\n");
		try{
			if(worktimeSetting != null){
				Document docWorktimeSetting = null;
				if(worktimeSetting.getUnid() != null){
					docWorktimeSetting = dbConfig.getDocumentByUNID(worktimeSetting.getUnid());
				}
				if(docWorktimeSetting == null){
					docWorktimeSetting = dbConfig.createDocument();
					docWorktimeSetting.replaceItemValue("form", "WorktimeSettings");
					docWorktimeSetting.replaceItemValue("parentDocId", worktimeSetting.getParentDocId());
					docWorktimeSetting.replaceItemValue("docId", docWorktimeSetting.getUniversalID());
					if(worktimeSetting.getParentForm().equals("Location")){
						Location location = new LocationService(persistenceService).getLocations().get(worktimeSetting.getParentDocId());
						if(location != null){
							docWorktimeSetting.replaceItemValue("name",location.getName());
						}
					}else{
						Collection<Person> persons = new PersonService(persistenceService).getPersons().values();
						for (Person person : persons) {
							if(person.getPrimaryKey().equals(worktimeSetting.getParentDocId())){
								docWorktimeSetting.replaceItemValue("name",person.getFullname());
								break;
							}
						}
					}
					worktimeSetting.setUnid(docWorktimeSetting.getUniversalID());
					worktimeSetting.setPrimaryKey(docWorktimeSetting.getUniversalID());
				}
				
				docWorktimeSetting.replaceItemValue("parentForm", worktimeSetting.getParentForm());
				
				
				DateTime dateTime = persistenceService.getSession().createDateTime(worktimeSetting.getValidAfterDate());
				docWorktimeSetting.replaceItemValue("validAfterDate", dateTime);
				
				docWorktimeSetting.removeItem("locationDocID");
				if(worktimeSetting.getLocationDocId() != null){
					docWorktimeSetting.replaceItemValue("locationDocID", worktimeSetting.getLocationDocId());					
				}

				docWorktimeSetting.removeItem("CoreTimeBegin");
				if(worktimeSetting.getCoreTimeBegin() != null){
					docWorktimeSetting.replaceItemValue("CoreTimeBegin", worktimeSetting.getCoreTimeBegin());
				}
				
				docWorktimeSetting.removeItem("CoreTimeEnd");
				if(worktimeSetting.getCoreTimeEnd() != null){
					docWorktimeSetting.replaceItemValue("CoreTimeEnd", worktimeSetting.getCoreTimeEnd());
				}

				docWorktimeSetting.removeItem("CoreTimeExtraAmount");
				if(worktimeSetting.getCoreTimeExtraAmount() != null){
					docWorktimeSetting.replaceItemValue("CoreTimeExtraAmount", worktimeSetting.getCoreTimeExtraAmount());
				}
				
				docWorktimeSetting.removeItem("InclusiveTime");
				if(worktimeSetting.getInclusiveTime() != null){
					docWorktimeSetting.replaceItemValue("InclusiveTime", worktimeSetting.getInclusiveTime());
				}
				
				docWorktimeSetting.removeItem("HourlyRate");
				if(worktimeSetting.getHourlyRate() != null){
					docWorktimeSetting.replaceItemValue("HourlyRate", worktimeSetting.getHourlyRate());
				}
								
				docWorktimeSetting.removeItem("PausetimeGeneral");
				if(worktimeSetting.getPausetimeGeneral() != null){
					docWorktimeSetting.replaceItemValue("PausetimeGeneral", worktimeSetting.getPausetimeGeneral());
				}
								
				
				
				for(int i = 1;i<8;i++){
					docWorktimeSetting.removeItem("DailyWorktimehours_" +i);
					if(worktimeSetting.getDailyWorktimes().get(i)!=null){
						docWorktimeSetting.replaceItemValue("DailyWorktimehours_" +i, worktimeSetting.getDailyWorktimes().get(i));
					}
				}

				for(int i = 1;i<8;i++){
					docWorktimeSetting.removeItem("DailyPercents_" +i);
					if(worktimeSetting.getDailyPercents().get(i)!=null){
						docWorktimeSetting.replaceItemValue("DailyPercents_" +i, worktimeSetting.getDailyPercents().get(i));
					}
				}

				//----------- deleteExistingPaustimes ------------------------------------------------
				for(int i = 0;i<11;i++){
					docWorktimeSetting.removeItem("pausetimeafter_" + i);
					docWorktimeSetting.removeItem("pausetime_" + i);
				}
				
				//----------- pausetimes ------------------------------------------------------------------
				sb.append("----- PAUSETIMES ----------------\n");
				for(int i = 0;i<3;i++){
					Pausetime pausetime = worktimeSetting.getPausetimes()[i];
					sb.append(" -- i:" + i + " --- Nach: " + pausetime.getAfter() + " eine Pasue von " + pausetime.getPause() + "\n");
					if(pausetime.getAfter() != null && pausetime.getPause() != null){
						docWorktimeSetting.replaceItemValue("pausetimeafter_" + i,pausetime.getAfter() );
						docWorktimeSetting.replaceItemValue("pausetime_" + i,pausetime.getPause());
					}
				}
				
				//---------- timeentrykinds ----------------------------------------------------------------
				sb.append("----- TIMEENTRYKINDS ----------------\n");
				Vector<String> timeentryKinds = new Vector<String>();
				for(TimeentryKind timeentryKind:worktimeSetting.getTimeentryKinds().values()){
					sb.append("--- kindentry: " + timeentryKind.getName() + " : " + timeentryKind.getSelectable() + " value: " + timeentryKind.getValue() + " percent:" + timeentryKind.getPercent() + "\n");
					if(timeentryKind.getValue() != null || !timeentryKind.getSelectable().equals("0")){
						timeentryKinds.add(timeentryKind.getPrimaryKey()+"#"  + timeentryKind.getSelectable() + "#" + (timeentryKind.getValue()!=null?timeentryKind.getValue():"") + "#" + timeentryKind.getPercent());
					}
				}
				docWorktimeSetting.replaceItemValue("timeentryKinds",timeentryKinds);
				//------------------------------------------------------------------------------------------
				
				logger.debug( sb.toString() + "worktime setting saved!",this.getClass());
				docWorktimeSetting.save();
			}
			
		}catch(Exception e){
			logger.error("",e);
		}
		
	}
	
	/**
	 * Erzeugt ein WorktimeSetting Object auf Basis eines Notes Document 
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 * @throws NotesException
	 */
	private WorktimeSetting getWorktimeSettingFromDoc(DominoPersistenceService persistenceService, Document doc) throws NotesException, PersistenceException{
		Logger logger = persistenceService.getLogger();
		StringBuffer sb = new StringBuffer();
		WorktimeSetting worktimeSetting = new WorktimeSetting();
		worktimeSetting.setPrimaryKey(doc.getItemValueString("docid"));
		worktimeSetting.setParentDocId(doc.getItemValueString("parentDocId"));
		worktimeSetting.setUnid(doc.getUniversalID());
		Name creator = persistenceService.getDbConfig().getParent().createName(doc.getItemValueString("creator"));
		worktimeSetting.setCreator(creator.getCommon());
		DateTime datetime = (DateTime) doc.getItemValueDateTimeArray("validAfterDate").elementAt(0);
		worktimeSetting.setValidAfterDate(datetime.toJavaDate());
		
		worktimeSetting.setParentForm(doc.getItemValueString("parentForm"));
		
		if(doc.hasItem("inclusiveTime")){
			worktimeSetting.setInclusiveTime(doc.getItemValueInteger("inclusiveTime"));
		}
		if(doc.hasItem("CoreTimeExtraAmount")){
			worktimeSetting.setCoreTimeExtraAmount(doc.getItemValueDouble("CoreTimeExtraAmount"));
		}
		if(doc.hasItem("coreTimeBegin")){
			worktimeSetting.setCoreTimeBegin(doc.getItemValueInteger("coreTimeBegin"));
		}		
		if(doc.hasItem("coreTimeEnd")){
			worktimeSetting.setCoreTimeEnd(doc.getItemValueInteger("coreTimeEnd"));
		}
		if(doc.hasItem("HourlyRate")){
			worktimeSetting.setHourlyRate(doc.getItemValueDouble("HourlyRate"));
		}
		if(doc.hasItem("pausetimeGeneral")){
			worktimeSetting.setPausetimeGeneral(doc.getItemValueInteger("pausetimeGeneral"));
		}
		if(doc.hasItem("locationDocId")){
			worktimeSetting.setLocationDocId(doc.getItemValueString("locationDocId"));		
		}
		
		sb.append("------------------- DAILLY WORKTIMES ----------------\n");
		//--- get daily worktime settings ---------------------------------------------------
		Map<Integer,Integer> dailyWorktimes = new TreeMap<Integer, Integer>();
		for(int i = 1;i<8;i++){
			if(doc.hasItem("DailyWorktimehours_" + i)){
				dailyWorktimes.put(i, doc.getItemValueInteger("DailyWorktimehours_" + i));
			}else{
				dailyWorktimes.put(i, null);
			}
			sb.append(i + ": " + dailyWorktimes.get(i) + "\n");
		}		
		worktimeSetting.setDailyWorktimes(dailyWorktimes);
		// -----------------------------------------------------------------------------------
		
		sb.append("------------------- DAILLY PERCENT ----------------\n");
		//--- get daily percent settings ---------------------------------------------------
		Map<Integer,Double> dailyPercents = new TreeMap<Integer, Double>();
		for(int i = 1;i<8;i++){
			if(doc.hasItem("DailyPercents_" + i)){
				dailyPercents.put(i, doc.getItemValueDouble("DailyPercents_" +i));				
			}else{
				dailyPercents.put(i, null);
			}
			sb.append(i + ": " + dailyPercents.get(i) + "\n");
		}		
		worktimeSetting.setDailyPercents(dailyPercents);
		// -----------------------------------------------------------------------------------

		//--- get pausetime settings ---------------------------------------------------
		for(int i = 0;i<3;i++){
			if(doc.hasItem("pausetimeafter_" + i) && doc.hasItem("pausetime_" + i)){
				Integer pausetimeafter = doc.getItemValueInteger("pausetimeafter_" + i);
				Integer pausetime = doc.getItemValueInteger("pausetime_" + i);
				if(pausetimeafter > 0 && pausetime > 0){
					Pausetime p = worktimeSetting.getPausetimes()[i];
					p.setAfter(pausetimeafter);
					p.setPause(pausetime);
				}
			}
		}
		
		Vector<String> vTimeenntryKinds = doc.getItemValue("timeentryKinds");
		Map<String,TimeentryKind> timeentryKinds = persistenceService.getTimeentryKindService().getCopyTimeentryKinds();
		timeentryKinds.remove("0");
		timeentryKinds.remove("1");
		timeentryKinds.remove("2");
		for(String entry:vTimeenntryKinds){
			sb.append("\nenttry: " + entry);
			if(!entry.equals("")){
				String[] arr = entry.split("#");
				sb.append("\n" + arr[0] + " - " + arr[1] + " --- percent: " + arr[3] + " --- " + (timeentryKinds.containsKey(arr[0])));
				if(timeentryKinds.containsKey(arr[0])){
					timeentryKinds.get(arr[0]).setSelectable(arr[1]);
					timeentryKinds.get(arr[0]).setValue(!arr[2].equals("")?Double.parseDouble(arr[2]):null);
					timeentryKinds.get(arr[0]).setPercent(arr[3].equals("")?"0":arr[3]);
				}
			}
		}
		logger.trace(sb.toString(),this.getClass());
		worktimeSetting.setTimeentryKinds(timeentryKinds);
		return worktimeSetting;
	}

	
	/**
	 * generiert eine Liste von Versionen aus der Historie des Dokuments
	 * Die liste wird aus Feldern des Dokuments gelesen die dort serialisiert 
	 * gespeichert sind und f�gt Sie der Liste in umgekehrter Reihenfolge zu
	 * 
	 * @param persistenceService
	 * @param doc
	 * @return
	 */	
	public List<WorktimeSetting> getHistory(DominoPersistenceService persistenceService,Document doc){
		List<WorktimeSetting> listHistory = new ArrayList<WorktimeSetting>();
		List<String> entries = persistenceService.getObjectHistory(doc);
		Logger logger = persistenceService.getLogger();
		for(int i = entries.size()-1;i>=0;i--){
			try {
				WorktimeSetting worktimeSetting = (WorktimeSetting) persistenceService.deserializeObjectfromString(entries.get(i),WorktimeSetting.class);
				listHistory.add(worktimeSetting);
			} catch (Exception e) {
				logger.error("Error DominoPersistenceService.getHistory ", e);
			} 
		}
		return listHistory;
	}
}
