package de.bosc.timerecording.basic.entity;

import java.io.Serializable;
import java.util.HashMap;

import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.VersionControl;
import de.bosc.timerecording.basic.model.YearString;

public class HolidayEntitlementEntity extends VersionControl implements Serializable {
	private static final long serialVersionUID = 1L;

	private String unid;
	private String primaryKey;
	private String personalNumber;
	private String fullname;
	private String comment;
	private YearString year;
	private Integer amountDays;
	private Integer effectiveDays;
	
	public HolidayEntitlementEntity() {
	}
	
	public HolidayEntitlementEntity(Person person, YearString year){
		this.personalNumber = person.getPersonalNumber();
		this.year = year;
		this.fullname = person.getFullname();
		this.amountDays = person.getHolidayEntitlement();
		this.effectiveDays = person.getHolidayEntitlement();
	}
	
	@Override
	public HashMap<String, String> compareVersion(VersionControl o) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Integer getEffectiveDays() {
		return effectiveDays;
	}
	public void setEffectiveDays(Integer effectiveDays) {
		this.effectiveDays = effectiveDays;
	}
	public String getUnid() {
		return unid;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public Integer getAmountDays() {
		return amountDays;
	}

	public String getComment() {
		return comment;
	}

	public String getFullname() {
		return fullname;
	}

	public String getPersonalNumber() {
		return personalNumber;
	}

	public YearString getYear() {
		return year;
	}

	public void setAmountDays(Integer amountDays) {
		this.amountDays = amountDays;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}

	public void setYear(YearString year) {
		this.year = year;
	}	
}
