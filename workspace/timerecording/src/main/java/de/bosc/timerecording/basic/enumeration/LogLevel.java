package de.bosc.timerecording.basic.enumeration;

public enum LogLevel {
	ERROR,WARN,INFO,DEBUG,TRACE;
	
	public static LogLevel getLogLevel(String strLogLevel){
		LogLevel logLevel = INFO;
		
		if(strLogLevel == null){
			logLevel = INFO;
		}else if(strLogLevel.equals("1")){
			logLevel = ERROR;
		}else if(strLogLevel.equals("2")){
			logLevel = WARN;
		}else if(strLogLevel.equals("3")){
			logLevel = INFO;
		}else if(strLogLevel.equals("4")){
			logLevel = DEBUG;
		}else if(strLogLevel.equals("5")){
			logLevel = TRACE;
		}
		
		
		return logLevel;
	}
}
