package de.bosc.timerecording.basic.interfaces;

import java.util.HashMap;

import de.bosc.timerecording.basic.model.VersionControl;

public interface VersionCompare {
	public HashMap<String,String> compareVersion(VersionControl o);

}
