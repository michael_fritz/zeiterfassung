package de.bosc.timerecording.basic.logging;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Map;

import de.bosc.timerecording.basic.enumeration.LogLevel;

@SuppressWarnings("rawtypes")
public abstract class Logger implements Serializable {

	private static final long serialVersionUID = 1L;
	protected Map<String,LogLevel> logLevelsClass;
	protected LogLevel logLevel = LogLevel.INFO;
	
	public abstract void error(String msg,Exception e);
	public abstract void warn(String msg);
	public abstract void info(String msg);
	public abstract void info(String msg,Class clazz);
	public abstract void debug(String msg);
	public abstract void debug(String msg,Class clazz);
	public abstract void trace(String msg);
	public abstract void trace(String msg,Class clazz);
	
	public static String getStackTrace(Throwable t) {
		StringWriter sw = new StringWriter();
	    t.printStackTrace(new PrintWriter(sw));
	    return sw.toString();
	}
	
	public LogLevel getLogLevelForClass(Class clazz){
		return logLevelsClass != null && logLevelsClass.containsKey(clazz.getCanonicalName())?logLevelsClass.get(clazz.getCanonicalName()):logLevel;
	}
}
