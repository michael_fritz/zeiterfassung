package de.bosc.timerecording.basic.logging;

@SuppressWarnings("rawtypes")
public class LoggerDefault extends de.bosc.timerecording.basic.logging.Logger {

	private static final long serialVersionUID = 1L;

	
	@Override
	public void error(String msg, Exception e) {
		System.out.println(msg + "\n" + Logger.getStackTrace(e));
	}

	@Override
	public void warn(String msg) {
		System.out.println(msg);
	}

	@Override
	public void info(String msg) {
		System.out.println(msg);
	}
	
	@Override
	public void info(String msg,Class clazz) {
		System.out.println(clazz.getCanonicalName() + ": " + msg);
	}
	
	@Override
	public void debug(String msg) {
		System.out.println(msg);
	}

	@Override
	public void debug(String msg,Class clazz) {
		System.out.println(clazz.getCanonicalName() + ": " + msg);
	}
	
	@Override
	public void trace(String msg) {
		System.out.println(msg);
	}

	@Override
	public void trace(String msg,Class clazz) {
		System.out.println(clazz.getCanonicalName() + ": " + msg);
	}

}
