package de.bosc.timerecording.basic.logging;

import java.util.HashMap;
import java.util.Map;

import de.bosc.timerecording.basic.enumeration.LogLevel;
import lotus.domino.Log;
import lotus.domino.NotesException;
import lotus.domino.Session;

@SuppressWarnings("rawtypes")
public class LoggerDomino extends Logger {

	private LoggerDefault defaultLog = new LoggerDefault();
	private Log agentLog = null;
	
	public LoggerDomino(Session session, String server, String logname, String logPath, LogLevel logLevel,Map<String,LogLevel> logLevelsClass) {
		try {
			if(logLevel != null){
				this.logLevel = logLevel;
			}

			if(logLevelsClass == null){
				logLevelsClass = new HashMap<String, LogLevel>();
			}
			this.logLevelsClass = logLevelsClass;
			agentLog = session.createLog(logname);
			agentLog.openNotesLog(server,logPath);
		} catch (NotesException e) {
			defaultLog.error("",e);
		}	
	}
	
	public void logAction(String msg){
		try {
			agentLog.logAction(msg);
		} catch (NotesException e) {
			defaultLog.error("", e);
		}		
	}
	
	public void logAction(String msg,Class clazz){
		try {
			String progname = agentLog.getProgramName();
			if(clazz != null){
				agentLog.setProgramName(clazz.getSimpleName());
			}
			agentLog.logAction(msg);
			agentLog.setProgramName(progname);
		} catch (NotesException e) {
			defaultLog.error("", e);
		}		
	}
	
	
	@Override
	public void error(String msg, Exception e) {
		try {
			agentLog.logError(9999, msg + "\n" + getStackTrace(e));
		} catch (NotesException e1) {
			defaultLog.error("", e1);
		}
	}

	@Override
	public void warn(String msg) {
		if(logLevel.ordinal() >= LogLevel.WARN.ordinal()){
			logAction(msg);
		}
	}

	@Override
	public void info(String msg) {
		if(logLevel.ordinal() >= LogLevel.INFO.ordinal()){
			logAction("INFO: " + msg,null);
		}
	}
	
	public void info(String msg,Class clazz) {
		if(logLevelsClass.containsKey(clazz.getCanonicalName())){
			if(logLevelsClass.get(clazz.getCanonicalName()).ordinal() >= LogLevel.INFO.ordinal()){
				logAction("INFO: " + clazz.getSimpleName() + ": " + msg,clazz);
			}
		}else if(logLevel.ordinal() >= LogLevel.INFO.ordinal()){
			logAction("INFO: " + msg,clazz);
		}
	}

	@Override
	public void debug(String msg) {
		if(logLevel.ordinal() >= LogLevel.DEBUG.ordinal()){
			logAction("DEBUG: " + msg,null);
		}
	}

	public void debug(String msg,Class clazz) {
		if(logLevelsClass.containsKey(clazz.getCanonicalName())){
			if(logLevelsClass.get(clazz.getCanonicalName()).ordinal() >= LogLevel.DEBUG.ordinal()){
				logAction("DEBUG: " + clazz.getSimpleName() + ": " + msg,clazz);
			}
		}else if(logLevel.ordinal() >= LogLevel.DEBUG.ordinal()){
			logAction("DEBUG: " + msg,clazz);
		}
	}
	
	@Override
	public void trace(String msg) {
		if(logLevel.ordinal() >= LogLevel.TRACE.ordinal()){
			logAction("TRACE: " + msg,null);
		}
	}
	
	public void trace(String msg,Class clazz) {
		if(logLevelsClass.containsKey(clazz.getCanonicalName())){
			if(logLevelsClass.get(clazz.getCanonicalName()).ordinal() >= LogLevel.TRACE.ordinal()){
				logAction("TRACE: " + clazz.getSimpleName() + ": " + msg,clazz);
			}
		}else if(logLevel.ordinal() >= LogLevel.TRACE.ordinal()){
			logAction("TRACE: " + msg,clazz);
		}
	}
	

}
