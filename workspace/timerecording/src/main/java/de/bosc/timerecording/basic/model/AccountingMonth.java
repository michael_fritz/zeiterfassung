package de.bosc.timerecording.basic.model;

import java.io.Serializable;

public class AccountingMonth implements Serializable{


	private static final long serialVersionUID = 1L;
	private String unid;
	private String personalNumber;
	private MonthString month;
	private Double daysSickness = 0.0;
	private Double daysPublicHoliday = 0.0;
	private Double daysHoliday = 0.0;
	private Integer inclusiveTime = 0;
	private Integer overtime = 0;
	private Integer soll = 0;
	private Integer ist = 0;
	
	public AccountingMonth() {
		
	}
	public AccountingMonth(String personalNumber, MonthString month) {
		super();
		this.personalNumber = personalNumber;
		this.month = month;
	}
	public String getPersonalNumber() {
		return personalNumber;
	}
	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}
	public MonthString getMonth() {
		return month;
	}
	public void setMonth(MonthString month) {
		this.month = month;
	}
	public Double getDaysSickness() {
		return daysSickness;
	}
	public void setDaysSickness(Double daysSickness) {
		this.daysSickness = daysSickness;
	}
	public Double getDaysPublicHoliday() {
		return daysPublicHoliday;
	}
	public void setDaysPublicHoliday(Double daysPublicHoliday) {
		this.daysPublicHoliday = daysPublicHoliday;
	}
	public Double getDaysHoliday() {
		return daysHoliday;
	}
	public void setDaysHoliday(Double daysHoliday) {
		this.daysHoliday = daysHoliday;
	}
	public Integer getOvertime() {
		return overtime;
	}
	public void setOvertime(Integer overtime) {
		this.overtime = overtime;
	}
	public Integer getSoll() {
		return soll;
	}
	public void setSoll(Integer soll) {
		this.soll = soll;
	}
	public Integer getIst() {
		return ist;
	}
	public void setIst(Integer ist) {
		this.ist = ist;
	}
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}

	public Integer getInclusiveTime() {
		return inclusiveTime;
	}
	public void setInclusiveTime(Integer inclusiveTime) {
		this.inclusiveTime = inclusiveTime;
	}
	public boolean equals(AccountingMonth accountingMonth) {
		if(!accountingMonth.getMonth().equals(this.getMonth())){
			return false;
		}else if(!this.getDaysHoliday().equals(accountingMonth.getDaysHoliday())){
			return false;
		}else if(!this.getDaysPublicHoliday().equals(accountingMonth.getDaysPublicHoliday())){
			return false;
		}else if(!this.getDaysSickness().equals(accountingMonth.getDaysSickness())){
			return false;
		}else if(!this.getIst().equals(accountingMonth.getIst())){
			return false;
		}else if(!this.getSoll().equals(accountingMonth.getSoll())){
			return false;
		}else if(!this.getOvertime().equals(accountingMonth.getOvertime())){
			return false;
		}else if(!this.getInclusiveTime().equals(accountingMonth.getInclusiveTime())){
			return false;
		}else if(!accountingMonth.getPersonalNumber().equals(this.getPersonalNumber())){
			return false;
		}
		return true;
	}
}
