package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.Date;

public class AccountingPerson implements Serializable{


	private static final long serialVersionUID = 1L;
	private String unid;
	private String personalNumber;
	private Double daysSickness = 0.0;
	private Double daysHoliday = 0.0;
	private Double startOvertime = 0.0;
	private Double startHoliday = 0.0;
	private Integer overtime = 0;
	private Date lastUpdate;
	
	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Double getStartOvertime() {
		return startOvertime;
	}
	public void setStartOvertime(Double startOvertime) {
		this.startOvertime = startOvertime;
	}
	public Double getStartHoliday() {
		return startHoliday;
	}
	public void setStartHoliday(Double startHoliday) {
		this.startHoliday = startHoliday;
	}
	public String getPersonalNumber() {
		return personalNumber;
	}
	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}
	public Double getDaysSickness() {
		return daysSickness;
	}
	public void setDaysSickness(Double daysSickness) {
		this.daysSickness = daysSickness;
	}
	public Double getDaysHoliday() {
		return daysHoliday;
	}
	public void setDaysHoliday(Double daysHoliday) {
		this.daysHoliday = daysHoliday;
	}
	public Integer getOvertime() {
		return overtime;
	}
	public void setOvertime(Integer overtime) {
		this.overtime = overtime;
	}
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}

	public boolean equals(AccountingPerson accountingPerson) {
		if(!this.getDaysHoliday().equals(accountingPerson.getDaysHoliday())){
			return false;
		}else if(!this.getDaysSickness().equals(accountingPerson.getDaysSickness())){
			return false;
		}else if(!this.getOvertime().equals(accountingPerson.getOvertime())){
			return false;
		}else if(!accountingPerson.getPersonalNumber().equals(this.getPersonalNumber())){
			return false;
		}
		return true;
	}
}
