package de.bosc.timerecording.basic.model;

import java.io.Serializable;

public class AccountingYear implements Serializable{


		private static final long serialVersionUID = 1L;
		private String unid;
		private String personalNumber;
		private YearString year;
		private Double daysSickness = 0.0;
		private Double daysPublicHoliday = 0.0;
		private Double daysHoliday = 0.0;
		private Integer daysGivenHoliday = 0;
		private Integer overtime = 0;
		
		
		public Integer getDaysGivenHoliday() {
			return daysGivenHoliday;
		}
		public void setDaysGivenHoliday(Integer daysGivenHoliday) {
			this.daysGivenHoliday = daysGivenHoliday;
		}
		public String getPersonalNumber() {
			return personalNumber;
		}
		public void setPersonalNumber(String personalNumber) {
			this.personalNumber = personalNumber;
		}
		public YearString getYear() {
			return year;
		}
		public void setYear(YearString year) {
			this.year = year;
		}
		public Double getDaysSickness() {
			return daysSickness;
		}
		public void setDaysSickness(Double daysSickness) {
			this.daysSickness = daysSickness;
		}
		public Double getDaysPublicHoliday() {
			return daysPublicHoliday;
		}
		public void setDaysPublicHoliday(Double daysPublicHoliday) {
			this.daysPublicHoliday = daysPublicHoliday;
		}
		public Double getDaysHoliday() {
			return daysHoliday;
		}
		public void setDaysHoliday(Double daysHoliday) {
			this.daysHoliday = daysHoliday;
		}
		public Integer getOvertime() {
			return overtime;
		}
		public void setOvertime(Integer overtime) {
			this.overtime = overtime;
		}
		public String getUnid() {
			return unid;
		}
		public void setUnid(String unid) {
			this.unid = unid;
		}

		public boolean equals(AccountingYear accountingYear) {
			if(!accountingYear.getYear().equals(this.getYear())){
				return false;
			}else if(!this.getDaysHoliday().equals(accountingYear.getDaysHoliday())){
				return false;
			}else if(!this.getDaysPublicHoliday().equals(accountingYear.getDaysPublicHoliday())){
				return false;
			}else if(!this.getDaysSickness().equals(accountingYear.getDaysSickness())){
				return false;
			}else if(!this.getOvertime().equals(accountingYear.getOvertime())){
				return false;
			}else if(!accountingYear.getPersonalNumber().equals(this.getPersonalNumber())){
				return false;
			}
			return true;
		}
}
