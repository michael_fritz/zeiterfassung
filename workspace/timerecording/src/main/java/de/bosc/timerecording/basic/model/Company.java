package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import de.bosc.timerecording.basic.interfaces.VersionCompare;

public class Company extends VersionControl implements Serializable,VersionCompare {

	private static final long serialVersionUID = 1L;
	
	private String unid;
	private String primaryKey;
	private String name;
	private String locationDocid;
	private boolean active;
	
	private transient Location location;
	private transient List<Company> listVersions;	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocationDocid() {
		return locationDocid;
	}
	public void setLocationDocid(String locationDocid) {
		this.locationDocid = locationDocid;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public List<Company> getListVersions() {
		return listVersions;
	}
	public void setListVersions(List<Company> listVersions) {
		this.listVersions = listVersions;
	}
	public HashMap<String, String> compareVersion(VersionControl o) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
