package de.bosc.timerecording.basic.model;

import java.io.Serializable;

public class CompensatoryTime implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String unid;
	private String primaryKey;
	private DateString date;
	private String fullname;
	private String comment;
	private String personalNumber;
	private Integer hours;
	
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public DateString getDate() {
		return date;
	}
	public void setDate(DateString date) {
		this.date = date;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getPersonalNumber() {
		return personalNumber;
	}
	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}
	public Integer getHours() {
		return hours;
	}
	public void setHours(Integer hours) {
		this.hours = hours;
	}

}
