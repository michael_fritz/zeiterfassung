package de.bosc.timerecording.basic.model;

import java.util.Date;
import java.util.Map;

import de.bosc.timerecording.basic.enumeration.LogLevel;

public class DatabaseConfiguration {

	private String pathDbData;
	private String pathDbLog;
	private Integer cutOffTime = 0;
	private Integer startYear = 2018;
	private LogLevel logLevel;
	private Map<String,LogLevel> logLevelsClass;
	
	public Integer getStartYear() {
		return startYear;
	}
	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}
	public LogLevel getLogLevel() {
		return logLevel;
	}
	public void setLogLevel(LogLevel logLevel) {
		this.logLevel = logLevel;
	}
	public Map<String, LogLevel> getLogLevelsClass() {
		return logLevelsClass;
	}
	public void setLogLevelsClass(Map<String, LogLevel> logLevelsClass) {
		this.logLevelsClass = logLevelsClass;
	}
	public Integer getCutOffTime() {
		return cutOffTime;
	}
	public void setCutOffTime(Integer cutOffTime) {
		this.cutOffTime = cutOffTime;
	}
	public String getPathDbData() {
		return pathDbData;
	}
	public void setPathDbData(String pathDbData) {
		this.pathDbData = pathDbData;
	}
	public String getPathDbLog() {
		return pathDbLog;
	}
	public void setPathDbLog(String pathDbLog) {
		this.pathDbLog = pathDbLog;
	}
	
}
