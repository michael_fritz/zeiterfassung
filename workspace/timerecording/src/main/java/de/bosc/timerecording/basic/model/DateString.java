package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

import de.bosc.timerecording.basic.interfaces.ITimeString;
import de.bosc.timerecording.basic.service.TimeentryService;

public class DateString implements Serializable,ITimeString, Comparable<DateString>{

	private static final long serialVersionUID = 1L;
	private String date;
	
	public DateString(String date) {
		this.date = date;
	}

	public DateString(Date date) {
		this.date = TimeentryService.dateFormat.format(date);
	}

	public String getDate() {
		return date;
	}

	public Date getJavaDate() throws ParseException {
		return TimeentryService.dateFormat.parse(date);
	}
	
	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return date;
	}

	public boolean equals(String strDate){
		return date.equals(strDate);
	}
	public boolean equals(DateString dateString){
		return date.equals(dateString.getDate());
	}
	public int compareTo(DateString o) {
		return this.toString().compareTo(o.toString());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateString other = (DateString) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}

	

}
