package de.bosc.timerecording.basic.model;

import java.io.Serializable;

public class DayTimeentry implements Serializable, Comparable<DayTimeentry> {

	private static final long serialVersionUID = 1L;
	private Integer durationTotal = 0;
	private Integer durationOtherDay = 0;
	private Integer durationDay = 0;
	private Integer durationShow = 0;
	private Integer breaktime = 0;
	private Integer startTime = 0;
	private Integer endTime = 0;
	private Integer additionKind;
	private String timeDuration;
	private TimeentryKind timeentryKind;
	private boolean calculateBreaktime = false;

	public TimeentryKind getTimeentryKind() {
		return timeentryKind;
	}

	public Integer getAdditionKind() {
		return additionKind;
	}

	public void setAdditionKind(Integer additionKind) {
		this.additionKind = additionKind;
	}

	public void setTimeentryKind(TimeentryKind timeentryKind) {
		this.timeentryKind = timeentryKind;
	}

	public String getTimeDuration() {
		return timeDuration;
	}

	public void setTimeDuration(String timeDuration) {
		this.timeDuration = timeDuration;
	}

	public Integer getDurationOtherDay() {
		return durationOtherDay;
	}

	public void setDurationOtherDay(Integer durationOtherDay) {
		this.durationOtherDay = durationOtherDay;
	}

	public Integer getDurationTotal() {
		return durationTotal;
	}

	public void setDurationTotal(Integer durationTotal) {
		this.durationTotal = durationTotal;
	}

	public Integer getDurationDay() {
		return durationDay;
	}

	public void setDurationDay(Integer durationDay) {
		this.durationDay = durationDay;
	}

	public Integer getBreaktime() {
		return breaktime;
	}

	public void setBreaktime(Integer breaktime) {
		this.breaktime = breaktime;
	}

	public Integer getStartTime() {
		return startTime;
	}

	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}

	public Integer getEndTime() {
		return endTime;
	}

	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}

	public boolean isCalculateBreaktime() {
		return calculateBreaktime;
	}

	public void setCalculateBreaktime(boolean calculateBreaktime) {
		this.calculateBreaktime = calculateBreaktime;
	}

	public boolean isAdditional() {
		boolean result = false;
		if( timeentryKind.getTimeCalculation().equals("3")){
			result = true;
		}else if( timeentryKind.getTimeCalculation().equals("4") && timeDuration.equals("3")){
			result = true;
		}
		return result;
	}

	public boolean isComplementary() {
		return timeentryKind.getTimeCalculation().equals("4") && !timeDuration.equals("3");
	}

	public boolean isHoliday() {
		return getTimeentryKind().getPrimaryKey().equals("1");
	}

	public boolean isSickness() {
		return getTimeentryKind().getPrimaryKey().equals("2");
	}

	public Integer getDurationShow() {
		return durationShow;
	}

	public void setDurationShow(Integer durationShow) {
		this.durationShow = durationShow;
	}

	public int compareTo(DayTimeentry dayTimeentry) {
		return startTime.compareTo(dayTimeentry.getStartTime());
	}
}
