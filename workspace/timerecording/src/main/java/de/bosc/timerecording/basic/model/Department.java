package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.HashMap;

import de.bosc.timerecording.basic.interfaces.VersionCompare;

public class Department extends VersionControl implements Serializable,VersionCompare {
	
	private static final long serialVersionUID = 1L;

	private String docId;
	private String name;
	private String alias;
	private Person orgUnitManager;
	
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public Person getOrgUnitManager() {
		return orgUnitManager;
	}
	public void setOrgUnitManager(Person orgUnitManager) {
		this.orgUnitManager = orgUnitManager;
	}

	public HashMap<String, String> compareVersion(VersionControl o) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
