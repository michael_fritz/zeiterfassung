package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import de.bosc.timerecording.basic.tool.Util;

public class Device extends VersionControl implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private transient boolean editable = false;
	private String primaryKey;
	private boolean active;
	private String unid;
	private String deviceKey = "";
	private String deviceName = "not found";
	private String locationdocid = "";
	private String companydocid = "";
	private Vector<String> deviceManager = new Vector<String>();
	
	private transient List<Device> listVersions;	
	private transient Company company = null;
	private transient Location location = null;

	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}	
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}
	
	public String getDeviceKey() {
		return deviceKey;
	}
	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public Vector<String> getDeviceManager() {
		return deviceManager;
	}
	public void setDeviceManager(Vector<String> deviceManager) {
		this.deviceManager = deviceManager;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public String getLocationdocid() {
		return locationdocid;
	}
	public void setLocationdocid(String locationdocid) {
		this.locationdocid = locationdocid;
	}
	public String getCompanydocid() {
		return companydocid;
	}
	public void setCompanydocid(String companydocid) {
		this.companydocid = companydocid;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public List<Device> getListVersions() {
		return listVersions;
	}
	public void setListVersions(List<Device> listVersions) {
		this.listVersions = listVersions;
	}
	
	@Override
	public HashMap<String, String> compareVersion(VersionControl o) {
		HashMap<String, String> changes = new HashMap<String, String>();
		Device device = (Device)o;
		if(!this.getPrimaryKey().equals(device.getPrimaryKey())){
			changes.put("Primary key", this.getPrimaryKey());
		}
		if(this.isActive() != device.isActive()){
			changes.put("Active", String.valueOf(this.isActive()));
		}
		if(!this.getDeviceName().equals(device.getDeviceName())){
			changes.put("Device name", this.getDeviceName());
		}
		if(!this.getCompanydocid().equals(device.getCompanydocid())){
			changes.put("Company key", this.getCompanydocid());
		}
		if(!this.getLocationdocid().equals(device.getLocationdocid())){
			changes.put("Location key", this.getLocationdocid());
		}
		if(!Util.equals(this.getDeviceManager(),device.getDeviceManager())){
			changes.put("Device manager", Util.implode(this.getDeviceManager(), "; "));
		}
		return changes;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((companydocid == null) ? 0 : companydocid.hashCode());
		result = prime * result + ((deviceKey == null) ? 0 : deviceKey.hashCode());
		result = prime * result + ((deviceManager == null) ? 0 : deviceManager.hashCode());
		result = prime * result + ((deviceName == null) ? 0 : deviceName.hashCode());
		result = prime * result + ((locationdocid == null) ? 0 : locationdocid.hashCode());
		result = prime * result + ((primaryKey == null) ? 0 : primaryKey.hashCode());
		result = prime * result + ((unid == null) ? 0 : unid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Device other = (Device) obj;
		if (active != other.active)
			return false;
		if (companydocid == null) {
			if (other.companydocid != null)
				return false;
		} else if (!companydocid.equals(other.companydocid))
			return false;
		if (deviceKey == null) {
			if (other.deviceKey != null)
				return false;
		} else if (!deviceKey.equals(other.deviceKey))
			return false;
		if (deviceManager == null) {
			if (other.deviceManager != null)
				return false;
		} else if (!deviceManager.equals(other.deviceManager))
			return false;
		if (deviceName == null) {
			if (other.deviceName != null)
				return false;
		} else if (!deviceName.equals(other.deviceName))
			return false;
		if (locationdocid == null) {
			if (other.locationdocid != null)
				return false;
		} else if (!locationdocid.equals(other.locationdocid))
			return false;
		if (primaryKey == null) {
			if (other.primaryKey != null)
				return false;
		} else if (!primaryKey.equals(other.primaryKey))
			return false;
		if (unid == null) {
			if (other.unid != null)
				return false;
		} else if (!unid.equals(other.unid))
			return false;
		return true;
	}

	
}
