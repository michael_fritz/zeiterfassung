package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import de.bosc.timerecording.basic.interfaces.VersionCompare;

public class Location extends VersionControl implements Serializable ,VersionCompare{
	private static final long serialVersionUID = 1L;

	private String unid;
	private String name;
	private String displayname;
	private String primaryKey;
	private boolean active;
	private transient boolean editable = false;

	
	private transient List<Location> listVersions;	
	
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public List<Location> getListVersions() {
		return listVersions;
	}
	public void setListVersions(List<Location> listVersions) {
		this.listVersions = listVersions;
	}
	public HashMap<String, String> compareVersion(VersionControl o) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
