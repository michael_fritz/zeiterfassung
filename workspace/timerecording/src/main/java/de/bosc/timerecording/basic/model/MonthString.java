package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.bosc.timerecording.basic.interfaces.ITimeString;
import de.bosc.timerecording.basic.service.TimeentryService;

public class MonthString implements Serializable,ITimeString, Comparable<MonthString>{

	private static final long serialVersionUID = 1L;
	private String month;

	public MonthString(Date date) {
		SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy-MM");
		this.month = monthFormat.format(date);
	}
	public MonthString(String month) {
		this.month = month;
	}

	public String getMonth() {
		return month;
	}
	
	public Date getDate() throws ParseException{
		return TimeentryService.parseTimeEntryDate(month + "-01", TimeentryService.parseTimeToString(0));
	}

	public void setMonth(String month) {
		this.month = month;
	}

	@Override
	public String toString() {
		return month ;
	}
	
	public int compareTo(MonthString o) {
		return this.toString().compareTo(o.toString());
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonthString other = (MonthString) obj;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		return true;
	}

}
