package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import de.bosc.timerecording.basic.interfaces.VersionCompare;

public class Notification extends VersionControl implements Serializable,VersionCompare {

	private static final long serialVersionUID = 1L;
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-");

	private String unid;
	private Date date;
	private String creator;
	private String message;
	private String primaryKey;
	private String personalDocId;
	
	private boolean notRead = true;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public boolean isNotRead() {
		return notRead;
	}
	public void setNotRead(boolean notRead) {
		this.notRead = notRead;
	}

	public String getPersonalDocId() {
		return personalDocId;
	}
	public void setPersonalDocId(String personalDocId) {
		this.personalDocId = personalDocId;
	}
	public String getKey(){
		return dateFormat.format(date) + " " + creator;
	}

	
	
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}
	public HashMap<String, String> compareVersion(VersionControl o) {
		// TODO Auto-generated method stub
		return null;
	}

}
