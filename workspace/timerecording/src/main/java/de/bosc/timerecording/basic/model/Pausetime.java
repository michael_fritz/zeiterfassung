package de.bosc.timerecording.basic.model;

import java.io.Serializable;

public class Pausetime implements Serializable{

	private Integer after;
	private Integer pause;
	public Integer getAfter() {
		return after;
	}
	public void setAfter(Integer after) {
		this.after = after;
	}
	public Integer getPause() {
		return pause;
	}
	public void setPause(Integer pause) {
		this.pause = pause;
	}
	
	public Pausetime clone(){
		Pausetime pausetime = new Pausetime();
		pausetime.setAfter(after != null?new Integer(after):null);
		pausetime.setPause(pause != null?new Integer(pause):null);
		return pausetime;
	}
}
