package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Vector;

import de.bosc.timerecording.basic.interfaces.VersionCompare;

public class Person extends VersionControl implements Serializable ,VersionCompare{
	private static final long serialVersionUID = 1L;

	private String unid;
	private String primaryKey;
	private String lastname;
	private String firstname;
	private String personalNumber;
	private String shortname;
	private String passcode;
	private Date entryDate;
	private Date dateOfSeparation;
	private TreeMap<Long,WorktimeSetting> worktimeSettings;
	private Vector<String> rfidTagsTime;
	private String locationDocId;
	private String companyDocId;
	private String departmentDocId;
	private Double startOvertime;
	private Double startHoliday;
	private Integer holidayEntitlement;
	private String superiorId;

	
	private boolean active;

	private String emailBusiness;
	private String emailPrivate;
	
	private String phoneBusiness;
	private String phonePrivate;
	private String phoneMobile;
	private String fax;

	private String street;
	private String zip;
	private String city;
	private String region;
	private String country;

	private transient boolean editable = false;
	
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	public String getFullname(){
		return lastname + ", " + firstname;
	}
	public String getSuperiorId() {
		return superiorId;
	}
	public void setSuperiorId(String superiorId) {
		this.superiorId = superiorId;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getPersonalNumber() {
		return personalNumber;
	}
	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
	public Date getDateOfSeparation() {
		return dateOfSeparation;
	}
	public void setDateOfSeparation(Date dateOfSeparation) {
		this.dateOfSeparation = dateOfSeparation;
	}
	public Vector<String> getRfidTagsTime() {
		return rfidTagsTime;
	}

	public void setRfidTagsTime(Vector<String> rfidTagsTime) {
		this.rfidTagsTime = rfidTagsTime;
	}

	public String getEmailBusiness() {
		return emailBusiness;
	}
	public void setEmailBusiness(String emailBusiness) {
		this.emailBusiness = emailBusiness;
	}
	public String getEmailPrivate() {
		return emailPrivate;
	}
	public void setEmailPrivate(String emailPrivate) {
		this.emailPrivate = emailPrivate;
	}
	public String getPhoneBusiness() {
		return phoneBusiness;
	}
	public void setPhoneBusiness(String phoneBusiness) {
		this.phoneBusiness = phoneBusiness;
	}
	public String getPhonePrivate() {
		return phonePrivate;
	}
	public void setPhonePrivate(String phonePrivate) {
		this.phonePrivate = phonePrivate;
	}
	public String getPhoneMobile() {
		return phoneMobile;
	}
	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getPasscode() {
		return passcode;
	}
	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public void setWorktimeSettings(TreeMap<Long, WorktimeSetting> worktimeSettings) {
		this.worktimeSettings = worktimeSettings;
	}

	public TreeMap<Long, WorktimeSetting> getWorktimeSettings() {
		return worktimeSettings;
	}

	public String getLocationDocId() {
		return locationDocId;
	}

	public void setLocationDocId(String locationDocId) {
		this.locationDocId = locationDocId;
	}

	public String getCompanyDocId() {
		return companyDocId;
	}

	public String getDepartmentDocId() {
		return departmentDocId;
	}

	public void setDepartmentDocId(String departmentDocId) {
		this.departmentDocId = departmentDocId;
	}

	public void setCompanyDocId(String companyDocId) {
		this.companyDocId = companyDocId;
	}

	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Double getStartOvertime() {
		return startOvertime;
	}
	public void setStartOvertime(Double startOvertime) {
		this.startOvertime = startOvertime;
	}

	public Double getStartHoliday() {
		return startHoliday;
	}
	public void setStartHoliday(Double startHoliday) {
		this.startHoliday = startHoliday;
	}
	public Integer getHolidayEntitlement() {
		return holidayEntitlement;
	}

	public void setHolidayEntitlement(Integer holidayEntitlement) {
		this.holidayEntitlement = holidayEntitlement;
	}

	@Override
	public String toString() {
		return getFullname();
	}

	public HashMap<String, String> compareVersion(VersionControl o) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((companyDocId == null) ? 0 : companyDocId.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((departmentDocId == null) ? 0 : departmentDocId.hashCode());
		result = prime * result + ((emailBusiness == null) ? 0 : emailBusiness.hashCode());
		result = prime * result + ((emailPrivate == null) ? 0 : emailPrivate.hashCode());
		result = prime * result + ((entryDate == null) ? 0 : entryDate.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((holidayEntitlement == null) ? 0 : holidayEntitlement.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((locationDocId == null) ? 0 : locationDocId.hashCode());
		result = prime * result + ((passcode == null) ? 0 : passcode.hashCode());
		result = prime * result + ((personalNumber == null) ? 0 : personalNumber.hashCode());
		result = prime * result + ((phoneBusiness == null) ? 0 : phoneBusiness.hashCode());
		result = prime * result + ((phoneMobile == null) ? 0 : phoneMobile.hashCode());
		result = prime * result + ((phonePrivate == null) ? 0 : phonePrivate.hashCode());
		result = prime * result + ((primaryKey == null) ? 0 : primaryKey.hashCode());
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		result = prime * result + ((rfidTagsTime == null) ? 0 : rfidTagsTime.hashCode());
		result = prime * result + ((shortname == null) ? 0 : shortname.hashCode());
		result = prime * result + ((startHoliday == null) ? 0 : startHoliday.hashCode());
		result = prime * result + ((startOvertime == null) ? 0 : startOvertime.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((superiorId == null) ? 0 : superiorId.hashCode());
		result = prime * result + ((unid == null) ? 0 : unid.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (active != other.active)
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (companyDocId == null) {
			if (other.companyDocId != null)
				return false;
		} else if (!companyDocId.equals(other.companyDocId))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (departmentDocId == null) {
			if (other.departmentDocId != null)
				return false;
		} else if (!departmentDocId.equals(other.departmentDocId))
			return false;
		if (emailBusiness == null) {
			if (other.emailBusiness != null)
				return false;
		} else if (!emailBusiness.equals(other.emailBusiness))
			return false;
		if (emailPrivate == null) {
			if (other.emailPrivate != null)
				return false;
		} else if (!emailPrivate.equals(other.emailPrivate))
			return false;
		if (entryDate == null) {
			if (other.entryDate != null)
				return false;
		} else if (!entryDate.equals(other.entryDate))
			return false;
		if (fax == null) {
			if (other.fax != null)
				return false;
		} else if (!fax.equals(other.fax))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (holidayEntitlement == null) {
			if (other.holidayEntitlement != null)
				return false;
		} else if (!holidayEntitlement.equals(other.holidayEntitlement))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (locationDocId == null) {
			if (other.locationDocId != null)
				return false;
		} else if (!locationDocId.equals(other.locationDocId))
			return false;
		if (passcode == null) {
			if (other.passcode != null)
				return false;
		} else if (!passcode.equals(other.passcode))
			return false;
		if (personalNumber == null) {
			if (other.personalNumber != null)
				return false;
		} else if (!personalNumber.equals(other.personalNumber))
			return false;
		if (phoneBusiness == null) {
			if (other.phoneBusiness != null)
				return false;
		} else if (!phoneBusiness.equals(other.phoneBusiness))
			return false;
		if (phoneMobile == null) {
			if (other.phoneMobile != null)
				return false;
		} else if (!phoneMobile.equals(other.phoneMobile))
			return false;
		if (phonePrivate == null) {
			if (other.phonePrivate != null)
				return false;
		} else if (!phonePrivate.equals(other.phonePrivate))
			return false;
		if (primaryKey == null) {
			if (other.primaryKey != null)
				return false;
		} else if (!primaryKey.equals(other.primaryKey))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		if (rfidTagsTime == null) {
			if (other.rfidTagsTime != null)
				return false;
		} else if (!rfidTagsTime.equals(other.rfidTagsTime))
			return false;
		if (shortname == null) {
			if (other.shortname != null)
				return false;
		} else if (!shortname.equals(other.shortname))
			return false;
		if (startHoliday == null) {
			if (other.startHoliday != null)
				return false;
		} else if (!startHoliday.equals(other.startHoliday))
			return false;
		if (startOvertime == null) {
			if (other.startOvertime != null)
				return false;
		} else if (!startOvertime.equals(other.startOvertime))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (superiorId == null) {
			if (other.superiorId != null)
				return false;
		} else if (!superiorId.equals(other.superiorId))
			return false;
		if (unid == null) {
			if (other.unid != null)
				return false;
		} else if (!unid.equals(other.unid))
			return false;
		if (zip == null) {
			if (other.zip != null)
				return false;
		} else if (!zip.equals(other.zip))
			return false;
		return true;
	}
	
}
