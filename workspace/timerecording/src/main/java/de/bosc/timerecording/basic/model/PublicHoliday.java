package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import de.bosc.timerecording.basic.interfaces.VersionCompare;
import de.bosc.timerecording.basic.tool.Util;

public class PublicHoliday  extends VersionControl implements Serializable,Comparable<PublicHoliday>,VersionCompare {
	private static final long serialVersionUID = 1L;

	private transient List<PublicHoliday> listVersions;

	
	private String unid;
	private String name;
	private String date;
	private Double timeBonus;
	private boolean halfDay;
	private Vector<String> locations;
	private Vector<String> companies;
	
	
	public Double getTimeBonus() {
		return timeBonus;
	}
	public void setTimeBonus(Double timeBonus) {
		this.timeBonus = timeBonus;
	}

	public boolean isHalfDay() {
		return halfDay;
	}
	public void setHalfDay(boolean halfDay) {
		this.halfDay = halfDay;
	}
	public Vector<String> getCompanies() {
		return companies;
	}
	public void setCompanies(Vector<String> companies) {
		this.companies = companies;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Vector<String> getLocations() {
		return locations;
	}
	public void setLocations(Vector<String> locations) {
		this.locations = locations;
	}
	public List<PublicHoliday> getListVersions() {
		return listVersions;
	}
	public void setListVersions(List<PublicHoliday> listVersions) {
		this.listVersions = listVersions;
	}
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}

	@Override
	public HashMap<String, String> compareVersion(VersionControl o) {
		HashMap<String, String> changes = new HashMap<String, String>();
		PublicHoliday holiday = (PublicHoliday)o;

		if(!this.getTimeBonus().equals(holiday.getTimeBonus())){
			changes.put("Time Bonus", this.getDate());
		}
		if(this.isHalfDay() != holiday.isHalfDay()){
			changes.put("Half day", this.getDate());
		}
		if(!this.getDate().equals(holiday.getDate())){
			changes.put("Date", this.getDate());
		}
		if(!this.getName().equals(holiday.getName())){
			changes.put("Name", this.getName());
		}
		if(!Util.equals(this.getLocations(),holiday.getLocations())){
			changes.put("Locations", Util.implode(this.getLocations(), "; "));
		}
		if(!Util.equals(this.getCompanies(),holiday.getCompanies())){
			changes.put("Locations", Util.implode(this.getLocations(), "; "));
		}
 
		return changes;
	}
	public int compareTo(PublicHoliday o) {
		return this.getDate().compareTo(o.getDate());
	}
}
