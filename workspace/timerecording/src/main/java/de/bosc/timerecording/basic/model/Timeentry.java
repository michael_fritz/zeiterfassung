package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

public class Timeentry implements Serializable, Comparable<Timeentry>{
	private static final long serialVersionUID = 1L;
	private String primaryKey;
	private String unid;
	private String primaryKeyOther;
	private String personalNumber;
	private DateString startDate;
	private DateString endDate;
	private Integer startTime;
	private Integer endTime;
	private Vector<String> dates;
	private String timeentryKindId;
	private String deviceDocIdStart;
	private String deviceDocIdEnd;
	private String companyDocId;
	private String locationDocId;
	private String departmentDocId;
	private String editHistory;
	private String edited;
	private String fullname;
	private String systemClosed;
	private String timeDuration;
	private String comment;
	private YearString year; 
	private YearString yearOther; 
	private boolean deleted;
	private boolean newEntry;
	private boolean collision;
	
	private transient String processHint;

	
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public YearString getYearOther() {
		return yearOther;
	}
	public void setYearOther(YearString yearOther) {
		this.yearOther = yearOther;
	}
	public YearString getYear() {
		return year;
	}
	public void setYear(YearString year) {
		this.year = year;
	}
	public String getPrimaryKeyOther() {
		return primaryKeyOther;
	}

	public boolean isNewEntry() {
		return newEntry;
	}
	public void setNewEntry(boolean newEntry) {
		this.newEntry = newEntry;
	}
	public void setPrimaryKeyOther(String primaryKeyOther) {
		this.primaryKeyOther = primaryKeyOther;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public String getTimeDuration() {
		return timeDuration;
	}
	public void setTimeDuration(String timeDuration) {
		this.timeDuration = timeDuration;
	}
	public String getSystemClosed() {
		return systemClosed;
	}
	public void setSystemClosed(String systemClosed) {
		this.systemClosed = systemClosed;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getEditHistory() {
		return editHistory;
	}
	public void setEditHistory(String editHistory) {
		this.editHistory = editHistory;
	}
	public String getEdited() {
		return edited;
	}
	public void setEdited(String edited) {
		this.edited = edited;
	}
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}
	public String getPersonalNumber() {
		return personalNumber;
	}
	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}
	public String getTimeentryKindId() {
		return timeentryKindId;
	}
	public void setTimeentryKindId(String timeentryKindId) {
		this.timeentryKindId = timeentryKindId;
	}
	public String getDeviceDocIdStart() {
		return deviceDocIdStart;
	}
	public void setDeviceDocIdStart(String deviceDocIdStart) {
		this.deviceDocIdStart = deviceDocIdStart;
	}
	public String getDeviceDocIdEnd() {
		return deviceDocIdEnd;
	}
	public void setDeviceDocIdEnd(String deviceDocIdEnd) {
		this.deviceDocIdEnd = deviceDocIdEnd;
	}
	public DateString getStartDate() {
		return startDate;
	}
	public void setStartDate(DateString startDate) {
		this.startDate = startDate;
	}
	public DateString getEndDate() {
		return endDate;
	}
	public void setEndDate(DateString endDate) {
		this.endDate = endDate;
	}
	public Integer getStartTime() {
		return startTime;
	}
	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}
	public Integer getEndTime() {
		return endTime;
	}
	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}
	public String getProcessHint() {
		return processHint;
	}
	public void setProcessHint(String processHint) {
		this.processHint = processHint;
	}

	public String getCompanyDocId() {
		return companyDocId;
	}
	public void setCompanyDocId(String companyDocId) {
		this.companyDocId = companyDocId;
	}
	public String getLocationDocId() {
		return locationDocId;
	}
	public void setLocationDocId(String locationDocId) {
		this.locationDocId = locationDocId;
	}
	public String getDepartmentDocId() {
		return departmentDocId;
	}
	public void setDepartmentDocId(String departmentDocId) {
		this.departmentDocId = departmentDocId;
	}
	public Vector<String> getDates() {
		return dates;
	}
	public void setDates(Vector<String> dates) {
		this.dates = dates;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public boolean isCollision() {
		return collision;
	}
	public void setCollision(boolean collision) {
		this.collision = collision;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (collision ? 1231 : 1237);
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((companyDocId == null) ? 0 : companyDocId.hashCode());
		result = prime * result + ((dates == null) ? 0 : dates.hashCode());
		result = prime * result + (deleted ? 1231 : 1237);
		result = prime * result + ((departmentDocId == null) ? 0 : departmentDocId.hashCode());
		result = prime * result + ((deviceDocIdEnd == null) ? 0 : deviceDocIdEnd.hashCode());
		result = prime * result + ((deviceDocIdStart == null) ? 0 : deviceDocIdStart.hashCode());
		result = prime * result + ((editHistory == null) ? 0 : editHistory.hashCode());
		result = prime * result + ((edited == null) ? 0 : edited.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((locationDocId == null) ? 0 : locationDocId.hashCode());
		result = prime * result + (newEntry ? 1231 : 1237);
		result = prime * result + ((personalNumber == null) ? 0 : personalNumber.hashCode());
		result = prime * result + ((primaryKey == null) ? 0 : primaryKey.hashCode());
		result = prime * result + ((primaryKeyOther == null) ? 0 : primaryKeyOther.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		result = prime * result + ((systemClosed == null) ? 0 : systemClosed.hashCode());
		result = prime * result + ((timeDuration == null) ? 0 : timeDuration.hashCode());
		result = prime * result + ((timeentryKindId == null) ? 0 : timeentryKindId.hashCode());
		result = prime * result + ((unid == null) ? 0 : unid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Timeentry other = (Timeentry) obj;
		if (collision != other.collision)
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (companyDocId == null) {
			if (other.companyDocId != null)
				return false;
		} else if (!companyDocId.equals(other.companyDocId))
			return false;
		if (dates == null) {
			if (other.dates != null)
				return false;
		} else if (!dates.equals(other.dates))
			return false;
		if (deleted != other.deleted)
			return false;
		if (departmentDocId == null) {
			if (other.departmentDocId != null)
				return false;
		} else if (!departmentDocId.equals(other.departmentDocId))
			return false;
		if (deviceDocIdEnd == null) {
			if (other.deviceDocIdEnd != null)
				return false;
		} else if (!deviceDocIdEnd.equals(other.deviceDocIdEnd))
			return false;
		if (deviceDocIdStart == null) {
			if (other.deviceDocIdStart != null)
				return false;
		} else if (!deviceDocIdStart.equals(other.deviceDocIdStart))
			return false;
		if (editHistory == null) {
			if (other.editHistory != null)
				return false;
		} else if (!editHistory.equals(other.editHistory))
			return false;
		if (edited == null) {
			if (other.edited != null)
				return false;
		} else if (!edited.equals(other.edited))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (locationDocId == null) {
			if (other.locationDocId != null)
				return false;
		} else if (!locationDocId.equals(other.locationDocId))
			return false;
		if (newEntry != other.newEntry)
			return false;
		if (personalNumber == null) {
			if (other.personalNumber != null)
				return false;
		} else if (!personalNumber.equals(other.personalNumber))
			return false;
		if (primaryKey == null) {
			if (other.primaryKey != null)
				return false;
		} else if (!primaryKey.equals(other.primaryKey))
			return false;
		if (primaryKeyOther == null) {
			if (other.primaryKeyOther != null)
				return false;
		} else if (!primaryKeyOther.equals(other.primaryKeyOther))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		if (systemClosed == null) {
			if (other.systemClosed != null)
				return false;
		} else if (!systemClosed.equals(other.systemClosed))
			return false;
		if (timeDuration == null) {
			if (other.timeDuration != null)
				return false;
		} else if (!timeDuration.equals(other.timeDuration))
			return false;
		if (timeentryKindId == null) {
			if (other.timeentryKindId != null)
				return false;
		} else if (!timeentryKindId.equals(other.timeentryKindId))
			return false;
		if (unid == null) {
			if (other.unid != null)
				return false;
		} else if (!unid.equals(other.unid))
			return false;
		return true;
	}
	
	public int compareTo(Timeentry timeentry) {
		String datetime = this.getStartDate() + " " + this.getStartTime();
		return datetime.compareTo(timeentry.getStartDate() + " " + timeentry.getStartTime());
	}

	
}
