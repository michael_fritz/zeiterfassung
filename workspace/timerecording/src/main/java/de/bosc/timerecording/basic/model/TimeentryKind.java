package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

import de.bosc.timerecording.basic.interfaces.VersionCompare;

public class TimeentryKind extends VersionControl implements Serializable,VersionCompare {
	@Override
	public String toString() {
		return name;
	}

	private static final long serialVersionUID = 1L;

	private String primaryKey;
	private String name;
	private String active;
	private String notation;
	private String description;
	private Double value;
	private String percent = "0";
	private Integer sort = 0;
	private Vector<String> timeEntryOptions;
	private String timeEntryOptionsDefault;
	private String timeCalculation;
	private String selectable = "0";
	
	
	public String getSelectable() {
		return selectable;
	}
	public void setSelectable(String selectable) {
		this.selectable = selectable;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getPercent() {
		return percent;
	}
	public void setPercent(String percent) {
		this.percent = percent;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public String getTimeEntryOptionsDefault() {
		return timeEntryOptionsDefault;
	}
	public void setTimeEntryOptionsDefault(String timeEntryOptionsDefault) {
		this.timeEntryOptionsDefault = timeEntryOptionsDefault;
	}
	public String getTimeCalculation() {
		return timeCalculation;
	}
	public void setTimeCalculation(String timeCalculation) {
		this.timeCalculation = timeCalculation;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getNotation() {
		return notation;
	}
	public void setNotation(String notation) {
		this.notation = notation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public Vector<String> getTimeEntryOptions() {
		return timeEntryOptions;
	}
	public void setTimeEntryOptions(Vector<String> timeEntryOptions) {
		this.timeEntryOptions = timeEntryOptions;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public TimeentryKind clone(){
		TimeentryKind timeentryKindClone = new TimeentryKind();
		timeentryKindClone.setActive(active);
		timeentryKindClone.setDescription(description);
		timeentryKindClone.setName(name);
		timeentryKindClone.setNotation(notation);
		timeentryKindClone.setPrimaryKey(primaryKey);
		timeentryKindClone.setValue(value);
		timeentryKindClone.setPercent(percent);
		timeentryKindClone.setSort(sort);
		timeentryKindClone.setSelectable(selectable);
		timeentryKindClone.setTimeEntryOptionsDefault(timeEntryOptionsDefault);
		Vector<String> v = new Vector<String>();
		v.addAll(timeEntryOptions);
		timeentryKindClone.setTimeEntryOptions(v);
		timeentryKindClone.setTimeCalculation(timeCalculation);
		return timeentryKindClone;
	}
	public HashMap<String, String> compareVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	public HashMap<String, String> compareVersion(VersionControl o) {
		// TODO Auto-generated method stub
		return null;
	}
}
