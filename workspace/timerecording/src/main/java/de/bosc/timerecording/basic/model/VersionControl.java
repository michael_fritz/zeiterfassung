package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.HashMap;

public abstract class VersionControl implements Serializable{

	private static final long serialVersionUID = 1L;
	private transient String updated;
	private String updateDate;
	private String updatedBy;
	
	abstract public HashMap<String,String> compareVersion(VersionControl o);

	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}
