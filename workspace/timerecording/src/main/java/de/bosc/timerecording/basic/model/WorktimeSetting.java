package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import de.bosc.timerecording.basic.interfaces.VersionCompare;
import de.bosc.timerecording.basic.service.TimeentryKindService;
import de.bosc.timerecording.basic.service.TimeentryService;
import de.bosc.timerecording.basic.tool.Util;

public class WorktimeSetting extends VersionControl implements Serializable ,VersionCompare{
	private static final long serialVersionUID = 1L;

	private String unid;
	private String parentDocId;
	private String primaryKey;
	private String creator;
	private String parentForm;
	private Date validAfterDate = new Date();
//	private Person superior;
//	private Department department;
//	private Location location;
//	private String superiorDocId;
	private String departmentDocId;
	private String locationDocId;
	private String companyDocId;
	private Map<Integer,Integer> dailyWorktimes = new TreeMap<Integer, Integer>();
	private Map<Integer,Double> dailyPercents = new TreeMap<Integer, Double>();
	private Pausetime[] pausetimes = {new Pausetime(),new Pausetime(),new Pausetime()};	
	private Map<String,TimeentryKind> timeentryKinds = new HashMap<String, TimeentryKind>();
	private Integer pausetimeGeneral;
	private Integer coreTimeBegin;					//in seconds
	private Integer coreTimeEnd;					//in seconds
	private Integer inclusiveTime;					//in seconds
	private Double coreTimeExtraAmount;				//in procent
	private Double hourlyRate;					// n �
	
	public WorktimeSetting() {
		dailyWorktimes.put(Calendar.SUNDAY, null);
		dailyWorktimes.put(Calendar.MONDAY, null);
		dailyWorktimes.put(Calendar.TUESDAY, null);
		dailyWorktimes.put(Calendar.WEDNESDAY, null);
		dailyWorktimes.put(Calendar.THURSDAY, null);
		dailyWorktimes.put(Calendar.FRIDAY, null);
		dailyWorktimes.put(Calendar.SATURDAY, null);
		dailyPercents.put(Calendar.SUNDAY, null);
		dailyPercents.put(Calendar.MONDAY, null);
		dailyPercents.put(Calendar.TUESDAY, null);
		dailyPercents.put(Calendar.WEDNESDAY, null);
		dailyPercents.put(Calendar.THURSDAY, null);
		dailyPercents.put(Calendar.FRIDAY, null);
		dailyPercents.put(Calendar.SATURDAY, null);		
	}

	public String getParentForm() {
		return parentForm;
	}

	public void setParentForm(String parentForm) {
		this.parentForm = parentForm;
	}

	public Double getCoreTimeExtraAmount() {
		return coreTimeExtraAmount;
	}

	public void setCoreTimeExtraAmount(Double coreTimeExtraAmount) {
		this.coreTimeExtraAmount = coreTimeExtraAmount;
	}

	public Pausetime[] getPausetimes() {
		return pausetimes;
	}

	public void setPausetimes(Pausetime[] pausetimes) {
		this.pausetimes = pausetimes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Map<Integer, Double> getDailyPercents() {
		return dailyPercents;
	}

	public void setDailyPercents(Map<Integer, Double> dailyPercents) {
		this.dailyPercents = dailyPercents;
	}


	public Date getValidAfterDate() {
		return validAfterDate;
	}
	public void setValidAfterDate(Date validAfterDate) {
		this.validAfterDate = validAfterDate;
	}
	public String getDepartmentDocId() {
		return departmentDocId;
	}
	public void setDepartmentDocId(String departmentDocId) {
		this.departmentDocId = departmentDocId;
	}
	public Map<Integer, Integer> getDailyWorktimes() {
		return dailyWorktimes;
	}

	public void setDailyWorktimes(Map<Integer, Integer> dailyWorktimes) {
		this.dailyWorktimes = dailyWorktimes;
	}
	public Integer getPausetimeGeneral() {
		return pausetimeGeneral;
	}
	public Double getHourlyRate() {
		return hourlyRate;
	}
	public void setHourlyRate(Double hourlyRate) {
		this.hourlyRate = hourlyRate;
	}
	public String getPrimaryKey(){
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getLocationDocId() {
		return locationDocId;
	}
	public void setLocationDocId(String locationDocId) {
		this.locationDocId = locationDocId;
	}
	public Integer getCoreTimeBegin() {
		return coreTimeBegin;
	}
	/**
	 * sets the start time of the core time in seconds
	 * @param coreTimeBegin
	 */
	public void setCoreTimeBegin(Integer coreTimeBegin) {
		this.coreTimeBegin = coreTimeBegin;
	}
	public Integer getCoreTimeEnd() {
		return coreTimeEnd;
	}
	/**
	 * sets the end time of the core time in seconds
	 * @param coreTimeBegin
	 */
	public void setCoreTimeEnd(Integer coreTimeEnd) {
		this.coreTimeEnd = coreTimeEnd;
	}
	public Integer getInclusiveTime() {
		return inclusiveTime;
	}
	public void setInclusiveTime(Integer inclusiveTime) {
		this.inclusiveTime = inclusiveTime;
	}
	public void setPausetimeGeneral(Integer pausetimeGeneral) {
		this.pausetimeGeneral = pausetimeGeneral;
	}
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}
	public String getParentDocId() {
		return parentDocId;
	}
	public void setParentDocId(String parentDocId) {
		this.parentDocId = parentDocId;
	}
	public String getCompanyDocId() {
		return companyDocId;
	}
	public void setCompanyDocId(String companyDocId) {
		this.companyDocId = companyDocId;
	}
	public Integer getDailyWorktimeSunday(){
		return dailyWorktimes.get(Calendar.SUNDAY);
	}
	public void setDailyWorktimeSunday(Integer dailyWorktime) {
		dailyWorktimes.put(Calendar.SUNDAY,dailyWorktime);
	}
	public Integer getDailyWorktimeMonday(){
		return dailyWorktimes.get(Calendar.MONDAY);
	}
	public void setDailyWorktimeMonday(Integer dailyWorktime) {
		dailyWorktimes.put(Calendar.MONDAY,dailyWorktime);
	}
	public Integer getDailyWorktimeThuesday(){
		return dailyWorktimes.get(Calendar.TUESDAY);
	}
	public void setDailyWorktimeThuesday(Integer dailyWorktime) {
		dailyWorktimes.put(Calendar.TUESDAY,dailyWorktime);
	}
	public Integer getDailyWorktimeWednesday(){
		return dailyWorktimes.get(Calendar.WEDNESDAY);
	}
	public void setDailyWorktimeWednesday(Integer dailyWorktime) {
		dailyWorktimes.put(Calendar.WEDNESDAY,dailyWorktime);
	}
	public Integer getDailyWorktimeThursday(){
		return dailyWorktimes.get(Calendar.THURSDAY);
	}
	public void setDailyWorktimeThursday(Integer dailyWorktime) {
		dailyWorktimes.put(Calendar.THURSDAY,dailyWorktime);
	}
	public Integer getDailyWorktimeFryday(){
		return dailyWorktimes.get(Calendar.FRIDAY);
	}
	public void setDailyWorktimeFryday(Integer dailyWorktime) {
		dailyWorktimes.put(Calendar.FRIDAY,dailyWorktime);
	}
	public Integer getDailyWorktimeSaturday(){
		return dailyWorktimes.get(Calendar.SATURDAY);
	}
	public void setDailyWorktimeSaturday(Integer dailyWorktime) {
		dailyWorktimes.put(Calendar.SATURDAY,dailyWorktime);
	}
	
	/* DAILY PERCENT*/
	public Double getDailyPercentSunday(){
		return dailyPercents.get(Calendar.SUNDAY);
	}
	public void setDailyPercentSunday(Double dailyPercent) {
		dailyPercents.put(Calendar.SUNDAY,dailyPercent);
	}
	public Double getDailyPercentMonday(){
		return dailyPercents.get(Calendar.MONDAY);
	}
	public void setDailyPercentMonday(Double dailyPercent) {
		dailyPercents.put(Calendar.MONDAY,dailyPercent);
	}
	public Double getDailyPercentTuesday(){
		return dailyPercents.get(Calendar.TUESDAY);
	}
	public void setDailyPercentTuesday(Double dailyPercent) {
		dailyPercents.put(Calendar.TUESDAY,dailyPercent);
	}
	public Double getDailyPercentWednesday(){
		return dailyPercents.get(Calendar.WEDNESDAY);
	}
	public void setDailyPercentWednesday(Double dailyPercent) {
		dailyPercents.put(Calendar.WEDNESDAY,dailyPercent);
	}
	public Double getDailyPercentThursday(){
		return dailyPercents.get(Calendar.THURSDAY);
	}
	public void setDailyPercentThursday(Double dailyPercent) {
		dailyPercents.put(Calendar.THURSDAY,dailyPercent);
	}
	public Double getDailyPercentFriday(){
		return dailyPercents.get(Calendar.FRIDAY);
	}
	public void setDailyPercentFriday(Double dailyPercent) {
		dailyPercents.put(Calendar.FRIDAY,dailyPercent);
	}
	public Double getDailyPercentSaturday(){
		return dailyPercents.get(Calendar.SATURDAY);
	}
	public void setDailyPercentSaturday(Double dailyPercent) {
		dailyPercents.put(Calendar.SATURDAY,dailyPercent);
	}	
	public Map<String, TimeentryKind> getTimeentryKinds() {
		return timeentryKinds;
	}
	public void setTimeentryKinds(Map<String, TimeentryKind> timeentryKinds) {
		this.timeentryKinds = timeentryKinds;
	}
//	public String getSuperiorDocId() {
//		return superiorDocId;
//	}
//
//	public void setSuperiorDocId(String superiorDocId) {
//		this.superiorDocId = superiorDocId;
//	}

	public HashMap<String, String> compareVersion(VersionControl o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WorktimeSetting clone() {
		WorktimeSetting worktimeSetting = new WorktimeSetting();
		worktimeSetting.setCoreTimeBegin(coreTimeBegin != null?new Integer(coreTimeBegin):null);
		worktimeSetting.setCoreTimeEnd(coreTimeEnd != null?new Integer(coreTimeEnd):null);
		worktimeSetting.setCoreTimeExtraAmount(coreTimeExtraAmount != null?new Double(coreTimeExtraAmount):null);		
		worktimeSetting.setDailyPercents(Util.deepCopyDouble((TreeMap<Integer, Double>) dailyPercents));
		worktimeSetting.setDailyWorktimes((TreeMap<Integer, Integer>) dailyWorktimes);
		worktimeSetting.setDepartmentDocId(departmentDocId);
		worktimeSetting.setHourlyRate(hourlyRate != null?new Double(hourlyRate):null);
		worktimeSetting.setInclusiveTime(inclusiveTime != null?new Integer(inclusiveTime):null);
		worktimeSetting.setLocationDocId(locationDocId);
		worktimeSetting.setParentDocId(parentDocId);
		worktimeSetting.setParentForm(parentForm);
		worktimeSetting.setPausetimeGeneral(pausetimeGeneral != null?new Integer(pausetimeGeneral):null);
		
		Pausetime[] pausetimesClone = {new Pausetime(),new Pausetime(),new Pausetime()};	
		for(int i = 0;i<3; i++){
			pausetimesClone[i] = pausetimes[i].clone();
		}
		worktimeSetting.setPausetimes(pausetimesClone);
		
		worktimeSetting.setPrimaryKey(primaryKey);
		
		worktimeSetting.setTimeentryKinds(TimeentryKindService.getCopyTimeentryKinds(timeentryKinds));
		
		worktimeSetting.setUnid(unid);
		worktimeSetting.setValidAfterDate(new Date(validAfterDate.getTime()));

		return worktimeSetting;
	}
	
	
}
