package de.bosc.timerecording.basic.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.bosc.timerecording.basic.interfaces.ITimeString;
import de.bosc.timerecording.basic.service.TimeentryService;

public class YearString implements Serializable,ITimeString, Comparable<YearString>{

	private static final long serialVersionUID = 1L;
	private String year;

	public YearString(Date date) {
		SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy");
		this.year = monthFormat.format(date);
	}
	
	public YearString(String year) {
		this.year = year;
	}
	
	public Date getDate() throws ParseException{
		return TimeentryService.parseTimeEntryDate(year + "-01-01", TimeentryService.parseTimeToString(0));
	}
	
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return year;
	}

	public int compareTo(YearString o) {
		return this.toString().compareTo(o.toString());
	}

	public boolean equals(YearString year) {
		return this.year.equals(year.getYear());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		YearString other = (YearString) obj;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}
	
	
}
