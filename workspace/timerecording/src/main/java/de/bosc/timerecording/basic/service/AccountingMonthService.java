package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.AccountingMonth;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.YearString;

public class AccountingMonthService implements Serializable{
	private static final long serialVersionUID = 1L;

	private PersistenceService persistenceService;
	private Logger logger;

	public AccountingMonthService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
		this.logger = persistenceService.getLogger();
	}
	
	public Map<MonthString, AccountingMonth> getAccountingMonthsYear(Person person,YearString year){
		return persistenceService.getAccountingMonthsYear(person,year);
	}
	
	public List<AccountingMonth> getAccountingMonths(MonthString month){
		return persistenceService.getAccountingMonths(month);
	}
	
	
	public boolean saveAccountingMonth(AccountingMonth accountingMonth){
		try {
			return persistenceService.saveAccountingMonth(accountingMonth);
		} catch (PersistenceException e) {
			logger.error("", e);
		}
		return false;
	}


}
