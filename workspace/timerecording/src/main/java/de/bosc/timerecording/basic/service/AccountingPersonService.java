package de.bosc.timerecording.basic.service;

import java.io.Serializable;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.AccountingPerson;
import de.bosc.timerecording.basic.model.Person;

public class AccountingPersonService  implements Serializable{
	private static final long serialVersionUID = 1L;

	private PersistenceService persistenceService;
	private Logger logger;

	public AccountingPersonService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
		this.logger = persistenceService.getLogger();
	}
	
	public AccountingPerson getAccountingPerson(Person person){
		return persistenceService.getAccountingPerson(person);
	}
	
	
	public boolean saveAccountingPerson(AccountingPerson accountingPerson){
		try {
			return persistenceService.saveAccountingPerson(accountingPerson);
		} catch (PersistenceException e) {
			logger.error("", e);
		}
		return false;
	}

}