package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.Map;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.AccountingYear;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.YearString;

public class AccountingYearService  implements Serializable{
	private static final long serialVersionUID = 1L;

	private PersistenceService persistenceService;
	private Logger logger;

	public AccountingYearService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
		this.logger = persistenceService.getLogger();
	}
	
	public Map<YearString, AccountingYear> getAccountingYears(Person person){
		return persistenceService.getAccountingYears(person);
	}
	
	
	public boolean saveAccountingYear(AccountingYear accountingYear){
		try {
			return persistenceService.saveAccountingYear(accountingYear);
		} catch (PersistenceException e) {
			logger.error("", e);
		}
		return false;
	}

}
