package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.HashMap;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.model.Company;

public class CompanyService implements Serializable{
	private static final long serialVersionUID = 1L;

	private PersistenceService persistenceService;
	private HashMap<String,Company> companies;

	public CompanyService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
	}

	public HashMap<String, Company> getCompanies() throws PersistenceException {
		if(companies == null){
			companies = new HashMap<String, Company>();
			for(Company company:this.persistenceService.getCompanies()){
				companies.put(company.getPrimaryKey(), company);
			}
		}		
		return companies;
	}
	
	public Company getCompany(String key) throws PersistenceException{
		return getCompanies().get(key);
	}

	public void setCompanies(HashMap<String, Company> companies) {
		this.companies = companies;
	}
	
}
