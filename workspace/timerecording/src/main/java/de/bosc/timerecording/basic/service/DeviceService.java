package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.HashMap;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.model.Device;

public class DeviceService implements Serializable{
	private static final long serialVersionUID = 1L;

	private PersistenceService persistenceService;

	public DeviceService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
	}
	
	public Device getDevice(String key) throws PersistenceException{
		Device device = getDevices().get(key);
		return device;
	}

	public HashMap<String, Device> getDevices() throws PersistenceException {
		HashMap<String,Device> devices = new HashMap<String, Device>();
		for(Device device: persistenceService.getDevices()){
			devices.put(device.getDeviceKey(), device);
		}
		return devices;
	}

}
