package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.model.Location;

public class LocationService implements Serializable{
	private static final long serialVersionUID = 1L;

	private PersistenceService persistenceService;
	private Map<String,Location> locations;
	
	public LocationService(PersistenceService persistenceService) {
		persistenceService.getLogger().debug("init",this.getClass());
		this.persistenceService = persistenceService;
	}

	public PersistenceService getPersistenceService() {
		return persistenceService;
	}

	public void setPersistenceService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
	}

	public Map<String, Location> getLocations() throws PersistenceException {
		if(locations == null){
			persistenceService.getLogger().debug("locations: get new ",this.getClass());
			locations = new HashMap<String, Location>();
			for(Location location: persistenceService.getLocations()){
				locations.put(location.getPrimaryKey(), location);				
			}
		}else{
			persistenceService.getLogger().debug("locations already exists: " + locations.size() ,this.getClass());			
		}
		return locations;
	}
	
	public void saveLocation(Location location) {
		try {
			persistenceService.saveLocation(location);
		} catch (PersistenceException e) {
			persistenceService.getLogger().error("", e);
		}
	}

	public void setLocations(Map<String, Location> locations) {
		this.locations = locations;
	}
	
	public Location getLocation(String key) throws PersistenceException{
		return getLocations().get(key);
	}
	
}
