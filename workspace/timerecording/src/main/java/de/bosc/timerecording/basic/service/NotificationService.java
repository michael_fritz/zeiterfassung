package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.TreeMap;

import javax.net.ssl.KeyStoreBuilderParameters;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.model.Notification;

public class NotificationService  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private PersistenceService persistenceService;
	private TreeMap<String,TreeMap<String,Notification>> notifications;
	
	public NotificationService( PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
	}
	
	public TreeMap<String,Notification> getPersonNotifications(String personDocId) throws PersistenceException{
		return getNotifications().get(personDocId);
	}
	
	public TreeMap<String,TreeMap<String,Notification>> getNotifications() throws PersistenceException {
		if(notifications == null){
			notifications = new TreeMap<String,TreeMap<String,Notification>>();
			for(Notification notification:this.persistenceService.getNotifications()){
				TreeMap<String,Notification> personNoifications = notifications.get(notification.getPersonalDocId()); 
				if(personNoifications == null){
					personNoifications = new TreeMap<String, Notification>();
					notifications.put(notification.getPersonalDocId(), personNoifications);
				}
				personNoifications.put(notification.getKey(), notification);
			}
		}		
		return notifications;
	}
}
