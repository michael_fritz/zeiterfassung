package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.Person;

public class PersonService  implements Serializable{
	private static final long serialVersionUID = 1L;

	private PersistenceService persistenceService;
	private HashMap<String,Person> persons;

	public PersonService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
	}

	private HashMap<String, Person> readPersonsFromPersistenceService() throws PersistenceException{
		if(persons == null){
			persons = new HashMap<String, Person>();			
			for(Person person:this.persistenceService.getPersons()){
				persons.put(person.getPersonalNumber(), person);
			}
		}
		return persons;
	}
	
	public Map<String, Person> getPersons() throws PersistenceException {
		return readPersonsFromPersistenceService();
	}

	/**
	 * 
	 * @param personalNumber
	 * @return
	 * @throws PersistenceException
	 */
	public Person getPerson(String personalNumber) throws PersistenceException {
		Person person = null;
		for(Person p:getPersons().values()){
			if(p.getPersonalNumber().equals(personalNumber)){
				person = p;
				break;
			}
		}
		return person;
	}
	
	public void setPersons(HashMap<String, Person> persons) {
		this.persons = persons;
	}

	public Map<String, Person> getPersonsActiveFullname() throws PersistenceException {
		Logger logger = persistenceService.getLogger();
		TreeMap<String, Person> personsActiveFullname = new TreeMap<String, Person>();
		HashMap<String,Person> personList = readPersonsFromPersistenceService();
		for(Person person:personList.values()){
			if(person.isActive()){
				personsActiveFullname.put(person.getFullname(), person);					
			}		
		}		
		return personsActiveFullname;
	}
	
	public Map<String, Person> getPersonsByFullname() throws PersistenceException {
		TreeMap<String, Person> personsActiveFullname = new TreeMap<String, Person>();
		HashMap<String,Person> personList = readPersonsFromPersistenceService();
		for(Person person:personList.values()){
			personsActiveFullname.put(person.getFullname(), person);					
		}		
		return personsActiveFullname;
	}

	public Map<String, Person> getPersonsByTagId() throws PersistenceException {
		HashMap<String,Person> personsByTagId = new HashMap<String,Person>();;
		HashMap<String,Person> personList = readPersonsFromPersistenceService();
		for(Person person:personList.values()){
			for(String tagid:person.getRfidTagsTime()){
				personsByTagId.put(tagid, person);						
			}
		}
		return personsByTagId;
	}

	public Map<String,TreeMap<String,Person>> getPersonsByLocation() throws PersistenceException {
		HashMap<String,TreeMap<String,Person>> personsByLocation = new HashMap<String,TreeMap<String,Person>>();
		HashMap<String,Person> personList = readPersonsFromPersistenceService();
		for(Person person:personList.values()){
			if(!personsByLocation.containsKey(person.getLocationDocId())){
				TreeMap<String,Person> newLocation = new TreeMap<String,Person>();
				personsByLocation.put(person.getLocationDocId(),newLocation);
			}
			personsByLocation.get(person.getLocationDocId()).put(person.getFullname(),person);
		}		
		return personsByLocation;
	}
	
	public Map<String,TreeMap<String,Person>> getPersonsByCompany() throws PersistenceException {
		HashMap<String,TreeMap<String,Person>> personsByCompany = new HashMap<String,TreeMap<String,Person>>();
		HashMap<String,Person> personList = readPersonsFromPersistenceService();
		for(Person person:personList.values()){
			if(!personsByCompany.containsKey(person.getCompanyDocId())){
				TreeMap<String,Person> newLocation = new TreeMap<String,Person>();
				personsByCompany.put(person.getCompanyDocId(),newLocation);
			}
			personsByCompany.get(person.getCompanyDocId()).put(person.getFullname(),person);
		}		
		return personsByCompany;
	}
	
	public Map<String, Person> findPerson(String searchString,Map<String,Person> allPersons){
		HashMap<String, Person> personList = new  HashMap<String, Person>();
		try {
			for(Map.Entry<String,Person> person:allPersons.entrySet()){
				if(person.getValue().getFullname().toUpperCase().contains(searchString.toUpperCase())){
					personList.put(person.getKey(),person.getValue());				
				} else if(person.getValue().getPersonalNumber().toUpperCase().contains(searchString.toUpperCase())){
					personList.put(person.getKey(),person.getValue());	
				}

			}
		} catch (Exception e) {
			persistenceService.getLogger().error("", e);
		}
		persistenceService.getLogger().debug("findPerson: " + personList.size(),this.getClass());
		
		return personList;
	}
	
}
