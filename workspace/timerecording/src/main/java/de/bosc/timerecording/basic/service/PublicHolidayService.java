package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.PublicHoliday;
import de.bosc.timerecording.basic.model.YearString;

public class PublicHolidayService implements Serializable{
	private static final long serialVersionUID = 1L;

	private PersistenceService persistenceService;
	private Logger logger;

	public PublicHolidayService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
		this.logger = persistenceService.getLogger();
	}
	
	public Map<DateString,PublicHoliday> getPublicHolidaysForYear(YearString year){
		Map<DateString,PublicHoliday> mapHolidays = new TreeMap<DateString, PublicHoliday>();
		try{
			for(PublicHoliday publicHoliday:persistenceService.getPublicHolidaysYear(year)){
				mapHolidays.put(new DateString(publicHoliday.getDate()),publicHoliday);
			}	
		}catch(Exception e){
			logger.error("",e);
		}
		return mapHolidays;
	}

	public PublicHoliday getPublicHoliday(Map<DateString,PublicHoliday> mapHolidays, DateString date,String key){
		PublicHoliday publicHoliday = null;
		StringBuffer sb = new StringBuffer("getPublicHoliday: " + date + " key: " + key + "\n");
		
		for (PublicHoliday pubHoliday : mapHolidays.values()) {
			if(pubHoliday.getDate().equals(date) && (pubHoliday.getLocations().contains(key) || pubHoliday.getCompanies().contains(key))){
				publicHoliday = pubHoliday;
			}
		}
		sb.append("--- is public holiday: " + (publicHoliday!=null));
		logger.debug(sb.toString(),this.getClass());
		return publicHoliday;
	}
}
