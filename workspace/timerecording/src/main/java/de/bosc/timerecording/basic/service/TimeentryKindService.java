package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.model.TimeentryKind;

public class TimeentryKindService implements Serializable{
	private static final long serialVersionUID = 1L;

	private PersistenceService persistenceService;
	private HashMap<String,TimeentryKind> timeentryKinds;

	public TimeentryKindService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
	}
	
	public HashMap<String,TimeentryKind> getTimeentryKinds()  {
		persistenceService.getLogger().debug("getCopyTimeentryKinds timeentryKinds: " + (timeentryKinds == null), this.getClass());
		try{
			if(timeentryKinds == null){
				timeentryKinds = new HashMap<String, TimeentryKind>();
				for(TimeentryKind timeentryKind: persistenceService.getTimeentryKinds()){
					timeentryKinds.put(timeentryKind.getPrimaryKey(), timeentryKind);				
				}
				if(!timeentryKinds.containsKey("0")){
					persistenceService.getLogger().debug("getCopyTimeentryKinds timeentryKind for 0 not configurated: ", this.getClass());
					TimeentryKind timeentryKind = new TimeentryKind();
					timeentryKind.setActive("1");
					timeentryKind.setValue(null);
					timeentryKind.setPercent("0");
					timeentryKind.setTimeCalculation("1");
					Vector<String> v = new Vector<String>();
					v.add("3");
					timeentryKind.setTimeEntryOptions(v);
					timeentryKind.setTimeEntryOptionsDefault("3");
					timeentryKind.setSort(0);
					timeentryKind.setDescription("normal work time");
					timeentryKind.setNotation("Arbeit");
					timeentryKind.setName("Arbeit");
					timeentryKind.setPrimaryKey("0");
					timeentryKinds.put(timeentryKind.getPrimaryKey(), timeentryKind);				
				}
			}			
		}catch(Exception e){
			persistenceService.getLogger().error("", e);
		}

		return timeentryKinds;
	}
	
	public TimeentryKind getTimeentryKind(String primaryKey){
		return getTimeentryKinds().get(primaryKey);
	}
	
	/**
	 * Gibt die zugeh�rige Zeiteintragsart zur�ck 
	 * 
	 * @param key
	 * @return
	 */
	public static TimeentryKind getTimeentryKind(List<TimeentryKind> timeentryKinds,String key){
		TimeentryKind timeentryKind = null;
		for(TimeentryKind kind:timeentryKinds){
			if(kind.getPrimaryKey().equals(key)){
				timeentryKind = kind;
				break;
			}
		}
		return timeentryKind;
	}
	
	
	public List<TimeentryKind> getTimeentryKindsList(){
		List<TimeentryKind> list = new ArrayList<TimeentryKind>();
		try {
			for(TimeentryKind timeentryKind:getTimeentryKinds().values()){
				list.add(timeentryKind);
			}
		} catch (Exception e) {
			persistenceService.getLogger().error("", e);
		}
		return list;
	}
	
	public Map<String,TimeentryKind> getCopyTimeentryKinds() throws PersistenceException{
		HashMap<String,TimeentryKind> copyTimeentryKinds = new HashMap<String, TimeentryKind>();
		persistenceService.getLogger().debug("getCopyTimeentryKinds: " + copyTimeentryKinds.size(), this.getClass());
		for(TimeentryKind timeentryKind:getTimeentryKinds().values()){
			timeentryKind.setSelectable("0");
			copyTimeentryKinds.put(timeentryKind.getPrimaryKey(), timeentryKind.clone());
		}		
		return copyTimeentryKinds;
	}
	
	public static Map<String,TimeentryKind> getCopyTimeentryKinds(Map<String,TimeentryKind> timeentryKinds){
		Map<String,TimeentryKind> copyTimeentryKinds = new HashMap<String, TimeentryKind>();
		for(TimeentryKind timeentryKind:timeentryKinds.values()){
			copyTimeentryKinds.put(timeentryKind.getPrimaryKey(), timeentryKind.clone());
		}		
		return copyTimeentryKinds;
	}
}
