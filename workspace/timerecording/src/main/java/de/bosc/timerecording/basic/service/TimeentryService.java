package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.logging.Logger;
import de.bosc.timerecording.basic.model.DateString;
import de.bosc.timerecording.basic.model.MonthString;
import de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.Timeentry;
import de.bosc.timerecording.basic.model.YearString;

public class TimeentryService implements Serializable{
	private static final long serialVersionUID = 1L;
	public static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	public static final DecimalFormat decimalFormat = new DecimalFormat("00");
	public static final int MAX_ENDTIME = 24*60*60;
	
	/**
	 * Format = yyyy-MM-dd HH:mm:ss
	 * @param date
	 * @param time
	 * @return
	 * @throws ParseException
	 */
	public static Date parseTimeEntryDate(String date, String time) throws ParseException{
		return dateTimeFormat.parse(date + " " + time);
	}
	
	public static String parseTimeToString(Integer time){
		int seconds = time;
		int hours = (seconds - seconds % 3600)/3600;
		seconds = seconds-hours*3600;
		int minutes = (seconds - seconds % 60)/60;
		seconds = seconds-minutes*60;
		String result = decimalFormat.format(hours) + ":" + decimalFormat.format(minutes) + ":" + decimalFormat.format(seconds);
		return result;//hours==24?"24:00:00":timeFormat.format(cal.getTime());
	}
	
	public static Integer parseDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.HOUR_OF_DAY)*60*60 + cal.get(Calendar.MINUTE)*60 + cal.get(Calendar.SECOND);
	}
	
	private PersistenceService persistenceService;
	private Logger logger;
	
	public TimeentryService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
		this.logger = persistenceService.getLogger();
	}

	public Map<DateString,List<Timeentry>> getTimeentriesMonth(String personalNumber, MonthString month) {
		return this.persistenceService.getTimeentriesMonth(personalNumber, month);
	}

	public Map<DateString,List<Timeentry>> getTimeentriesMonthDeleted(String personalNumber, MonthString month) {
		return this.persistenceService.getTimeentriesMonthDeleted(personalNumber, month);
	}

	public List<Timeentry> getOpenTimeentries(String personalNumber) throws PersistenceException{
		return persistenceService.getOpenTimeentries(personalNumber);
	}
	
	public Timeentry getTimeentry(String unid, YearString year){
		return persistenceService.getTimeentry(unid,year);
	}
	
	public Timeentry getOpenTimeentry(Person person) throws ParseException, PersistenceException{
		Timeentry timeentry = null;
		if(person != null){
			List<Timeentry> openTimentries = getOpenTimeentries(person.getPersonalNumber());
			logger.debug("getOpenTimeentry - openTimentries.size:" + openTimentries.size(),this.getClass());
			for(Timeentry entry:openTimentries){
				entry = checkTimeEntry(entry);
			}
			for(Timeentry entry:openTimentries){
				if(timeentry == null && entry.getEndTime() == -1){
					logger.debug("getOpenTimeentry - set timeentry: " + entry.getStartDate() + " " + entry.getStartTime(),this.getClass());
					timeentry = entry;
				}else if(entry.getEndTime() == -1){
					logger.debug("getOpenTimeentry - set timeentry: " + entry.getStartDate() + " " + entry.getStartTime(),this.getClass());
					Date dateTimeeentry = parseTimeEntryDate(timeentry.getStartDate().toString(), parseTimeToString(timeentry.getStartTime()));
					Date dateEntry = parseTimeEntryDate(entry.getStartDate().toString(), parseTimeToString(entry.getStartTime()));
					if(dateEntry.after(dateTimeeentry)){
						//TODO Zeiteintrag schliessen
						timeentry = entry;
					}
				}
			}
		}
		return timeentry;
	}

	public Timeentry checkTimeEntry(Timeentry timeentry){
		//TODO close openTimeentry if cutoffday reached
		
		return timeentry;
	}
	
	public void saveTimeEntry(Timeentry timeentry) throws PersistenceException{
		persistenceService.saveTimeentry(timeentry);
		
	}
	
	public int getDayWorktime(String personalNumber, String date){
		int worktime = 0;
		logger.debug("----- getDayWorktime: " + date,this.getClass());
		try {
			List<Timeentry> timeentries = persistenceService.getTimeentries(personalNumber,date);
			logger.debug("----- getDayWorktime found: " + timeentries.size(),this.getClass());
			for(Timeentry timeentry:timeentries){
				if(!timeentry.getStartDate().equals(date)){
					timeentry.setStartDate(new DateString(date));
					timeentry.setStartTime(0);
				}
				worktime +=getDuration(timeentry);
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		
		return worktime;
	}
	
	public String getDayWorktimeString(String personalNumber, String date){
		int worktime = getDayWorktime(personalNumber, date);
		return parseTimeToString(worktime);		
	}
	
	public String getWorktimeOpenTimeentry(Timeentry timeentry){
		int worktime = 0;
		try {
			worktime = getDuration(timeentry);
			logger.debug("getWorktimeOpenTimeentry: " + worktime,this.getClass());
		} catch (ParseException e) {
			logger.error("", e);
		}
		return parseTimeToString(worktime);
	}
	
	public int getDuration(Timeentry timeentry) throws ParseException{
		int duration = 0;
		if(timeentry.getStartTime() != -1 && timeentry.getEndTime() != -1){
			duration = timeentry.getEndTime() - timeentry.getStartTime();
		}else{
			Date dateTimeentry = dateTimeFormat.parse(timeentry.getStartDate() + " " + parseTimeToString(timeentry.getStartTime()));		
			duration = (int) ((new Date()).getTime() - dateTimeentry.getTime())/1000;
		}
		return duration;
	}
	
	public void updateTimeentry(Timeentry timeentry){
		
	}
	
}
