package de.bosc.timerecording.basic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.bosc.timerecording.basic.business.PersistenceException;
import de.bosc.timerecording.basic.business.PersistenceService;
import de.bosc.timerecording.basic.enumeration.LogLevel;
import de.bosc.timerecording.basic.logging.LoggerDomino;
import de.bosc.timerecording.basic.model.DateString;
import 	de.bosc.timerecording.basic.model.Person;
import de.bosc.timerecording.basic.model.TimeentryKind;
import de.bosc.timerecording.basic.model.WorktimeSetting;
import de.bosc.timerecording.basic.tool.Util;

public class WorktimeSettingService implements Serializable{
	private static final long serialVersionUID = 4L;

	private PersistenceService persistenceService;

	public WorktimeSettingService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
	}

	public WorktimeSetting getWorktimeSettingForDate(Person person, Date date) throws PersistenceException{
		WorktimeSetting worktimeSetting = null;
		if(person != null){
			persistenceService.getLogger().debug("getWorktimeSettingForDate: Person: " + person.getFullname(),this.getClass());
			worktimeSetting = this.getWorktimeSettingForDate(person.getPrimaryKey(), date);
			if(worktimeSetting == null){
				persistenceService.getLogger().debug("getWorktimeSettingForDate: no setting found get setings for location: " + person.getLocationDocId(),this.getClass());
				worktimeSetting = new WorktimeSettingService(persistenceService).getWorktimeSettingForDate(person.getLocationDocId(), date);
			}

		}
		return worktimeSetting;
	}
	
	public WorktimeSetting getWorktimeSettingForDate(String primaryKey, Date date) throws PersistenceException{
		WorktimeSetting worktimeSetting = null;
		StringBuffer sb = new StringBuffer("getWorktimeSettingForDate: " + primaryKey + " Date: " + date + "\n");
		try{
			Map<Date, WorktimeSetting> worktimeSettings = getWorktimeSettings(primaryKey);
			sb.append("--- worktimeSettings f�r " + primaryKey + " gefunden: " + worktimeSettings.size() + "\n");
			worktimeSetting = getWorktimeSettingForDate(worktimeSettings,date);
			worktimeSetting = getCompareWorktimeSetting(worktimeSetting, date);
			if(worktimeSetting != null){
				sb.append("--- Arbeitseinstellungen Datum = " + worktimeSetting.getValidAfterDate() + "\n");
				sb.append("--- ParentForm = " + worktimeSetting.getParentForm() + "\n");				
			}else{
				sb.append("--- Keine Arbeitseinstellung gefunden!!\n");				
			}
			persistenceService.getLogger().debug(sb.toString(),this.getClass());
		}catch(Exception e){
			persistenceService.getLogger().error(sb.toString() + "\n\n",e);
		}
	
		return worktimeSetting;
	}
	
	
	/**
	 * @param worktimeSettingsLocations
	 * @param worktimeSettings
	 * @param date
	 * @return
	 */
	public WorktimeSetting getCompareWorktimeSettingPerson( Map<String,Map<Date, WorktimeSetting>> worktimeSettingsLocations, Map<Date, WorktimeSetting> worktimeSettings,Date date){
		WorktimeSetting worktimeSetting = null;
		StringBuffer sbTrace = new StringBuffer();
		try{
			for(WorktimeSetting ws:worktimeSettings.values()){
				persistenceService.getLogger().trace("getWorktimeSettingForDate(TreeMap<Date, WorktimeSetting>,date): " + ws.getValidAfterDate() + " = " + (ws.getValidAfterDate().compareTo(date)<=0),this.getClass());
				if(ws.getValidAfterDate().before(date) || new DateString(date).equals(new DateString(ws.getValidAfterDate()))){
					worktimeSetting = ws;
				}

			}
			if(worktimeSetting != null){
				String locationDocId = worktimeSetting.getLocationDocId();
				if(worktimeSettingsLocations.containsKey(locationDocId)){
					WorktimeSetting worktimeSettingLocation = null;
					for(WorktimeSetting ws:worktimeSettingsLocations.get(locationDocId).values()){
						if(ws.getValidAfterDate().before(date) || new DateString(date).equals(new DateString(ws.getValidAfterDate()))){
							worktimeSettingLocation = ws;
						}
					}
					if(worktimeSettingLocation != null){
						worktimeSetting = writeLocationSettingsToPersonSettings(worktimeSetting, worktimeSettingLocation, sbTrace);
					}
				}
			}
			persistenceService.getLogger().trace("getWorktimeSettingForDate(TreeMap<Date, WorktimeSetting>,date): " + worktimeSetting.getValidAfterDate(), this.getClass() );
			
		}catch(Exception e){
			
		}
		return worktimeSetting;
	}

	/**
	 * @param worktimeSetting
	 * @param date
	 * @return
	 */
	public WorktimeSetting getCompareWorktimeSetting(WorktimeSetting worktimeSetting,Date date){
		StringBuffer sbTrace = new StringBuffer("getCompareWorktimeSettings " + TimeentryService.dateFormat.format(date) + "\n");
		StringBuffer sbDebug = new StringBuffer();
		StringBuffer sbDefault = new StringBuffer();
		LogLevel logLevel = ((LoggerDomino)persistenceService.getLogger()).getLogLevelForClass(this.getClass());
		try{
			if(worktimeSetting != null){
				
				sbDefault.append("----- Arbeitseinstellungen Datum:" + TimeentryService.dateFormat.format(date)   +  "\n");
				sbDefault.append("----- Arbeitseinstellungen f�r:" + worktimeSetting.getParentForm().toUpperCase()  +  "\n");
				sbDefault.append("----- f�r Datum: :" + TimeentryService.dateFormat.format(worktimeSetting.getValidAfterDate())  +  "\n");
				sbDefault.append("----- Key:" + worktimeSetting.getPrimaryKey()  +  "\n");
				if(logLevel == LogLevel.TRACE){
					sbTrace.append("=============================================================\n");
					sbTrace.append("=================== WERTE VOR ERSETZUNGEN ===================\n");
					sbTrace.append(getWorktimeSettingInfo(worktimeSetting));
				}
			}
			if(worktimeSetting != null && worktimeSetting.getParentForm() != null && worktimeSetting.getParentForm().equals("Person")){
				worktimeSetting = worktimeSetting.clone();
				sbTrace.append("=============================================================\n\n");
				sbTrace.append("=================== SETZE WERTE STANDORTWERTEN ===================\n\n");
				sbTrace.append("----- Location for person: " + worktimeSetting.getLocationDocId() + "\n");
				if(worktimeSetting.getLocationDocId() != null && !worktimeSetting.getLocationDocId().equals("")){
					WorktimeSetting worktimeSettingLocation = getWorktimeSettingForDate(worktimeSetting.getLocationDocId(),date);
					sbTrace.append("----- Location worktimeSetting date = " + (worktimeSettingLocation != null?worktimeSettingLocation.getValidAfterDate():" worktime setting for location is null") + "\n");
					if(worktimeSettingLocation != null){
						if(logLevel == LogLevel.TRACE){
							sbDebug.append(getWorktimeSettingInfo(worktimeSettingLocation));
						}
						worktimeSetting = writeLocationSettingsToPersonSettings(worktimeSetting, worktimeSettingLocation, sbTrace);
					}
				}
			}

			if(logLevel == LogLevel.TRACE){
				sbDebug.append(sbTrace.toString());				
			}
			if(worktimeSetting != null && worktimeSetting.getParentForm().equals("Person")){
				sbDebug.append(sbDefault.toString());	
				sbDebug.append("=============================================================\n");
				sbDebug.append("=================== WERTE NACH ERSETZUNGEN ==================\n");
				sbDebug.append(getWorktimeSettingInfo(worktimeSetting));
			}
			if(!sbDebug.toString().equals("")){
				persistenceService.getLogger().debug(sbDebug.toString(), this.getClass());					
			}
			
			
		}catch(Exception e){
			persistenceService.getLogger().error(sbTrace.toString(), e);
		}
		return worktimeSetting;
	}
	
	public WorktimeSetting writeLocationSettingsToPersonSettings(WorktimeSetting worktimeSetting, WorktimeSetting worktimeSettingLocation,StringBuffer sbTrace){

		/* 
		 * vergleiche Arbeitseinstellungen Person und Standort und setzte
		 * leere Werte aus Standorteinstellungen in Personeneinstellungen
		 */	
		sbTrace.append("CoreTimeBegin: " + worktimeSetting.getCoreTimeBegin() + " -- " + worktimeSettingLocation.getCoreTimeBegin() + "\n");
		if(worktimeSetting.getCoreTimeBegin() == null){
			worktimeSetting.setCoreTimeBegin(worktimeSettingLocation.getCoreTimeBegin());
		}

		sbTrace.append("CoreTimeEnd: " + worktimeSetting.getCoreTimeEnd() + " -- " + worktimeSettingLocation.getCoreTimeEnd() + "\n");
		if(worktimeSetting.getCoreTimeEnd() == null){
			worktimeSetting.setCoreTimeEnd(worktimeSettingLocation.getCoreTimeEnd());
		}
		
		sbTrace.append("CoreTimeExtraAmount: " + worktimeSetting.getCoreTimeExtraAmount() + " -- " + worktimeSettingLocation.getCoreTimeExtraAmount() + "\n");
		if(worktimeSetting.getCoreTimeExtraAmount() == null){
			worktimeSetting.setCoreTimeExtraAmount(worktimeSettingLocation.getCoreTimeExtraAmount());
		}


		sbTrace.append("------------ DailyPercent --------" + "\n");
		for(int i = 1;i<8;i++){
			sbTrace.append("i: " + i + " ==== " + worktimeSetting.getDailyPercents().get(i) + "  ---  " + worktimeSettingLocation.getDailyPercents().get(i) + "\n");
			if( worktimeSetting.getDailyPercents().get(i) == null){
				 worktimeSetting.getDailyPercents().put(i,worktimeSettingLocation.getDailyPercents().get(i));
			}			
		}
		sbTrace.append("--------------------------------------" + "\n");

		sbTrace.append("------------ DailyWorktime --------" + "\n");
		for(int i = 1;i<8;i++){
			sbTrace.append("i: " + i + " ==== " + worktimeSetting.getDailyWorktimes().get(i) + "  ---  " + worktimeSettingLocation.getDailyWorktimes().get(i) + "\n");
			if( worktimeSetting.getDailyWorktimes().get(i) == null){
				 worktimeSetting.getDailyWorktimes().put(i,worktimeSettingLocation.getDailyWorktimes().get(i));
			}			
		}
		sbTrace.append("--------------------------------------" + "\n");

		sbTrace.append("HourlyRate: " + worktimeSetting.getHourlyRate() + " -- " + worktimeSettingLocation.getHourlyRate() + "\n");
		if(worktimeSetting.getHourlyRate() == null){
			worktimeSetting.setHourlyRate(worktimeSettingLocation.getHourlyRate());
		}

		sbTrace.append("InclusiveTime: " + worktimeSetting.getInclusiveTime() + " -- " + worktimeSettingLocation.getInclusiveTime() + "\n");
		if(worktimeSetting.getInclusiveTime() == null){
			worktimeSetting.setInclusiveTime(worktimeSettingLocation.getInclusiveTime());
		}
	
		sbTrace.append("PausetimeGeneral: " + worktimeSetting.getPausetimeGeneral() + " -- " + worktimeSettingLocation.getPausetimeGeneral() + "\n");
		if(worktimeSetting.getPausetimeGeneral() == null){
			worktimeSetting.setPausetimeGeneral(worktimeSettingLocation.getPausetimeGeneral());
		}
	
		sbTrace.append("------------ Pausetimes --------" + "\n");
		for(int i = 0;i<3;i++){
			sbTrace.append("i: " + i + " ==== nach:" + worktimeSetting.getPausetimes()[i].getAfter() + " Pause: " + worktimeSetting.getPausetimes()[i].getPause() + "  ---  nach:" + worktimeSettingLocation.getPausetimes()[i].getAfter() + " Pause: " + worktimeSettingLocation.getPausetimes()[i].getPause() + "\n");
			if(worktimeSetting.getPausetimes()[i].getAfter() == null || worktimeSetting.getPausetimes()[i].getPause() == null){
				worktimeSetting.getPausetimes()[i].setAfter(worktimeSettingLocation.getPausetimes()[i].getAfter());
				worktimeSetting.getPausetimes()[i].setPause(worktimeSettingLocation.getPausetimes()[i].getPause());
			}
		}
		
		sbTrace.append("------------ TimeentryKinds --------" + "\n");
		for(Map.Entry<String,TimeentryKind> timeentryKind:worktimeSettingLocation.getTimeentryKinds().entrySet()){
			sbTrace.append("--- Arbeitsart: " + timeentryKind.getValue().getName() + " bei der Person gesetzt?  " + worktimeSetting.getTimeentryKinds().keySet().contains(timeentryKind.getKey()) + "\n");
			if(!worktimeSetting.getTimeentryKinds().keySet().contains(timeentryKind.getKey())){
				worktimeSetting.getTimeentryKinds().put(timeentryKind.getKey(), timeentryKind.getValue());
				sbTrace.append("------ Zuschlag f�r Arbeitsart hinzuf�gen: " + timeentryKind.getValue().getName() + "\n");
			}else{
				TimeentryKind timeentryKindPerson = worktimeSetting.getTimeentryKinds().get(timeentryKind.getKey());
				if(timeentryKindPerson.getSelectable().equals("0")){
					timeentryKindPerson.setSelectable(timeentryKind.getValue().getSelectable());
				}
				if(timeentryKindPerson.getValue() == null){
					timeentryKindPerson.setValue(timeentryKind.getValue().getValue());
					timeentryKindPerson.setPercent(timeentryKind.getValue().getPercent());
				}
				
			}

		}
		
		return worktimeSetting;
	}
	
	
	public StringBuffer getWorktimeSettingInfo(WorktimeSetting worktimeSetting){
		StringBuffer sb = new StringBuffer();
		
		if(worktimeSetting != null){
			sb.append("=============================================================\n------------ Ergebnis workzime setting --------\n");			
			sb.append("Arbeitseinstellungen f�r: " + worktimeSetting.getParentForm().toUpperCase() + "\n");
			sb.append("Key: " + worktimeSetting.getPrimaryKey() + "\n");
			sb.append("G�ltig ab: " + TimeentryService.dateFormat.format(worktimeSetting.getValidAfterDate()) + "\n");
			sb.append("--------------------------------------------------\n\n");
			sb.append("normale Arbeitszeit Beginn: " + TimeentryService.parseTimeToString(worktimeSetting.getCoreTimeBegin()) + "\n");
			sb.append("normale Arbeitszeit Ende: " + TimeentryService.parseTimeToString(worktimeSetting.getCoreTimeEnd())  + "\n");
			sb.append("Zuschlag ausserhalb normaler Arbeitszeit: " + Util.getPercent(worktimeSetting.getCoreTimeExtraAmount()) + "\n");

			sb.append("------------ Wochentagsarbeitszeiten --------" + "\n");
			for(int i = 1;i<8;i++){
				sb.append(Util.getWeekdayText(i) + " ==== " + Util.getHours(worktimeSetting.getDailyWorktimes().get(i)) + "\n");		
			}
			sb.append("--------------------------------------" + "\n");

			sb.append("------------ Wochentagszuschl�ge --------" + "\n");
			for(int i = 1;i<8;i++){
				sb.append(Util.getWeekdayText(i) + " ==== " + Util.getPercent(worktimeSetting.getDailyPercents().get(i)) + "\n");
			}
			sb.append("--------------------------------------" + "\n");

			sb.append("Stundensatz: " + worktimeSetting.getHourlyRate() + "\n");
			sb.append("Inkl. �berstunden: " + Util.getHours(worktimeSetting.getInclusiveTime()) +"\n");
			sb.append("pauschaler Abzug: " + Util.getMinutes(worktimeSetting.getPausetimeGeneral()) + "\n");
			
			sb.append("------------ Pausetimes --------" + "\n");
			for(int i = 0;i<3;i++){
				sb.append("---- nach: " + Util.getHours(worktimeSetting.getPausetimes()[i].getAfter()) + " Pause: " + Util.getMinutes(worktimeSetting.getPausetimes()[i].getPause()) + "\n");
			}
			
			sb.append("------------ Zuschl�ge Arbeitsarten --------" + "\n");
			for(Map.Entry<String,TimeentryKind> timeentryKind:worktimeSetting.getTimeentryKinds().entrySet()){
				sb.append("--- Arbeitsart: " + timeentryKind.getValue().getName() + " = " + Util.getPercent(timeentryKind.getValue().getValue()) + "\n");
			}

		}
		return sb;
	}
	
	public WorktimeSetting getWorktimeSettingForDate(Map<Date, WorktimeSetting> worktimeSettings,Date date) throws PersistenceException{
		WorktimeSetting worktimeSetting = null;// getDefaultWorktimeSetting();
		try{
			for(WorktimeSetting ws:worktimeSettings.values()){
				persistenceService.getLogger().trace("getWorktimeSettingForDate(TreeMap<Date, WorktimeSetting>,date): " + ws.getValidAfterDate() + " = " + (ws.getValidAfterDate().compareTo(date)<=0),this.getClass());
				if(ws.getValidAfterDate().before(date) || new DateString(date).equals(new DateString(ws.getValidAfterDate()))){
					worktimeSetting = getCompareWorktimeSetting(ws, date);
				}
			}
			persistenceService.getLogger().trace("getWorktimeSettingForDate(TreeMap<Date, WorktimeSetting>,date): " + worktimeSetting.getValidAfterDate(), this.getClass() );
			
		}catch(Exception e){
			
		}
		return worktimeSetting;
	}
	
	public WorktimeSetting getWorktimeSetting(String unid){
		if(unid == null) return null;
		return persistenceService.getWorktimeSetting(unid);
	}
	
	public List<WorktimeSetting> getWorktimeSettings() {
		return persistenceService.getWorktimeSettings();
	}
	
	public Map<Date, WorktimeSetting> getWorktimeSettings(Person person) throws PersistenceException {
		String key = (person == null?null:person.getPrimaryKey());
		return getWorktimeSettings(key);
	}

	public Map<Date, WorktimeSetting> getWorktimeSettings(String primaryKey) throws PersistenceException {
		TreeMap<Date,WorktimeSetting> worktimeSettings  = new TreeMap<Date, WorktimeSetting>();
		try{
			this.persistenceService.getLogger().trace("getWorktimeSettings: " + primaryKey,this.getClass());
			if(primaryKey != null){
				for(WorktimeSetting worktimeSetting:this.persistenceService.getWorktimeSettings(primaryKey)){
					worktimeSettings.put(worktimeSetting.getValidAfterDate(), worktimeSetting);
				}
				if(worktimeSettings.size()==0){
					persistenceService.getLogger().info("########## ACHTUNG KEINE WORKTIMESETTING F�R " + primaryKey + " GEFUNDEN #################",this.getClass());
				}
			}					
		}catch(Exception e){
			this.persistenceService.getLogger().error("",e);
		}
		return worktimeSettings;
	}

//	public WorktimeSetting getDefaultWorktimeSetting() throws PersistenceException{
//		WorktimeSetting worktimeSetting = new WorktimeSetting();
//		try{
//			worktimeSetting.setCoreTimeBegin(36000);
//			worktimeSetting.setCoreTimeEnd(72000);
//			worktimeSetting.setTimeentryKinds(new HashMap<String, TimeentryKind>());
//			worktimeSetting.getTimeentryKinds().put("0",this.persistenceService.getTimeentryKindService().getTimeentryKinds().get("0"));
//			worktimeSetting.setDailyWorktimeMonday(28800);
//			worktimeSetting.setDailyWorktimeThuesday(28800);
//			worktimeSetting.setDailyWorktimeWednesday(28800);
//			worktimeSetting.setDailyWorktimeThursday(28800);
//			worktimeSetting.setDailyWorktimeFryday(28800);
//			worktimeSetting.setDailyWorktimeSaturday(0);
//			worktimeSetting.setDailyWorktimeSunday(0);
//			worktimeSetting.setDailyPercentMonday(0);
//			worktimeSetting.setDailyPercentTuesday(0);
//			worktimeSetting.setDailyPercentWednesday(0);
//			worktimeSetting.setDailyPercentThursday(0);
//			worktimeSetting.setDailyPercentFriday(0);
//			worktimeSetting.setDailyPercentSaturday(0);
//			worktimeSetting.setDailyPercentSunday(0);
//			
//		}catch(Exception e){
//			persistenceService.getLogger().error("", e);
//		}
//
//		return worktimeSetting;
//	}

	public PersistenceService getPersistenceService() {
		return persistenceService;
	}
	
	
}
