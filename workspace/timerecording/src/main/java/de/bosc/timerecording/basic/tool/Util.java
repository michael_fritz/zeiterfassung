package de.bosc.timerecording.basic.tool;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class Util {

	static public String implode(Collection<String> coll, String delimiter){
		
		StringBuilder sb = new StringBuilder();
		int i =0;
		for(String str:coll){
			if(i != 0){
				sb.append(delimiter);
			}
			sb.append(str);
		}
		
		return sb.toString();
	}
	
	static public String getMinutes(Integer seconds){
		return (seconds != null?String.valueOf(seconds/60):"-") + " min";
	}
	static public String getHours(Integer seconds){
		return (seconds != null?String.valueOf(seconds/3600):"") + " h";
	}
	static public String getPercent(Integer percent){
		return (percent != null?percent:"") + " %";
	}
	static public String getPercent(Double percent){
		return (percent != null?percent:"") + " %";
	}
	static public String getWeekdayText(int day){
		SimpleDateFormat weekdayFormat = new SimpleDateFormat("EEE", Locale.GERMAN);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, day);
		
		return weekdayFormat.format(cal.getTime());
	}
	static public String getWeekdayTextLong(int day){
		SimpleDateFormat weekdayFormat = new SimpleDateFormat("EEEE", Locale.GERMAN);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, day);
		
		return weekdayFormat.format(cal.getTime());
	}
	
	/**
	 * Pr�ft ob zwei Listen von Strings identisch sind
	 * @param coll1
	 * @param coll2
	 * @return
	 */
	static public boolean equals(Collection<String> coll1, Collection<String> coll2){
		String str1 = implode(coll1,";");
		String str2 = implode(coll2,";");
		return str1.equals(str2);
	}
	
	static public Map<Integer,Integer> deepCopyInteger(Map<Integer,Integer> map){
		TreeMap<Integer,Integer> mapClone = new TreeMap<Integer,Integer>();
		for(Map.Entry<Integer, Integer> entry:map.entrySet()){
			mapClone.put((int)entry.getKey(), entry.getValue() != null?(int)entry.getValue():null);
		}
		return mapClone;
	}
	
	static public Map<Integer,Double> deepCopyDouble(Map<Integer,Double> map){
		TreeMap<Integer,Double> mapClone = new TreeMap<Integer,Double>();
		for(Map.Entry<Integer, Double> entry:map.entrySet()){
			mapClone.put((int)entry.getKey(), entry.getValue() != null?(double)entry.getValue():null);
		}
		return mapClone;
	}
}
